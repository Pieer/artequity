export default function(actionContext, slug, done) {
  actionContext.dispatch('RECEIVE_EXHIBITION_START');

  actionContext.service.read('exhibitions', { slug }, {}, (err, exhibition) => {
    if (err || !exhibition) {
      actionContext.dispatch('RECEIVE_ERROR', { code: 404, message: err.message });
    } else {
      actionContext.dispatch('RECEIVE_EXHIBITION_SUCCESS', exhibition);
    }

    done();
  });
}
