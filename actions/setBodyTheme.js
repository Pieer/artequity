export default function(actionContext, payload, done) {
  actionContext.dispatch('SET_BODY_THEME', payload);
  done();
}
