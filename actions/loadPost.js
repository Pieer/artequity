export default function(actionContext, slug, done) {
  actionContext.dispatch('RECEIVE_POST_START');

  actionContext.service.read('posts', { slug }, {}, (err, post) => {
    if (err || !post) {
      actionContext.dispatch('RECEIVE_ERROR', { code: 404, message: err.message });
    } else {
      actionContext.dispatch('RECEIVE_POST_SUCCESS', post);
    }

    done();
  });
}
