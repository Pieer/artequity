export default function(actionContext, { count, exclude }, done) {
  const params = {
    limit: count,
    exclude,
    sort: '-startDate',
  };

  actionContext.dispatch('RECEIVE_EXHIBITIONS_START');

  actionContext.service.read('exhibitions', params, {}, (err, exhibitions) => {
    if (err) {
      actionContext.dispatch('RECEIVE_EXHIBITIONS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_EXHIBITIONS_SUCCESS', {
        exhibitions,
        allExhibitionsLoaded: exhibitions.length < count,
      });
    }

    done();
  });
}
