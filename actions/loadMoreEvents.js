import EventStore from '../stores/EventStore';

export default function(actionContext, { count, exclude }, done) {
  const events = actionContext.getStore(EventStore).getEvents();

  const params = {
    skip: events ? events.length : 0,
    limit: count,
    exclude,
    sort: '-date',
  };

  actionContext.dispatch('RECEIVE_MORE_EVENTS_START');

  actionContext.service.read('events', params, {}, (err, moreEvents) => {
    if (err) {
      actionContext.dispatch('RECEIVE_MORE_EVENTS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_MORE_EVENTS_SUCCESS', {
        events: moreEvents,
        allEventsLoaded: moreEvents.length < count,
      });
    }

    done();
  });
}
