export default function(actionContext, payload, done) {
  actionContext.dispatch('RECEIVE_POST_CATEGORIES_START');

  actionContext.service.read('post-categories', {}, {}, (err, postCategories) => {
    if (err) {
      actionContext.dispatch('RECEIVE_POST_CATEGORIES_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_POST_CATEGORIES_SUCCESS', postCategories);
    }

    done();
  });
}
