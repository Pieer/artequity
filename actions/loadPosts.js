export default function(actionContext, { count, categories, artist, exhibition, exclude }, done) {
  const params = {
    categories: categories ? categories.join(',') : void 0,
    artist, exhibition,
    limit: count,
    exclude,
    sort: '-publishedDate',
  };

  actionContext.dispatch('RECEIVE_POSTS_START');

  actionContext.service.read('posts', params, {}, (err, posts) => {
    if (err) {
      actionContext.dispatch('RECEIVE_POSTS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_POSTS_SUCCESS', {
        posts,
        allPostsLoaded: posts.length < count,
      });
    }

    done();
  });
}
