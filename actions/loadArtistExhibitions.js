export default function(actionContext, artistSlug, done) {
  const params = {
    artist: artistSlug,
    sort: '-startDate',
  };

  actionContext.dispatch('RECEIVE_ARTIST_EXHIBITIONS_START');

  actionContext.service.read('exhibitions', params, {}, (err, exhibitions) => {
    if (err) {
      actionContext.dispatch('RECEIVE_ARTIST_EXHIBITIONS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_ARTIST_EXHIBITIONS_SUCCESS', exhibitions);
    }

    done();
  });
}
