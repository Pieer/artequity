export default function(actionContext, payload, done) {
  actionContext.dispatch('SET_CONFIG', payload);
  done();
}
