export default function(actionContext, count, done) {
  const params = {
    limit: count,
    random: true,
    tags: 'represented',
  };

  actionContext.dispatch('RECEIVE_REPRESENTED_ARTISTS_START');

  actionContext.service.read('artists', params, {}, (err, artists) => {
    if (err) {
      actionContext.dispatch('RECEIVE_REPRESENTED_ARTISTS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_REPRESENTED_ARTISTS_SUCCESS', artists);
    }

    done();
  });
}
