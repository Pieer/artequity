export default function(actionContext, artworkSlug, done) {
  const params = {
    artwork: artworkSlug,
    sort: '-startDate',
  };

  actionContext.dispatch('RECEIVE_ARTWORK_EXHIBITIONS_START');

  actionContext.service.read('exhibitions', params, {}, (err, exhibitions) => {
    if (err) {
      actionContext.dispatch('RECEIVE_ARTWORK_EXHIBITIONS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_ARTWORK_EXHIBITIONS_SUCCESS', exhibitions);
    }

    done();
  });
}
