export default function(actionContext, payload, done) {
  actionContext.dispatch('RECEIVE_DIRECTORS_START');

  actionContext.service.read('directors', {}, {}, (err, directors) => {
    if (err) {
      actionContext.dispatch('RECEIVE_DIRECTORS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_DIRECTORS_SUCCESS', directors);
    }

    done();
  });
}
