export default function(actionContext, slug, done) {
  actionContext.dispatch('RECEIVE_ARTWORK_START');

  actionContext.service.read('artworks', { slug }, {}, (err, artwork) => {
    if (err || !artwork) {
      actionContext.dispatch('RECEIVE_ERROR', { code: 404, message: err.message });
    } else {
      actionContext.dispatch('RECEIVE_ARTWORK_SUCCESS', artwork);
    }

    done();
  });
}
