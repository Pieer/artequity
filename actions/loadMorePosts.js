import PostStore from '../stores/PostStore';

export default function(actionContext, { count, categories, exclude }, done) {
  const posts = actionContext.getStore(PostStore).getPosts();

  const params = {
    skip: posts ? posts.length : 0,
    limit: count,
    categories: categories ? categories.join(',') : void 0,
    exclude,
    sort: '-publishedDate',
  };

  actionContext.dispatch('RECEIVE_MORE_POSTS_START');

  actionContext.service.read('posts', params, {}, (err, morePosts) => {
    if (err) {
      actionContext.dispatch('RECEIVE_MORE_POSTS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_MORE_POSTS_SUCCESS', {
        posts: morePosts,
        allPostsLoaded: morePosts.length < count,
      });
    }

    done();
  });
}
