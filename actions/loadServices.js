export default function(actionContext, payload, done) {
  actionContext.dispatch('RECEIVE_SERVICES_START');

  actionContext.service.read('services', {}, {}, (err, services) => {
    if (err) {
      actionContext.dispatch('RECEIVE_SERVICES_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_SERVICES_SUCCESS', services);
    }

    done();
  });
}
