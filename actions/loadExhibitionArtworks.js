export default function(actionContext, exhibitionSlug, done) {
  const params = {
    exhibition: exhibitionSlug,
    sort: 'name',
  };

  actionContext.dispatch('RECEIVE_EXHIBITION_ARTWORKS_START');

  actionContext.service.read('artworks', params, {}, (err, artworks) => {
    if (err) {
      actionContext.dispatch('RECEIVE_EXHIBITION_ARTWORKS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_EXHIBITION_ARTWORKS_SUCCESS', artworks);
    }

    done();
  });
}
