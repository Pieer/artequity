export default function(actionContext, slug, done) {
  const params = {
    slug,
    populate: 'instructions.artworks',
  };

  actionContext.dispatch('RECEIVE_SERVICE_START');

  actionContext.service.read('services', params, {}, (err, service) => {
    if (err || !service) {
      actionContext.dispatch('RECEIVE_ERROR', { code: 404, message: err.message });
    } else {
      actionContext.dispatch('RECEIVE_SERVICE_SUCCESS', service);
    }

    done();
  });
}
