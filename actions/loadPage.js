export default function(actionContext, path, done) {
  actionContext.dispatch('RECEIVE_PAGE_START');

  actionContext.service.read('pages', { path }, {}, (err, page) => {
    if (err || !page) {
      actionContext.dispatch('RECEIVE_PAGE_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_PAGE_SUCCESS', page);
    }

    done();
  });
}
