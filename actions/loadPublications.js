export default function(actionContext, payload, done) {
  actionContext.dispatch('RECEIVE_PUBLICATIONS_START');

  actionContext.service.read('publications', {}, {}, (err, publications) => {
    if (err) {
      actionContext.dispatch('RECEIVE_PUBLICATIONS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_PUBLICATIONS_SUCCESS', publications);
    }

    done();
  });
}
