export default function(actionContext, { hideInDealingRoom }, done) {
  const params = {
    hideInDealingRoom,
  };

  actionContext.dispatch('RECEIVE_TAGS_START');

  actionContext.service.read('tags', params, {}, (err, tags) => {
    if (err) {
      actionContext.dispatch('RECEIVE_TAGS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_TAGS_SUCCESS', tags);
    }

    done();
  });
}
