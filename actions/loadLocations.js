export default function(actionContext, payload, done) {
  actionContext.dispatch('RECEIVE_LOCATIONS_START');

  actionContext.service.read('locations', {}, {}, (err, locations) => {
    if (err) {
      actionContext.dispatch('RECEIVE_LOCATIONS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_LOCATIONS_SUCCESS', locations);
    }

    done();
  });
}
