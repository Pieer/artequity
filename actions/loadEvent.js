export default function(actionContext, slug, done) {
  actionContext.dispatch('RECEIVE_EVENT_START');

  actionContext.service.read('events', { slug }, {}, (err, event) => {
    if (err || !event) {
      actionContext.dispatch('RECEIVE_ERROR', { code: 404, message: err.message });
    } else {
      actionContext.dispatch('RECEIVE_EVENT_SUCCESS', event);
    }

    done();
  });
}
