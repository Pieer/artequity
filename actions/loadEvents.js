export default function(actionContext, { count, exclude }, done) {
  const params = {
    limit: count,
    exclude,
    sort: '-date',
  };

  actionContext.dispatch('RECEIVE_EVENTS_START');

  actionContext.service.read('events', params, {}, (err, events) => {
    if (err) {
      actionContext.dispatch('RECEIVE_EVENTS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_EVENTS_SUCCESS', {
        events,
        allEventsLoaded: events.length < count,
      });
    }

    done();
  });
}
