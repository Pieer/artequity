export default function(actionContext, payload, done) {
  const params = {
    limit: 1,
    current: true,
    sort: '-startDate',
  };

  actionContext.dispatch('RECEIVE_EXHIBITION_START');

  actionContext.service.read('exhibitions', params, {}, (err, [ exhibition ]) => {
    if (err) {
      actionContext.dispatch('RECEIVE_ERROR', { code: 404, message: err.message });
    } else {
      actionContext.dispatch('RECEIVE_EXHIBITION_SUCCESS', exhibition);
    }

    done();
  });
}
