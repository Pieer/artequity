export default function(actionContext, payload, done) {
  actionContext.dispatch('RECEIVE_LANDING_INFO_START');

  actionContext.service.read('pages', { path: '/' }, {}, (err, homePage) => {
    if (err || !homePage) {
      actionContext.dispatch('RECEIVE_LANDING_INFO_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_LANDING_INFO_SUCCESS', homePage.introduction);
    }

    done();
  });
}
