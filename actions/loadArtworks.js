export default function(actionContext, { count, dealingRoom, tags }, done) {
  const params = {
    limit: count,
    dealingRoom,
    tags: tags ? tags.join(',') : void 0,
    sort: 'name',
  };

  actionContext.dispatch('RECEIVE_ARTWORKS_START');

  actionContext.service.read('artworks', params, {}, (err, artworks) => {
    if (err) {
      actionContext.dispatch('RECEIVE_ARTWORKS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_ARTWORKS_SUCCESS', {
        artworks,
        allArtworksLoaded: artworks.length < count,
      });
    }

    done();
  });
}
