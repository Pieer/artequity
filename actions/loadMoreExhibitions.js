import ExhibitionStore from '../stores/ExhibitionStore';

export default function(actionContext, { count, exclude }, done) {
  const exhibitions = actionContext.getStore(ExhibitionStore).getExhibitions();

  const params = {
    skip: exhibitions ? exhibitions.length : 0,
    limit: count,
    exclude,
    sort: '-startDate',
  };

  actionContext.dispatch('RECEIVE_MORE_EXHIBITIONS_START');

  actionContext.service.read('exhibitions', params, {}, (err, moreExhibitions) => {
    if (err) {
      actionContext.dispatch('RECEIVE_MORE_EXHIBITIONS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_MORE_EXHIBITIONS_SUCCESS', {
        exhibitions: moreExhibitions,
        allExhibitionsLoaded: moreExhibitions.length < count,
      });
    }

    done();
  });
}
