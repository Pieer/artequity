export default function(actionContext, slug, done) {
  actionContext.dispatch('RECEIVE_ARTIST_START');

  actionContext.service.read('artists', { slug }, {}, (err, artist) => {
    if (err || !artist) {
      actionContext.dispatch('RECEIVE_ERROR', { code: 404, message: err.message });
    } else {
      actionContext.dispatch('RECEIVE_ARTIST_SUCCESS', artist);
    }

    done();
  });
}
