import ArtworkStore from '../stores/ArtworkStore';

export default function(actionContext, { count, dealingRoom, tags }, done) {
  const artworks = actionContext.getStore(ArtworkStore).getArtworks();

  const params = {
    skip: artworks ? artworks.length : 0,
    limit: count,
    dealingRoom,
    tags: tags ? tags.join(',') : void 0,
    sort: 'name',
  };

  actionContext.dispatch('RECEIVE_MORE_ARTWORKS_START');

  actionContext.service.read('artworks', params, {}, (err, moreArtworks) => {
    if (err) {
      actionContext.dispatch('RECEIVE_MORE_ARTWORKS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_MORE_ARTWORKS_SUCCESS', {
        artworks: moreArtworks,
        allArtworksLoaded: moreArtworks.length < count,
      });
    }

    done();
  });
}
