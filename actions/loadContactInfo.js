export default function(actionContext, payload, done) {
  actionContext.dispatch('RECEIVE_CONTACT_INFO_START');

  actionContext.service.read('pages', { path: '/contact' }, {}, (err, contactPage) => {
    if (err || !contactPage) {
      actionContext.dispatch('RECEIVE_CONTACT_INFO_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_CONTACT_INFO_SUCCESS', contactPage);
    }

    done();
  });
}
