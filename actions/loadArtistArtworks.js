export default function(actionContext, artistSlug, done) {
  const params = {
    artist: artistSlug,
    sort: 'name',
    visibleOnArtistProfile: true,
  };

  actionContext.dispatch('RECEIVE_ARTIST_ARTWORKS_START');

  actionContext.service.read('artworks', params, {}, (err, artworks) => {
    if (err) {
      actionContext.dispatch('RECEIVE_ARTIST_ARTWORKS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_ARTIST_ARTWORKS_SUCCESS', artworks);
    }

    done();
  });
}
