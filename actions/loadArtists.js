export default function(actionContext, tags, done) {
  const params = {
    tags: tags ? tags.join(',') : void 0,
    sort: 'name.last name.first',
  };

  actionContext.dispatch('RECEIVE_ARTISTS_START');

  actionContext.service.read('artists', params, {}, (err, artists) => {
    if (err) {
      actionContext.dispatch('RECEIVE_ARTISTS_FAILURE');
    } else {
      actionContext.dispatch('RECEIVE_ARTISTS_SUCCESS', artists);
    }

    done();
  });
}
