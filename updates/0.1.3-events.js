import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';
import cloudinary from 'cloudinary';
import faker from 'faker';
import htmlFaker from '../utils/htmlFaker';
import moment from 'moment';

export default function create(done) {
  if (process.env.NODE_ENV === 'production') { return done(); }

  async.auto({
    artworkImages(cb) {
      cloudinary.api.resources((result) => cb(void 0, result.resources), { type: 'upload', prefix: 'samples/artworks/' });
    },
  }, (err, { artworkImages }) => {
    if (err) { return done(err); }

    const events = _.times(20, () => {
      const featuredImage = faker.random.arrayElement(artworkImages);

      return {
        title: faker.commerce.productName(),
        state: 'published',
        date: moment(faker.date.future()).format('YYYY-MM-DD'),
        time: `${faker.random.number({ min: 1, max: 5 })} for ${faker.random.number({ min: 5, max: 10 })}pm start`,
        address: {
          number: faker.random.number({ min: 1, max: 100 }),
          street1: faker.address.streetAddress(),
          suburb: faker.address.city(),
          state: faker.address.state(),
          postcode: faker.address.zipCode(),
        },
        content: {
          extended: htmlFaker.paragraphs(),
        },
        featuredImage,
        thumbnailImage: featuredImage,
        rsvpLink: faker.internet.url(),
      };
    });

    keystone.createItems({
      Event: events,
    }, (createErr, stats) => {
      if (stats) { console.log(stats.message); }
      done(createErr);
    });
  });
}
