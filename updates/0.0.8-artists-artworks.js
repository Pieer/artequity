import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';
import cloudinary from 'cloudinary';
import faker from 'faker';
import htmlFaker from '../utils/htmlFaker';

export default function create(done) {
  async.auto({
    artistImages(cb) {
      cloudinary.api.resources((result) => cb(void 0, result.resources), { type: 'upload', prefix: 'samples/artists/' });
    },

    artworkImages(cb) {
      cloudinary.api.resources((result) => cb(void 0, result.resources), { type: 'upload', prefix: 'samples/artworks/' });
    },
  }, (err, { artistImages, artworkImages }) => {
    if (err) {
      console.log('Error retrieving sample images from cloudinary');
      return done(err);
    }

    let artworks = [];

    const tags = [
      { __ref: 'represented', name: 'Represented' },
      { __ref: 'indigenous', name: 'Indigenous' },
      { __ref: 'stockroom', name: 'Stockroom' },
    ];

    const representations = [
      { __ref: 'australia', name: 'N\H Australia' },
      { __ref: 'singapore', name: 'N\H Singapore' },
    ];

    const artists = _.times(40, (artistIndex) => {
      const artistRef = `artist_${artistIndex}`;

      const artistArtworks = _.times(faker.random.number({ min: 10, max: 15 }), (artworkIndex) => ({
        __ref: `artwork_${artistIndex}_${artworkIndex}`,
        title: faker.commerce.productName(),
        image: faker.random.arrayElement(artworkImages),
        shortDescription: faker.lorem.paragraph(),
        year: faker.random.number({ min: 1950, max: 2015 }),
        medium: faker.lorem.sentence(),
        size: `${faker.random.number({ min: 20, max: 40 })} x ${faker.random.number({ min: 40, max: 70 })}cm`,
        artist: artistRef,
        dealingRoom: faker.random.boolean(),
        visibleOnArtistProfile: faker.random.boolean(),
      }));

      artworks = artworks.concat(artistArtworks);

      return {
        __ref: artistRef,
        name: { full: faker.name.findName() },
        shortDescription: faker.lorem.paragraph(),
        birth: `Born ${faker.random.number({ min: 1950, max: 1990 })} in ${faker.address.country()}`,
        abode: faker.fake('Lives and works in {{address.city}}'),
        additionalInfo: faker.lorem.words(5).join(' '),
        bio: `<blockquote>${faker.lorem.paragraph()}</blockquote>${htmlFaker.paragraphs()}`,
        photo: faker.random.arrayElement(artistImages),
        featuredArtwork: artistArtworks[0].__ref,
        tags: _.unique(_.times(faker.random.number(3), () => faker.random.arrayElement(tags).__ref)),
        representedBy: _.unique(_.times(faker.random.number(2), () => faker.random.arrayElement(representations).__ref)),
      };
    });

    keystone.createItems({
      Tag: tags,
      Representation: representations,
      Artist: artists,
      Artwork: artworks,
    }, (createErr, stats) => {
      if (stats) { console.log(stats.message); }
      done(createErr);
    });
  });
}
