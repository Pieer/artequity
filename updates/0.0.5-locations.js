import keystone from 'keystone';

import async from 'async';
import cloudinary from 'cloudinary';
import faker from 'faker';
import htmlFaker from '../utils/htmlFaker';

export default function create(done) {
  async.auto({
    locationImages(cb) {
      cloudinary.api.resources((result) => cb(void 0, result.resources), { type: 'upload', prefix: 'samples/locations/' });
    },
  }, (err, { locationImages }) => {
    if (err) {
      console.log('Error retrieving sample images from cloudinary');
      return done(err);
    }

    keystone.createItems({
      Location: [
        {
          name: keystone.get('brand'),
          shortName: 'Sydney',
        },
        {
          name: 'Art Advisory London',
          shortName: 'London',
          description: htmlFaker.paragraphs(3),
          imageMode: 'background',
        },
        {
          name: 'Art Equity Singapore',
          shortName: 'Singapore',
          description: htmlFaker.paragraphs(3),
          image: faker.random.arrayElement(locationImages),
          imageMode: 'square',
        },
      ],
    }, (createErr, stats) => {
      if (stats) { console.log(stats.message); }
      done(createErr);
    });
  });
}
