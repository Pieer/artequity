export const create = {
  ContactPage: [{
    path: '/contact',
    title: 'Contact Us',
    address: {
      number: 'Level 1',
      street1: '66 King Street',
      suburb: 'Sydney',
      state: 'NSW',
      postcode: '2000',
    },
    phone: '+61 2 9262 6660',
    email: 'info@artequity.com.au',
    hours: '9am - 5pm Monday to Friday\n11am - 3pm Saturday',
    links: [
      'https://www.facebook.com/nandahobbs',
    ],
  }],
};
