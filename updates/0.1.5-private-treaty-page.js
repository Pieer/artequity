import htmlFaker from '../utils/htmlFaker';

export const create = {
  Page: [{
    path: '/private-treaty',
    title: 'Private Treaty',
    introduction: {
      title: 'Private Treaty',
      description: htmlFaker.paragraph(),
    },
  }],
};
