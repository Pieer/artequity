import faker from 'faker';

import htmlFaker from '../utils/htmlFaker';

export const create = {
  ServicesPage: [{
    path: '/services',
    title: 'Services',
    introduction: {
      title: 'Services',
      description: htmlFaker.paragraph(),
    },
    quote: {
      author: faker.name.findName(),
      text: faker.lorem.paragraph(),
    },
  }],
};
