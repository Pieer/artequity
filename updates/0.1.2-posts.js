import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';
import cloudinary from 'cloudinary';
import faker from 'faker';
import htmlFaker from '../utils/htmlFaker';
import moment from 'moment';

const Artist = keystone.list('Artist');
const Exhibition = keystone.list('Exhibition');
const Post = keystone.list('Post');
const PostCategory = keystone.list('PostCategory');
const User = keystone.list('User');

export default function create(done) {
  if (process.env.NODE_ENV === 'production') { return done(); }

  async.auto({
    artworkImages(cb) {
      cloudinary.api.resources((result) => cb(void 0, result.resources), { type: 'upload', prefix: 'samples/artworks/' });
    },

    categories(cb) {
      PostCategory.model.find().exec(cb);
    },

    users(cb) {
      User.model.find().exec(cb);
    },

    artists(cb) {
      Artist.model.find().exec(cb);
    },

    exhibitions(cb) {
      Exhibition.model.find().exec(cb);
    },
  }, (err, { artworkImages, categories, users, artists, exhibitions }) => {
    if (err) { return done(err); }

    const posts = _.times(20, () => {
      const image = faker.random.arrayElement(artworkImages);

      return {
        title: faker.commerce.productName(),
        state: 'published',
        author: faker.random.arrayElement(users),
        publishedDate: moment(faker.date.past()).format('YYYY-MM-DD'),
        image,
        content: {
          brief: htmlFaker.paragraph(),
          extended: htmlFaker.paragraphs(),
        },
        categories: _.unique(_.times(faker.random.number({ min: 1, max: 2 }), () => {
          return faker.random.arrayElement(categories).id;
        })),
        relatedArtists: _.unique(_.times(faker.random.number(1), () => {
          return faker.random.arrayElement(artists).id;
        })),
        relatedExhibitions: _.unique(_.times(faker.random.number(1), () => {
          return faker.random.arrayElement(exhibitions).id;
        })),
      };
    });

    function createPost(post, cb) {
      const PostModel = Post.model;
      const newPost = new PostModel(post);
      newPost.save(cb);
    }

    async.forEach(posts, createPost, (asyncErr) => {
      if (asyncErr) {
        console.error(asyncErr);
      } else {
        console.log(`\nSuccessfully created:\n\n${posts.length} Posts\n\n`);
      }

      done(err);
    });
  });
}
