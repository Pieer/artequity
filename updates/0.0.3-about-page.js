import faker from 'faker';
import htmlFaker from '../utils/htmlFaker';

export const create = {
  AboutPage: [{
    path: '/about',
    title: 'About',
    introduction: {
      title: 'About',
      description: htmlFaker.paragraph(),
    },
    feature: {
      title: 'A unique experience',
      description: htmlFaker.paragraphs(),
    },
  }],
};
