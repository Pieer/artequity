import keystone from 'keystone';

const Tag = keystone.list('Tag');

export default function create(done) {
  Tag.model.findOne({ slug: 'represented' }).exec((cb, tag) => {
    tag.hideInDealingRoom = true;
    tag.save(done);
  });
}
