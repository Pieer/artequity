import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';
import cloudinary from 'cloudinary';
import faker from 'faker';
import htmlFaker from '../utils/htmlFaker';
import moment from 'moment';

const Artist = keystone.list('Artist');
const Artwork = keystone.list('Artwork');
const Exhibition = keystone.list('Exhibition');

export default function create(done) {
  async.auto({
    artworkImages(cb) {
      cloudinary.api.resources((result) => cb(void 0, result.resources), { type: 'upload', prefix: 'samples/artworks/' });
    },

    artists(cb) {
      Artist.model.find().exec(cb);
    },

    artworks(cb) {
      Artwork.model.find().exec(cb);
    },
  }, (err, { artworkImages, artists, artworks }) => {
    if (err) { return done(err); }

    const exhibitions = _.times(20, () => {
      const featuredImage = faker.random.arrayElement(artworkImages);

      return {
        title: faker.commerce.productName(),
        shortDescription: faker.lorem.paragraph(),
        startDate: moment(faker.date.past()).format('YYYY-MM-DD'),
        endDate: moment(faker.date.future()).format('YYYY-MM-DD'),
        launchDate: moment(faker.date.future()).format('YYYY-MM-DD'),
        launchTime: `${faker.random.number({ min: 1, max: 5 })} to ${faker.random.number({ min: 5, max: 10 })}pm`,
        galleryHours: _.times(faker.random.number(3), () => {
          return `${faker.random.number({ min: 9, max: 11 })}am - ${faker.random.number({ min: 12, max: 6 })}pm ${moment(faker.date.future()).format('dddd')}`;
        }).join('\n'),
        location: {
          number: faker.random.number({ min: 1, max: 100 }),
          street1: faker.address.streetAddress(),
          suburb: faker.address.city(),
          state: faker.address.state(),
          postcode: faker.address.zipCode(),
        },
        essay: `<blockquote>${faker.lorem.paragraph()}</blockquote>${htmlFaker.paragraphs()}`,
        featuredImage,
        thumbnailImage: featuredImage,
        rsvpLink: faker.internet.url(),
      };
    });

    function createExhibition(exhibition, cb) {
      const ExhibitionModel = Exhibition.model;
      const newExhibition = new ExhibitionModel(exhibition);

      const artist = faker.random.arrayElement(artists);
      const artistArtworks = _.filter(artworks, (artwork) => artwork.artist.toString() === artist.id);

      newExhibition.artworks = _.unique(_.times(faker.random.number({ min: 5, max: 10 }), () => {
        return faker.random.arrayElement(artistArtworks).id;
      }));
      newExhibition.save(cb);
    }

    async.forEach(exhibitions, createExhibition, (asyncErr) => {
      if (asyncErr) {
        console.error(asyncErr);
      } else {
        console.log(`\nSuccessfully created:\n\n${exhibitions.length} Exhibitions\n\n`);
      }

      done(err);
    });
  });
}
