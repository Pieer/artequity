import keystone from 'keystone';

import async from 'async';
import cloudinary from 'cloudinary';
import faker from 'faker';
import htmlFaker from '../utils/htmlFaker';

export default function create(done) {
  async.auto({
    serviceImages(cb) {
      cloudinary.api.resources((result) => cb(void 0, result.resources), { type: 'upload', prefix: 'samples/services/' });
    },
  }, (err, { serviceImages }) => {
    if (err) {
      console.log('Error retrieving sample images from cloudinary');
      return done(err);
    }

    keystone.createItems({
      Service: [
        {
          __ref: 'art-rental',
          title: 'Art Rental',
          description: htmlFaker.paragraph(),
          image: faker.random.arrayElement(serviceImages),
          imageMode: 'wide',
          alignment: 'right',
          details: {
            intro: htmlFaker.paragraph(),
            content: htmlFaker.paragraphs(),
          },
          instructions: {
            title: '3 steps to Art Rental',
            content: htmlFaker.paragraph(),
          },
        },
        {
          __ref: 'portfolio-collection-services',
          title: 'Portfolio Collection Services',
          description: htmlFaker.paragraph(),
          image: faker.random.arrayElement(serviceImages),
          imageMode: 'square',
          alignment: 'right',
        },
        {
          __ref: 'investment-education',
          title: 'Investment Education',
          description: htmlFaker.paragraph(),
          image: faker.random.arrayElement(serviceImages),
          imageMode: 'wide',
          alignment: 'left',
        },
        {
          __ref: 'fine-arts-valuations',
          title: 'Fine Arts Valuations',
          description: htmlFaker.paragraph(),
          image: faker.random.arrayElement(serviceImages),
          imageMode: 'square',
          alignment: 'right',
        },
        {
          __ref: 'gallery-hire',
          title: 'Gallery Hire',
          description: htmlFaker.paragraph(),
          image: faker.random.arrayElement(serviceImages),
          imageMode: 'wide',
          alignment: 'right',
        },
      ],

      HomePage: [{
        path: '/',
        title: 'nanda\hobbs contemporary',
        introduction: {
          title: 'Introducing nanda\hobbs contemporary.',
          description: htmlFaker.paragraph(),
          link: '/about',
        },
        feature: {
          title: faker.company.companyName(),
          subtitle: faker.name.findName(),
          link: '/exhibitions',
        },
        services: [ 'art-rental', 'fine-arts-valuations', 'investment-education' ],
      }],
    }, (createErr, stats) => {
      if (stats) { console.log(stats.message); }
      done(createErr);
    });
  });
}
