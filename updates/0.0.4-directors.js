import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';
import cloudinary from 'cloudinary';
import faker from 'faker';
import htmlFaker from '../utils/htmlFaker';

export default function create(done) {
  async.auto({
    directorImages(cb) {
      cloudinary.api.resources((result) => cb(void 0, result.resources), { type: 'upload', prefix: 'samples/directors/' });
    },
  }, (err, { directorImages }) => {
    if (err) {
      console.log('Error retrieving sample images from cloudinary');
      return done(err);
    }

    keystone.createItems({
      Director: _.times(2, () => ({
        name: { full: faker.name.findName() },
        title: faker.name.jobTitle(),
        bio: htmlFaker.paragraph(),
        photo: faker.random.arrayElement(directorImages),
      })),
    }, (createErr, stats) => {
      if (stats) { console.log(stats.message); }
      done(createErr);
    });
  });
}
