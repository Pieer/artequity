import htmlFaker from '../utils/htmlFaker';

export const create = {
  Page: [{
    path: '/privacy',
    title: 'Privacy',
    content: htmlFaker.paragraphs(),
  }],
};
