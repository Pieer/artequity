const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');

const port = process.env.HOT_LOAD_PORT || 8888;

new WebpackDevServer(webpack(config), {
  contentBase: 'http://localhost:' + port,
  publicPath: config.output.publicPath,
  noInfo: true,
  hot: true,
}).listen(port, 'localhost', function listen(err) {
  if (err) { console.log(err); }
  console.log('Hot load server listening at localhost:' + port);
});
