import keystone from 'keystone';

import Page from './Page';

const Types = keystone.Field.Types;

const AboutPage = new keystone.List('AboutPage', {
  inherits: Page,
  nocreate: true,
  nodelete: true,
});

AboutPage.add('Feature', {
  feature: {
    title: { type: String, required: true, initial: true, note: 'e.g. “About”' },
    description: { type: Types.Html, wysiwyg: true, required: true, initial: true },
    image: { type: Types.CloudinaryImage },
  },
});

AboutPage.defaultColumns = 'title';
AboutPage.register();
