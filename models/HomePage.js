import keystone from 'keystone';

import Page from './Page';

const Types = keystone.Field.Types;

const HomePage = new keystone.List('HomePage', {
  inherits: Page,
  nocreate: true,
  nodelete: true,
});

HomePage.add('Feature', {
  feature: {
    title: { type: String, required: true, initial: true, note: 'e.g. “Super-Pop Mk4”' },
    subtitle: { type: String, initial: true, note: 'e.g. “Chris Booth”' },
    image: { type: Types.CloudinaryImage },
    dates: {
      start: { type: Types.Date, initial: true, label: 'Start Date' },
      end: { type: Types.Date, initial: true, label: 'End Date' },
    },
    link: { type: Types.Url, required: true, initial: true, note: 'e.g. “/exhibitions/super-pop-mk4”' },
  },
}, 'Services', {
  services: { type: Types.Relationship, ref: 'Service', many: true, initial: true },
});

HomePage.defaultColumns = 'title';
HomePage.register();
