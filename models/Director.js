import keystone from 'keystone';

import _ from 'lodash';

const Types = keystone.Field.Types;

const Director = new keystone.List('Director', {
  sortable: true,
  defaultSort: 'sortOrder',
  track: true,
});

Director.add({
  name: { type: Types.Name, required: true, initial: true },
  title: { type: String, required: true, initial: true },
  bio: { type: Types.Html, wysiwyg: true, required: true, initial: true },
  photo: { type: Types.CloudinaryImage },
});

Director.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    newRet.name.full = doc.name.full;
    return newRet;
  },
});

Director.defaultColumns = 'name';
Director.register();
