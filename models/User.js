import keystone from 'keystone';

import _ from 'lodash';

const Types = keystone.Field.Types;

const User = new keystone.List('User', {
  track: true,
});

User.add({
  name: { type: Types.Name, required: true, index: true },
  email: { type: Types.Email, initial: true, required: true, index: true },
  password: { type: Types.Password, initial: true, required: true },
});

User.schema.virtual('canAccessKeystone').get(() => true);

User.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v', 'password', 'email');
    newRet.id = doc.id;
    newRet.name.full = doc.name.full;
    return newRet;
  },
});

User.defaultColumns = 'name, email';
User.register();
