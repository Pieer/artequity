import keystone from 'keystone';

import _ from 'lodash';

const Representation = new keystone.List('Representation', {
  autokey: { path: 'slug', from: 'name', unique: true },
  track: true,
});

Representation.add({
  name: { type: String, required: true, initial: true, index: true },
});

Representation.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    return newRet;
  },
});

Representation.defaultColumns = 'name';
Representation.register();
