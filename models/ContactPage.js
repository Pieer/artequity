import keystone from 'keystone';

import Page from './Page';

const Types = keystone.Field.Types;

const ContactPage = new keystone.List('ContactPage', {
  inherits: Page,
  nocreate: true,
  nodelete: true,
});

ContactPage.add({
  map: { type: Types.CloudinaryImage },
  address: { type: Types.Location, required: true, initial: true },
  phone: { type: String, required: true, initial: true, note: 'e.g. “+61 2 9262 6660”' },
  email: { type: String, required: true, initial: true, note: 'e.g. “info@nandahobbs.com”' },
  hours: { type: Types.Textarea, required: true, initial: true },
  links: { type: Types.TextArray, initial: true, note: 'e.g. “https://www.facebook.com/nandahobbs”' },
});

ContactPage.defaultColumns = 'title';
ContactPage.register();
