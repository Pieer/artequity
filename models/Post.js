import keystone from 'keystone';

import _ from 'lodash';
import h2p from 'html2plaintext';

const Types = keystone.Field.Types;

const Post = new keystone.List('Post', {
  map: { name: 'title' },
  autokey: { path: 'slug', from: 'title', unique: true },
  track: true,
  defaultSort: '-publishedDate',
});

Post.add({
  title: { type: String, required: true, initial: true },
  state: { type: Types.Select, options: 'draft, published, archived', 'default': 'draft', index: true },
  author: { type: Types.Relationship, ref: 'User', index: true, initial: true },
  publishedDate: { type: Types.Datetime, index: true, dependsOn: { state: 'published' }, 'default': Date.now },
  image: { type: Types.CloudinaryImage },
  content: {
    brief: { type: Types.Html, wysiwyg: true, height: 150 },
    extended: { type: Types.Html, wysiwyg: true, height: 400 },
  },
  categories: { type: Types.Relationship, ref: 'PostCategory', many: true, required: true, initial: true },
  relatedArtists: { type: Types.Relationship, ref: 'Artist', many: true },
  relatedExhibitions: { type: Types.Relationship, ref: 'Exhibition', many: true },
  embedUrl: { type: Types.Url },
  embedContent: { type: Types.Embedly, from: 'embedUrl' },
  fileLabel: { type: String, label: 'File Attachment Label' },
  fileUpload: { type: Types.S3File, label: 'File Attachment Upload' },
});

Post.schema.virtual('twitterImage').get(function twitterImage() {
  return this._.image.scale(280, 150);
});

Post.schema.virtual('meta').get(function metaTags() {
  const metaTitle = this.title;
  const description = h2p(this.content.brief);
  const twitterHandle = keystone.get('twitter handle');

  return {
    title: `${metaTitle} - ${keystone.get('brand')}`,
    tags: [
      { name: 'description', content: description },
      { property: 'og:title', content: metaTitle },
      { property: 'og:description', content: description },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: twitterHandle },
      { name: 'twitter:creator', content: twitterHandle },
      { name: 'twitter:title', content: metaTitle },
      { name: 'twitter:description', content: description },
      { name: 'twitter:image', content: this.twitterImage },
    ],
  };
});

Post.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    newRet.meta = doc.meta;
    return newRet;
  },
});

Post.defaultColumns = 'title, state, author, publishedDate';
Post.register();
