import keystone from 'keystone';

import _ from 'lodash';

const Types = keystone.Field.Types;

const Artwork = new keystone.List('Artwork', {
  autokey: { path: 'slug', from: 'title', unique: true },
  track: true,
  defaultSort: '-_id',
});

Artwork.add({
  name: {
    type: String,
    noedit: true,
    hidden: true,
    initial: false,
    watch: 'title artist',
    value: function name(callback) {
      this.populate('artist', (err) => {
        if (err) { return callback(err); }
        callback(void 0, _.compact([
          this.title,
          this.artist && this.artist.name ? this.artist.name.full : void 0,
        ]).join(' - '));
      });
    },
  },
  title: { type: String, required: true, initial: true },
  metaDescription: { type: String, initial: true },
  image: { type: Types.CloudinaryImage },
  shortDescription: { type: String, initial: true },
  year: { type: Number, initial: true },
  medium: { type: String, initial: true, note: 'e.g. “Oil on canvas”' },
  size: { type: String, initial: true, note: 'e.g. “71 x 122cm”' },
  artist: { type: Types.Relationship, ref: 'Artist', initial: true, index: true },
  price: { type: String, initial: true, note: 'e.g. “$12 000”' },
  sold: { type: Boolean, initial: true },
  dealingRoom: { type: Boolean, initial: true, label: 'Displayed in dealing room', index: true },
  visibleOnArtistProfile: {
    type: Boolean,
    initial: true,
    label: 'Visible on artist’s profile',
    note:
      `An artwork could be visible within an exhibition but may no longer
       be included on the artist’s profile page.`,
    index: true,
  },
  fileLabel: { type: String, label: 'Private Treaty File Attachment Label', dependsOn: { dealingRoom: true } },
  fileUpload: { type: Types.S3File, label: 'Private Treaty File Attachment Upload', dependsOn: { dealingRoom: true } },
});

Artwork.schema.virtual('twitterImage').get(function twitterImage() {
  return this._.image.scale(280, 150);
});

Artwork.schema.virtual('meta').get(function metaTags() {
  const metaTitle = this.name;
  const description = this.metaDescription;
  const twitterHandle = keystone.get('twitter handle');

  return {
    title: `${metaTitle} - ${keystone.get('brand')}`,
    tags: [
      { name: 'description', content: description },
      { property: 'og:title', content: metaTitle },
      { property: 'og:description', content: description },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: twitterHandle },
      { name: 'twitter:creator', content: twitterHandle },
      { name: 'twitter:title', content: metaTitle },
      { name: 'twitter:description', content: description },
      { name: 'twitter:image', content: this.twitterImage },
    ],
  };
});

Artwork.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    newRet.meta = doc.meta;
    return newRet;
  },
});

Artwork.relationship({ ref: 'Exhibition', refPath: 'artworks' });

Artwork.defaultColumns = 'title, artist, year, medium, size, dealingRoom';
Artwork.register();
