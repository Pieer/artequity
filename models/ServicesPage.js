import keystone from 'keystone';

import Page from './Page';

const ServicesPage = new keystone.List('ServicesPage', {
  inherits: Page,
  nocreate: true,
  nodelete: true,
});

ServicesPage.add('Feature', {
  quote: {
    author: { type: String, initial: true },
    text: { type: String, initial: true },
  },
});

ServicesPage.defaultColumns = 'title';
ServicesPage.register();
