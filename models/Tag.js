import keystone from 'keystone';

import _ from 'lodash';

const Tag = new keystone.List('Tag', {
  autokey: { path: 'slug', from: 'name', unique: true },
  sortable: true,
  defaultSort: 'sortOrder',
  track: true,
});

Tag.add({
  name: { type: String, required: true, initial: true, index: true },
  hideInDealingRoom: { type: Boolean, 'default': false },
});

Tag.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    return newRet;
  },
});

Tag.defaultColumns = 'name';
Tag.register();
