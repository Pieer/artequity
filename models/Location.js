import keystone from 'keystone';

import _ from 'lodash';

const Types = keystone.Field.Types;

const Location = new keystone.List('Location', {
  sortable: true,
  defaultSort: 'sortOrder',
  track: true,
});

Location.add({
  name: { type: String, required: true, initial: true, note: 'e.g. “Art Advisory London”' },
  shortName: { type: String, required: true, initial: true, note: 'e.g. “London”' },
  description: { type: Types.Html, wysiwyg: true, initial: true },
  image: { type: Types.CloudinaryImage },
  imageMode: { type: Types.Select, options: 'background, square', 'default': 'square' },
});

Location.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    return newRet;
  },
});

Location.defaultColumns = 'name, shortName';
Location.register();
