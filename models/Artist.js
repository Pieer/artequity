import keystone from 'keystone';

import _ from 'lodash';
import random from 'mongoose-random';

import colors from '../utils/colors';

const Types = keystone.Field.Types;

const Artist = new keystone.List('Artist', {
  autokey: { path: 'slug', from: 'name.full', unique: true },
  track: true,
});

Artist.add({
  name: { type: Types.Name, required: true, index: true },
  metaDescription: { type: String, initial: true },
  color: { type: Types.Select, options: _.keys(colors), initial: true, 'default': 'blue' },
  shortDescription: { type: String, initial: true },
  birth: { type: String, initial: true, note: 'e.g. “Born 1960”, “Born 1970 in China” or “1923-2011”' },
  abode: { type: String, initial: true, note: 'e.g. “Lives and works in Sydney”' },
  additionalInfo: { type: String, initial: true },
  bio: { type: Types.Html, wysiwyg: true, initial: true },
  photo: { type: Types.CloudinaryImage },
  featuredImage: { type: Types.CloudinaryImage },
  featuredArtwork: { type: Types.Relationship, ref: 'Artwork', filters: { artist: ':_artistId' } },
  cv: { type: Types.S3File, label: 'CV' },
  tags: { type: Types.Relationship, ref: 'Tag', initial: true, many: true, index: true },
  representedBy: { type: Types.Relationship, ref: 'Representation', initial: true, many: true, index: true },
  _artistId: { type: String, hidden: true },
});

Artist.schema.plugin(random, { path: 'r' });

Artist.schema.pre('save', function preSave(next) {
  this._artistId = this.id;
  next();
});

Artist.schema.virtual('twitterImage').get(function twitterImage() {
  return this.featuredArtwork ? this.featuredArtwork.twitterImage : void 0;
});

Artist.schema.virtual('meta').get(function metaTags() {
  const metaTitle = this.name.full;
  const description = this.metaDescription;
  const twitterHandle = keystone.get('twitter handle');

  return {
    title: `${metaTitle} - ${keystone.get('brand')}`,
    tags: [
      { name: 'description', content: description },
      { property: 'og:title', content: metaTitle },
      { property: 'og:description', content: description },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: twitterHandle },
      { name: 'twitter:creator', content: twitterHandle },
      { name: 'twitter:title', content: metaTitle },
      { name: 'twitter:description', content: description },
      { name: 'twitter:image', content: this.twitterImage },
    ],
  };
});

Artist.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v', '_artistId', 'r');
    newRet.id = doc.id;
    newRet.name.full = doc.name.full;
    newRet.meta = doc.meta;
    return newRet;
  },
});

Artist.relationship({ ref: 'Artwork', path: 'artist' });
Artist.relationship({ ref: 'Exhibition', refPath: 'artists' });

Artist.defaultColumns = 'name, birth, abode';
Artist.register();

export default Artist;
