import keystone from 'keystone';

import _ from 'lodash';

import Artist from './Artist';

import colors from '../utils/colors';

const Types = keystone.Field.Types;

const Exhibition = new keystone.List('Exhibition', {
  map: { name: 'title' },
  autokey: { path: 'slug', from: 'title', unique: true },
  defaultSort: '-startDate',
  track: true,
});

Exhibition.add({
  title: { type: String, required: true, initial: true },
  metaDescription: { type: String, initial: true },
  color: { type: Types.Select, options: _.keys(colors), initial: true, 'default': 'blue' },
  shortDescription: { type: String, initial: true },
  startDate: { type: Types.Date, required: true, initial: true, index: true },
  endDate: { type: Types.Date, required: true, initial: true },
  launchDate: { type: Types.Datetime, required: true, initial: true },
  launchTime: { type: String, note: 'e.g. “6 to 8pm”', initial: true },
  galleryHours: { type: Types.Textarea, note: 'e.g. “9am - 5pm Monday to Friday”', initial: true },
  location: { type: Types.Location, required: true, initial: true },
  essay: { type: Types.Html, wysiwyg: true, initial: true, height: 500, label: 'Essay' },
  artworks: { type: Types.Relationship, ref: 'Artwork', many: true, required: true, initial: true },
  featuredImage: { type: Types.CloudinaryImage },
  thumbnailImage: { type: Types.CloudinaryImage },
  rsvpLink: {
    type: Types.Url,
    initial: true,
    note: 'e.g. “http://www.eventbrite.com/e/harry-guzzi-french-artist-in-sydney-exclusive-event-tickets-17778932265?aff=es2”',
    label: 'RSVP Link',
  },
  artists: {
    type: Types.Relationship,
    ref: 'Artist',
    many: true,
    noedit: true,
    watch: 'artworks',
    value: function artists(callback) {
      this.populate('artworks', (err) => {
        if (err) { return callback(err); }
        callback(void 0, _.uniq(_.compact(_.map(this.artworks, (artwork) => {
          if (artwork && artwork.artist) {
            const artistId = artwork.artist instanceof Artist.model ? artwork.artist.id : artwork.artist;
            return artistId.toString();
          }
        }))));
      });
    },
  },
});

Exhibition.schema.virtual('twitterImage').get(function twitterImage() {
  return this._.featuredImage.scale(280, 150);
});

Exhibition.schema.virtual('meta').get(function metaTags() {
  const metaTitle = this.title;
  const title = `${metaTitle} - ${_.compact(_.map(this.artists, (artist) => artist && artist.meta && artist.meta.title)).join(', ')}`;
  const description = this.metaDescription;
  const twitterHandle = keystone.get('twitter handle');

  return {
    title: `${metaTitle} - ${keystone.get('brand')}`,
    tags: [
      { name: 'description', content: description },
      { property: 'og:title', content: title },
      { property: 'og:description', content: description },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: twitterHandle },
      { name: 'twitter:creator', content: twitterHandle },
      { name: 'twitter:title', content: title },
      { name: 'twitter:description', content: description },
      { name: 'twitter:image', content: this.twitterImage },
    ],
  };
});

Exhibition.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    newRet.meta = doc.meta;
    if (newRet.location) { newRet.location.full = doc._.location.format(); }
    return newRet;
  },
});

Exhibition.defaultColumns = 'title, startDate, endDate, launchDate, launchTime';
Exhibition.register();
