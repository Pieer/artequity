import keystone from 'keystone';

import _ from 'lodash';

const PostCategory = new keystone.List('PostCategory', {
  autokey: { from: 'name', path: 'slug', unique: true },
  sortable: true,
  track: true,
});

PostCategory.add({
  name: { type: String, required: true },
});

PostCategory.relationship({ ref: 'Post', refPath: 'categories' });

PostCategory.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    return newRet;
  },
});

PostCategory.register();
