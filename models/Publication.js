import keystone from 'keystone';

import _ from 'lodash';

const Types = keystone.Field.Types;

const Publication = new keystone.List('Publication', {
  map: { name: 'title' },
  autokey: { path: 'slug', from: 'title', unique: true },
  track: true,
  sortable: true,
  defaultSort: '-id',
});

Publication.add({
  title: { type: String, required: true, initial: true },
  description: { type: Types.Html, wysiwyg: true, height: 400, initial: true },
  image: { type: Types.CloudinaryImage },
  price: { type: Types.Money, required: true, initial: true },
  paypalCode: { type: Types.Code, language: 'html', required: true, initial: true },
});

Publication.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    return newRet;
  },
});

Publication.defaultColumns = 'title, price';
Publication.register();
