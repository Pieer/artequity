import keystone from 'keystone';

import _ from 'lodash';
import faker from 'faker';

const Types = keystone.Field.Types;

const Page = new keystone.List('Page', {
  map: { name: 'title' },
  track: true,
});

Page.add({
  path: { type: Types.Url, required: true, initial: true, index: { unique: true }, note: 'eg. “/about”' },
}, 'Meta Data', {
  title: { type: String, required: true, initial: true },
  description: { type: Types.Textarea, required: true, initial: true, 'default': faker.lorem.paragraph() },
}, 'Introduction', {
  introduction: {
    title: { type: String, initial: true, note: 'e.g. “Introducing nanda\hobbs contemporary”' },
    description: { type: Types.Html, wysiwyg: true, initial: true },
    link: { type: Types.Url, initial: true, note: 'e.g. “/exhibitions/super-pop-mk4”' },
  },
}, 'Content', {
  content: { type: Types.Html, wysiwyg: true, initial: true },
});

Page.schema.virtual('meta').get(function metaTags() {
  const brand = keystone.get('brand');
  const facebookPage = keystone.get('facebook page');
  const root = keystone.get('root');

  return {
    title: `${this.title}${this.title !== brand ? ` - ${brand}` : ''}`,
    tags: [
      { name: 'description', content: this.description },
      { property: 'og:title', content: brand },
      { property: 'og:description', content: this.description },
      { property: 'og:site_name', content: brand },
      { property: 'og:url', content: `${root}${this.path}` },
      { property: 'og:image', content: `${root}/images/facebook.jpg` },
      { property: 'article:author', content: facebookPage },
      { property: 'article:publisher', content: facebookPage },
      { property: 'twitter:image:src', content: `${root}/images/twitter.jpg` },
    ],
  };
});

Page.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    newRet.meta = doc.meta;
    return newRet;
  },
});

Page.defaultColumns = 'title, path';
Page.register();

export default Page;
