import keystone from 'keystone';

import _ from 'lodash';

import colors from '../utils/colors';

const Types = keystone.Field.Types;

const Event = new keystone.List('Event', {
  map: { name: 'title' },
  autokey: { path: 'slug', from: 'title', unique: true },
  track: true,
  defaultSort: '-id',
});

Event.add({
  title: { type: String, required: true, initial: true },
  metaDescription: { type: String, initial: true },
  color: { type: Types.Select, options: _.keys(colors), initial: true, 'default': 'blue' },
  state: { type: Types.Select, options: 'draft, published, archived', 'default': 'draft', index: true },
  date: { type: Types.Datetime, required: true, initial: true },
  endTime: { type: Types.Datetime, initial: true },
  time: { type: String, initial: true, note: 'e.g. “6.15 for 6.30pm start”' },
  featuredImage: { type: Types.CloudinaryImage },
  thumbnailImage: { type: Types.CloudinaryImage },
  address: { type: Types.Location, required: true, initial: true },
  content: {
    extended: { type: Types.Html, wysiwyg: true, height: 400 },
  },
  rsvpLink: {
    type: Types.Url,
    initial: true,
    note: 'e.g. “http://www.eventbrite.com/e/harry-guzzi-french-artist-in-sydney-exclusive-event-tickets-17778932265?aff=es2”',
    label: 'RSVP Link',
  },
});

Event.schema.virtual('twitterImage').get(function twitterImage() {
  return this._.featuredImage.scale(280, 150);
});

Event.schema.virtual('meta').get(function metaTags() {
  const metaTitle = this.title;
  const description = this.metaDescription;
  const twitterHandle = keystone.get('twitter handle');

  return {
    title: `${metaTitle} - ${keystone.get('brand')}`,
    tags: [
      { name: 'description', content: description },
      { property: 'og:title', content: metaTitle },
      { property: 'og:description', content: description },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: twitterHandle },
      { name: 'twitter:creator', content: twitterHandle },
      { name: 'twitter:title', content: metaTitle },
      { name: 'twitter:description', content: description },
      { name: 'twitter:image', content: this.twitterImage },
    ],
  };
});

Event.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    newRet.meta = doc.meta;
    if (newRet.address) { newRet.address.full = doc._.address.format(); }
    return newRet;
  },
});

Event.defaultColumns = 'title, state, date';
Event.register();
