import keystone from 'keystone';

import _ from 'lodash';

const Types = keystone.Field.Types;

const Service = new keystone.List('Service', {
  autokey: { path: 'slug', from: 'title', unique: true },
  map: { name: 'title' },
  sortable: true,
  defaultSort: 'sortOrder',
  track: true,
});

Service.add({
  title: { type: String, required: true, initial: true },
  description: { type: Types.Html, wysiwyg: true, required: true, initial: true },
  image: { type: Types.CloudinaryImage },
  imageMode: { type: Types.Select, options: 'wide, square', 'default': 'wide' },
  alignment: { type: Types.Select, options: 'left, right', 'default': 'left' },
}, 'Details', {
  details: {
    intro: { type: Types.Html, wysiwyg: true },
    content: { type: Types.Html, wysiwyg: true },
    images: { type: Types.CloudinaryImages },
  },
}, 'Instructions', {
  instructions: {
    title: { type: String },
    content: { type: Types.Html, wysiwyg: true },
    artworks: { type: Types.Relationship, ref: 'Artwork', many: true },
  },
});

Service.schema.virtual('meta').get(function metaTags() {
  const metaTitle = this.title;

  return {
    title: `${metaTitle} - ${keystone.get('brand')}`,
    tags: [
      { property: 'og:title', content: metaTitle },
      { name: 'twitter:title', content: metaTitle },
    ],
  };
});

Service.schema.set('toJSON', {
  transform(doc, ret) {
    const newRet = _.omit(ret, '_id', '__v');
    newRet.id = doc.id;
    newRet.meta = doc.meta;
    return newRet;
  },
});

Service.defaultColumns = 'title';
Service.register();
