import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';

const Artist = keystone.list('Artist');
const Artwork = keystone.list('Artwork');
const Exhibition = keystone.list('Exhibition');
const Tag = keystone.list('Tag');

export default {
  name: 'artworks',
  read(req, resource, {
    id, slug,
    artist, exhibition,
    dealingRoom, visibleOnArtistProfile,
    tags,
    limit, skip = 0, sort,
    populate = 'artist'
  }, config, done) {
    if (id || slug) {
      const query = Artwork.model.findOne();

      if (id) { query.where('_id', id); }
      if (slug) { query.where({ slug }); }
      if (populate) { query.populate(populate); }

      query.exec((err, artwork) => {
        if (err) { return done(err); }
        if (!artwork) { return done(new Error('Artwork not found')); }
        done(err, artwork.toJSON());
      });
    } else {
      async.auto({
        tags(cb) {
          if (tags) {
            Tag.model.find({ slug: { $in: tags.split(',') } }).exec(cb);
          } else {
            cb();
          }
        },

        artists: [ 'tags', (cb, { tags: tagDocs }) => {
          if (tagDocs) {
            const query = Artist.model.find({}, { _id: 1 });
            if (tagDocs) { query.where({ tags: { $in: tagDocs } }); }
            query.exec(cb);
          } else {
            cb();
          }
        }],

        artist(cb) {
          if (artist) {
            Artist.model.findOne({ slug: artist }).exec(cb);
          } else {
            cb();
          }
        },

        exhibition(cb) {
          if (exhibition) {
            Exhibition.model.findOne({ slug: exhibition }).exec(cb);
          } else {
            cb();
          }
        },

        artworks: [ 'artists', 'artist', 'exhibition', (cb, { artists: artistDocs, artist: artistDoc, exhibition: exhibitionDoc }) => {
          const query = Artwork.model.find();

          if (artistDocs) { query.where('artist').in(artistDocs); }
          if (artistDoc) { query.where('artist', artistDoc); }
          if (exhibitionDoc) { query.where('_id').in(exhibitionDoc.artworks); }
          if (!_.isUndefined(dealingRoom)) { query.where('dealingRoom', dealingRoom); }
          if (!_.isUndefined(visibleOnArtistProfile)) { query.where('visibleOnArtistProfile', visibleOnArtistProfile); }
          if (limit) { query.limit(limit).skip(skip); }
          if (sort) { query.sort(sort); }
          if (populate) { query.populate(populate); }

          query.exec(cb);
        }],
      }, (err, { artworks }) => {
        done(err, _.invoke(artworks, 'toJSON'));
      });
    }
  },
};
