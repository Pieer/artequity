import keystone from 'keystone';

import _ from 'lodash';

const Location = keystone.list('Location');

export default {
  name: 'locations',
  read(req, resource, { sort = 'sortOrder' }, config, done) {
    const query = Location.model.find();
    if (sort) { query.sort(sort); }
    query.exec((err, locations) => done(err, _.invoke(locations, 'toJSON')));
  },
};
