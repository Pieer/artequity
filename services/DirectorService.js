import keystone from 'keystone';

import _ from 'lodash';

const Director = keystone.list('Director');

export default {
  name: 'directors',
  read(req, resource, { sort = 'sortOrder' }, config, done) {
    Director.model.find().sort(sort).exec((err, directors) => done(err, _.invoke(directors, 'toJSON')));
  },
};
