import keystone from 'keystone';

import _ from 'lodash';

const Page = keystone.list('Page');

export default {
  name: 'pages',
  read(req, resource, { path, populate }, config, done) {
    const decodedPath = decodeURI(path);

    Page.model.findOne().where('path', decodedPath).exec((err, page) => {
      if (err) { return done(err); }
      if (!page) { return done(new Error('Page not found')); }

      const schema = page.constructor.schema;
      const relationships = _.compact(_.map(schema.virtuals, (key, method) => {
        if (_.contains(method, 'RefList')) {
          return method.replace('RefList', '');
        }
      }));

      if (_.any(relationships)) {
        page.populate(relationships.join(' '), () => done(void 0, page.toJSON()));
      } else {
        done(void 0, page.toJSON());
      }
    });
  },
};
