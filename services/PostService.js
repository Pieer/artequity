import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';

const Artist = keystone.list('Artist');
const Exhibition = keystone.list('Exhibition');
const Post = keystone.list('Post');
const PostCategory = keystone.list('PostCategory');

export default {
  name: 'posts',
  read(req, resource, {
    id, slug,
    categories,
    artist, exhibition,
    exclude,
    limit, skip = 0, sort,
    populate = 'author categories'
  }, config, done) {
    const baseQuery = {
      state: 'published',
    };

    if (id || slug) {
      const query = Post.model.findOne(baseQuery);

      if (id) { query.where('_id', id); }
      if (slug) { query.where({ slug }); }
      if (populate) { query.populate(populate); }

      query.exec((err, post) => {
        if (err) { return done(err); }
        if (!post) { return done(new Error('Post not found')); }
        done(err, post.toJSON());
      });
    } else {
      async.auto({
        categories(cb) {
          if (categories) {
            PostCategory.model.find({ slug: { $in: categories.split(',') } }).exec(cb);
          } else {
            cb();
          }
        },

        artist(cb) {
          if (artist) {
            Artist.model.findOne({ slug: artist }).exec(cb);
          } else {
            cb();
          }
        },

        exhibition(cb) {
          if (exhibition) {
            Exhibition.model.findOne({ slug: exhibition }).exec(cb);
          } else {
            cb();
          }
        },

        posts: [ 'categories', 'artist', 'exhibition', (cb, { categories: categoryDocs, artist: artistDoc, exhibition: exhibitionDoc }) => {
          const query = Post.model.find(baseQuery);

          query.where('image.public_id').ne(null);

          if (categoryDocs) { query.where({ categories: { $in: categoryDocs } }); }
          if (exclude) { query.where('slug').ne(exclude); }
          if (artistDoc) { query.where('relatedArtists', artistDoc); }
          if (exhibitionDoc) { query.where('relatedExhibitions', exhibitionDoc); }
          if (limit) { query.limit(limit).skip(skip); }
          if (sort) { query.sort(sort); }
          if (populate) { query.populate(populate); }

          query.exec(cb);
        }],
      }, (err, { posts }) => {
        done(err, _.invoke(posts, 'toJSON'));
      });
    }
  },
};
