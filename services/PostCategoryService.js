import keystone from 'keystone';

import _ from 'lodash';

const PostCategory = keystone.list('PostCategory');

export default {
  name: 'post-categories',
  read(req, resource, { sort = 'sortOrder' }, config, done) {
    PostCategory.model.find().sort(sort).exec((err, postCategories) => done(err, _.invoke(postCategories, 'toJSON')));
  },
};
