import keystone from 'keystone';

import _ from 'lodash';

const Service = keystone.list('Service');

export default {
  name: 'services',
  read(req, resource, {
    id, slug,
    sort = 'sortOrder',
    populate,
  }, config, done) {
    if (id || slug) {
      const query = Service.model.findOne();

      if (id) { query.where('_id', id); }
      if (slug) { query.where({ slug }); }
      if (populate) { query.populate(populate); }

      query.exec((err, artist) => {
        if (err) { return done(err); }
        if (!artist) { return done(new Error('Service not found')); }
        done(err, artist.toJSON());
      });
    } else {
      Service.model.find().sort(sort).exec((err, services) => done(err, _.invoke(services, 'toJSON')));
    }
  },
};
