import keystone from 'keystone';

import _ from 'lodash';

const Tag = keystone.list('Tag');

export default {
  name: 'tags',
  read(req, resource, {
    hideInDealingRoom,
    sort = 'sortOrder',
  }, config, done) {
    const query = Tag.model.find().sort(sort);
    if (!_.isUndefined(hideInDealingRoom)) { query.where('hideInDealingRoom').ne(true); }
    query.exec((err, tags) => done(err, _.invoke(tags, 'toJSON')));
  },
};
