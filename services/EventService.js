import keystone from 'keystone';

import _ from 'lodash';

const Event = keystone.list('Event');

export default {
  name: 'events',
  read(req, resource, {
    id, slug,
    exclude,
    limit, skip = 0, sort,
  }, config, done) {
    const baseQuery = {
      state: 'published',
      date: { $gt: new Date() },
    };

    if (id || slug) {
      const query = Event.model.findOne(baseQuery);

      if (id) { query.where('_id', id); }
      if (slug) { query.where({ slug }); }

      query.exec((err, event) => {
        if (err) { return done(err); }
        if (!event) { return done(new Error('Event not found')); }
        done(err, event.toJSON());
      });
    } else {
      const query = Event.model.find(baseQuery);

      query.or([
        { 'featuredImage.public_id': { $ne: null } },
        { 'thumbnailImage.public_id': { $ne: null } },
      ]);

      if (exclude) { query.where('slug').ne(exclude); }
      if (limit) { query.limit(limit).skip(skip); }
      if (sort) { query.sort(sort); }

      query.exec((err, events) => {
        done(err, _.invoke(events, 'toJSON'));
      });
    }
  },
};
