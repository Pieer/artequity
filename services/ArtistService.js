import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';

const Artist = keystone.list('Artist');
const Representation = keystone.list('Representation');
const Tag = keystone.list('Tag');

export default {
  name: 'artists',
  read(req, resource, {
    id, slug,
    tags, representations,
    random,
    limit, skip = 0, sort,
    populate = 'featuredArtwork tags representedBy'
  }, config, done) {
    if (id || slug) {
      const query = Artist.model.findOne();

      if (id) { query.where('_id', id); }
      if (slug) { query.where({ slug }); }
      if (populate) { query.populate(populate); }

      query.exec((err, artist) => {
        if (err) { return done(err); }
        if (!artist) { return done(new Error('Artist not found')); }
        done(err, artist.toJSON());
      });
    } else {
      async.auto({
        tags(cb) {
          if (tags) {
            Tag.model.find({ slug: { $in: tags.split(',') } }).exec(cb);
          } else {
            cb();
          }
        },

        representations(cb) {
          if (representations) {
            Representation.model.find({ slug: { $in: representations.split(',') } }).exec(cb);
          } else {
            cb();
          }
        },

        artists: [ 'tags', 'representations', (cb, { tags: tagDocs, representations: representationsDocs }) => {
          const query = Artist.model[_.isUndefined(random) ? 'find' : 'findRandom']();

          query.where('featuredArtwork').ne(null);

          if (tagDocs) { query.where({ tags: { $in: tagDocs } }); }
          if (representationsDocs) { query.where({ representations: { $in: representationsDocs } }); }
          if (limit) { query.limit(limit).skip(skip); }
          if (sort) { query.sort(sort); }
          if (populate) { query.populate(populate); }

          query.exec(cb);
        }],
      }, (err, { artists }) => {
        done(err, _.invoke(artists, 'toJSON'));
      });
    }
  },
};
