import keystone from 'keystone';

import _ from 'lodash';

const Publication = keystone.list('Publication');

export default {
  name: 'publications',
  read(req, resource, {
    id, slug,
    exclude,
    limit, skip = 0, sort,
  }, config, done) {
    const query = Publication.model.find();

    query.where('image.public_id').ne(null);

    if (limit) { query.limit(limit).skip(skip); }
    if (sort) { query.sort(sort); }

    query.exec((err, publications) => {
      done(err, _.invoke(publications, 'toJSON'));
    });
  },
};
