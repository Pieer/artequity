import keystone from 'keystone';

import _ from 'lodash';
import async from 'async';

const Artist = keystone.list('Artist');
const Artwork = keystone.list('Artwork');
const Exhibition = keystone.list('Exhibition');

export default {
  name: 'exhibitions',
  read(req, resource, {
    id, slug,
    artist, artwork,
    current,
    exclude,
    limit, skip = 0, sort,
    populate = 'artists artworks'
  }, config, done) {
    if (id || slug) {
      const query = Exhibition.model.findOne();

      if (id) { query.where('_id', id); }
      if (slug) { query.where({ slug }); }
      if (populate) { query.populate(populate); }

      query.exec((err, exhibition) => {
        if (err) { return done(err); }
        if (!exhibition) { return done(new Error('Exhibition not found')); }

        if (_.contains(populate, 'artists')) {
          async.forEach(exhibition.artists, (exhibitionArtist, cb) => {
            exhibitionArtist.populate('tags representedBy', cb);
          }, (asyncErr) => {
            done(asyncErr, exhibition.toJSON());
          });
        } else {
          done(err, exhibition.toJSON());
        }
      });
    } else {
      async.auto({
        artist(cb) {
          if (artist) {
            Artist.model.findOne({ slug: artist }).exec(cb);
          } else {
            cb();
          }
        },

        artwork(cb) {
          if (artwork) {
            Artwork.model.findOne({ slug: artwork }).exec(cb);
          } else {
            cb();
          }
        },

        exhibitions: [ 'artist', 'artwork', (cb, { artist: artistDoc, artwork: artworkDoc }) => {
          const query = Exhibition.model.find();
          const now = Date.now();

          query.where('thumbnailImage.public_id').ne(null);

          if (exclude) { query.where('slug').ne(exclude); }
          if (artistDoc) { query.where('artists', artistDoc); }
          if (artworkDoc) { query.where('artworks', artworkDoc); }
          if (limit) { query.limit(limit).skip(skip); }
          if (sort) { query.sort(sort); }
          if (populate) { query.populate(populate); }
          if (!_.isUndefined(current)) { query.where('startDate').lte(now).where('endDate').gte(now); }

          query.exec(cb);
        }],
      }, (err, { exhibitions }) => {
        done(err, _.invoke(exhibitions, 'toJSON'));
      });
    }
  },
};
