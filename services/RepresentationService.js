import keystone from 'keystone';

import _ from 'lodash';

const Representation = keystone.list('Representation');

export default {
  name: 'representations',
  read(req, resource, { sort = 'name' }, config, done) {
    Representation.model.find().sort(sort).exec((err, representations) => done(err, _.invoke(representations, 'toJSON')));
  },
};
