import { createStore } from 'fluxible/addons';

const PostCategoryStore = createStore({
  storeName: 'PostCategoryStore',

  handlers: {
    'RECEIVE_POST_CATEGORIES_SUCCESS': 'receivePostCategories',
  },

  initialize() {
    this.postCategories = void 0;
  },

  receivePostCategories(postCategories) {
    this.postCategories = postCategories;
    this.emitChange();
  },

  getPostCategories() {
    return this.postCategories;
  },

  getState() {
    return {
      postCategories: this.postCategories,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.postCategories = state.postCategories;
  },
});

export default PostCategoryStore;
