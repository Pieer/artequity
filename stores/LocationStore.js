import { createStore } from 'fluxible/addons';

const LocationStore = createStore({
  storeName: 'LocationStore',

  handlers: {
    'RECEIVE_LOCATIONS_SUCCESS': 'receiveLocations',
  },

  initialize() {
    this.locations = void 0;
  },

  receiveLocations(locations) {
    this.locations = locations;
    this.emitChange();
  },

  getLocations() {
    return this.locations;
  },

  getState() {
    return {
      locations: this.locations,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.locations = state.locations;
  },
});

export default LocationStore;
