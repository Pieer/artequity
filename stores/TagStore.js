import { createStore } from 'fluxible/addons';

const TagStore = createStore({
  storeName: 'TagStore',

  handlers: {
    'RECEIVE_TAGS_SUCCESS': 'receiveTags',
  },

  initialize() {
    this.tags = void 0;
  },

  receiveTags(tags) {
    this.tags = tags;
    this.emitChange();
  },

  getTags() {
    return this.tags;
  },

  getState() {
    return {
      tags: this.tags,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.tags = state.tags;
  },
});

export default TagStore;
