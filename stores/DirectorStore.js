import { createStore } from 'fluxible/addons';

const DirectorStore = createStore({
  storeName: 'DirectorStore',

  handlers: {
    'RECEIVE_DIRECTORS_SUCCESS': 'receiveDirectors',
  },

  initialize() {
    this.directors = void 0;
  },

  receiveDirectors(directors) {
    this.directors = directors;
    this.emitChange();
  },

  getDirectors() {
    return this.directors;
  },

  getState() {
    return {
      directors: this.directors,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.directors = state.directors;
  },
});

export default DirectorStore;
