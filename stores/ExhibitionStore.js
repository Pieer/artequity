import { createStore } from 'fluxible/addons';

const ExhibitionStore = createStore({
  storeName: 'ExhibitionStore',

  handlers: {
    'RECEIVE_EXHIBITION_SUCCESS': 'receiveExhibition',
    'RECEIVE_EXHIBITIONS_SUCCESS': 'receiveExhibitions',
    'RECEIVE_MORE_EXHIBITIONS_START': 'receiveMoreExhibitionsStart',
    'RECEIVE_MORE_EXHIBITIONS_FAILURE': 'receiveMoreExhibitionsFailure',
    'RECEIVE_MORE_EXHIBITIONS_SUCCESS': 'receiveMoreExhibitions',
    'RECEIVE_ARTIST_EXHIBITIONS_SUCCESS': 'receiveArtistExhibitions',
    'RECEIVE_ARTWORK_EXHIBITIONS_SUCCESS': 'receiveArtworkExhibitions',
  },

  initialize() {
    this.exhibition = void 0;
    this.artistExhibitions = void 0;
    this.loadingMoreExhibitions = false;
    this.allExhibitionsLoaded = false;
  },

  receiveExhibition(exhibition) {
    this.exhibition = exhibition;
    this.emitChange();
  },

  receiveExhibitions({ exhibitions, allExhibitionsLoaded }) {
    this.allExhibitionsLoaded = allExhibitionsLoaded;
    this.exhibitions = exhibitions;
    this.emitChange();
  },

  receiveMoreExhibitionsStart() {
    this.loadingMoreExhibitions = true;
    this.emitChange();
  },

  receiveMoreExhibitionsFailure() {
    this.loadingMoreExhibitions = false;
    this.emitChange();
  },

  receiveMoreExhibitions({ exhibitions, allExhibitionsLoaded }) {
    this.allExhibitionsLoaded = allExhibitionsLoaded;
    this.exhibitions = this.exhibitions.concat(exhibitions);
    this.loadingMoreExhibitions = false;
    this.emitChange();
  },

  receiveArtistExhibitions(artistExhibitions) {
    this.artistExhibitions = artistExhibitions;
    this.emitChange();
  },

  receiveArtworkExhibitions(artworkExhibitions) {
    this.artworkExhibitions = artworkExhibitions;
    this.emitChange();
  },

  getExhibition() {
    return this.exhibition;
  },

  getExhibitions() {
    return this.exhibitions;
  },

  isLoadingMoreExhibitions() {
    return this.loadingMoreExhibitions;
  },

  isAllExhibitionsLoaded() {
    return this.allExhibitionsLoaded;
  },

  getArtistExhibitions() {
    return this.artistExhibitions;
  },

  getArtworkExhibitions() {
    return this.artworkExhibitions;
  },

  getState() {
    return {
      exhibition: this.exhibition,
      exhibitions: this.exhibitions,
      artistExhibitions: this.artistExhibitions,
      artworkExhibitions: this.artworkExhibitions,
      loadingMoreExhibitions: this.loadingMoreExhibitions,
      allExhibitionsLoaded: this.allExhibitionsLoaded,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.exhibition = state.exhibition;
    this.exhibitions = state.exhibitions;
    this.artistExhibitions = state.artistExhibitions;
    this.artworkExhibitions = state.artworkExhibitions;
    this.loadingMoreExhibitions = state.loadingMoreExhibitions;
    this.allExhibitionsLoaded = state.allExhibitionsLoaded;
  },
});

export default ExhibitionStore;
