import { createStore } from 'fluxible/addons';

const PageStore = createStore({
  storeName: 'PageStore',

  handlers: {
    'RECEIVE_PAGE_SUCCESS': 'receivePage',
    'RECEIVE_HOME_PAGE_SUCCESS': 'receiveHomePage',
    'RECEIVE_CONTACT_INFO_SUCCESS': 'receiveContactInfo',
  },

  initialize() {
    this.page = void 0;
    this.homePage = void 0;
    this.contactInfo = void 0;
  },

  receivePage(page) {
    this.page = page;
    this.emitChange();
  },

  receiveHomePage(homePage) {
    this.homePage = homePage;
    this.emitChange();
  },

  receiveContactInfo(contactInfo) {
    this.contactInfo = contactInfo;
    this.emitChange();
  },

  getPage() {
    return this.page;
  },

  getHomePage() {
    return this.homePage;
  },

  getContactInfo() {
    return this.contactInfo;
  },

  getState() {
    return {
      page: this.page,
      homePage: this.homePage,
      contactInfo: this.contactInfo,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.page = state.page;
    this.homePage = state.homePage;
    this.contactInfo = state.contactInfo;
  },
});

export default PageStore;
