import { createStore } from 'fluxible/addons';

const ArtworkStore = createStore({
  storeName: 'ArtworkStore',

  handlers: {
    'RECEIVE_ARTWORK_SUCCESS': 'receiveArtwork',
    'RECEIVE_ARTWORKS_SUCCESS': 'receiveArtworks',
    'RECEIVE_MORE_ARTWORKS_START': 'receiveMoreArtworksStart',
    'RECEIVE_MORE_ARTWORKS_FAILURE': 'receiveMoreArtworksFailure',
    'RECEIVE_MORE_ARTWORKS_SUCCESS': 'receiveMoreArtworks',
    'RECEIVE_ARTIST_ARTWORKS_SUCCESS': 'receiveArtistArtworks',
    'RECEIVE_EXHIBITION_ARTWORKS_SUCCESS': 'receiveExhibitionArtworks',
  },

  initialize() {
    this.artwork = void 0;
    this.artworks = void 0;
    this.artistArtworks = void 0;
    this.exhibitionArtworks = void 0;
    this.loadingMoreArtworks = false;
    this.allArtworksLoaded = false;
  },

  receiveArtwork(artwork) {
    this.artwork = artwork;
    this.emitChange();
  },

  receiveArtworks({ artworks, allArtworksLoaded }) {
    this.allArtworksLoaded = allArtworksLoaded;
    this.artworks = artworks;
    this.emitChange();
  },

  receiveMoreArtworksStart() {
    this.loadingMoreArtworks = true;
    this.emitChange();
  },

  receiveMoreArtworksFailure() {
    this.loadingMoreArtworks = false;
    this.emitChange();
  },

  receiveMoreArtworks({ artworks, allArtworksLoaded }) {
    this.allArtworksLoaded = allArtworksLoaded;
    this.artworks = this.artworks.concat(artworks);
    this.loadingMoreArtworks = false;
    this.emitChange();
  },

  receiveArtistArtworks(artistArtworks) {
    this.artistArtworks = artistArtworks;
    this.emitChange();
  },

  receiveExhibitionArtworks(exhibitionArtworks) {
    this.exhibitionArtworks = exhibitionArtworks;
    this.emitChange();
  },

  getArtwork() {
    return this.artwork;
  },

  getArtworks() {
    return this.artworks;
  },

  isLoadingMoreArtworks() {
    return this.loadingMoreArtworks;
  },

  isAllArtworksLoaded() {
    return this.allArtworksLoaded;
  },

  getArtistArtworks() {
    return this.artistArtworks;
  },

  getExhibitionArtworks() {
    return this.exhibitionArtworks;
  },

  getState() {
    return {
      artwork: this.artwork,
      artworks: this.artworks,
      artistArtworks: this.artistArtworks,
      exhibitionArtworks: this.exhibitionArtworks,
      loadingMoreArtworks: this.loadingMoreArtworks,
      allArtworksLoaded: this.allArtworksLoaded,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.artwork = state.artwork;
    this.artworks = state.artworks;
    this.artistArtworks = state.artistArtworks;
    this.exhibitionArtworks = state.exhibitionArtworks;
    this.loadingMoreArtworks = state.loadingMoreArtworks;
    this.allArtworksLoaded = state.allArtworksLoaded;
  },
});

export default ArtworkStore;
