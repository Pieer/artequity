import { createStore } from 'fluxible/addons';

const EventStore = createStore({
  storeName: 'EventStore',

  handlers: {
    'RECEIVE_EVENT_SUCCESS': 'receiveEvent',
    'RECEIVE_EVENTS_SUCCESS': 'receiveEvents',
    'RECEIVE_MORE_EVENTS_START': 'receiveMoreEventsStart',
    'RECEIVE_MORE_EVENTS_FAILURE': 'receiveMoreEventsFailure',
    'RECEIVE_MORE_EVENTS_SUCCESS': 'receiveMoreEvents',
  },

  initialize() {
    this.event = void 0;
    this.loadingMoreEvents = false;
    this.allEventsLoaded = false;
  },

  receiveEvent(event) {
    this.event = event;
    this.emitChange();
  },

  receiveEvents({ events, allEventsLoaded }) {
    this.allEventsLoaded = allEventsLoaded;
    this.events = events;
    this.emitChange();
  },

  receiveMoreEventsStart() {
    this.loadingMoreEvents = true;
    this.emitChange();
  },

  receiveMoreEventsFailure() {
    this.loadingMoreEvents = false;
    this.emitChange();
  },

  receiveMoreEvents({ events, allEventsLoaded }) {
    this.allEventsLoaded = allEventsLoaded;
    this.events = this.events.concat(events);
    this.loadingMoreEvents = false;
    this.emitChange();
  },

  getEvent() {
    return this.event;
  },

  getEvents() {
    return this.events;
  },

  isLoadingMoreEvents() {
    return this.loadingMoreEvents;
  },

  isAllEventsLoaded() {
    return this.allEventsLoaded;
  },

  getState() {
    return {
      event: this.event,
      events: this.events,
      loadingMoreEvents: this.loadingMoreEvents,
      allEventsLoaded: this.allEventsLoaded,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.event = state.event;
    this.events = state.events;
    this.loadingMoreEvents = state.loadingMoreEvents;
    this.allEventsLoaded = state.allEventsLoaded;
  },
});

export default EventStore;
