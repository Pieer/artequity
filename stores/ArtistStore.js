import { createStore } from 'fluxible/addons';

const ArtistStore = createStore({
  storeName: 'ArtistStore',

  handlers: {
    'RECEIVE_REPRESENTED_ARTISTS_SUCCESS': 'receiveRepresentedArtists',
    'RECEIVE_ARTISTS_SUCCESS': 'receiveArtists',
    'RECEIVE_ARTIST_SUCCESS': 'receiveArtist',
  },

  initialize() {
    this.representedArtists = void 0;
    this.artists = void 0;
    this.artist = void 0;
  },

  receiveRepresentedArtists(representedArtists) {
    this.representedArtists = representedArtists;
    this.emitChange();
  },

  receiveArtists(artists) {
    this.artists = artists;
    this.emitChange();
  },

  receiveArtist(artist) {
    this.artist = artist;
    this.emitChange();
  },

  getRepresentedArtists() {
    return this.representedArtists;
  },

  getArtists() {
    return this.artists;
  },

  getArtist() {
    return this.artist;
  },

  getState() {
    return {
      representedArtists: this.representedArtists,
      artists: this.artists,
      artist: this.artist,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.representedArtists = state.representedArtists;
    this.artists = state.artists;
    this.artist = state.artist;
  },
});

export default ArtistStore;
