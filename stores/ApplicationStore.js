import { createStore } from 'fluxible/addons';

const ApplicationStore = createStore({
  storeName: 'ApplicationStore',

  handlers: {
    'SET_CONFIG': 'setConfig',
    'SET_BODY_THEME': 'setBodyTheme',
    'RECEIVE_ERROR': 'receiveError',
  },

  initialize() {
    this.config = void 0;
    this.bodyTheme = 'primary';
    this.error = void 0;
  },

  setConfig(config) {
    this.config = config;
    this.emitChange();
  },

  setBodyTheme(bodyTheme) {
    this.bodyTheme = bodyTheme;
    this.emitChange();
  },

  receiveError(error) {
    this.error = error;
    this.emitChange();
  },

  getConfig() {
    return this.config;
  },

  getBodyTheme() {
    return this.bodyTheme;
  },

  getError() {
    return this.error;
  },

  getState() {
    return {
      config: this.config,
      bodyTheme: this.bodyTheme,
      error: this.error,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.config = state.config;
    this.bodyTheme = state.bodyTheme;
    this.error = state.error;
  },
});

export default ApplicationStore;
