import { createStore } from 'fluxible/addons';

const PublicationStore = createStore({
  storeName: 'PublicationStore',

  handlers: {
    'RECEIVE_PUBLICATIONS_SUCCESS': 'receivePublications',
  },

  initialize() {
    this.publications = void 0;
  },

  receivePublications(publications) {
    this.publications = publications;
    this.emitChange();
  },

  getPublications() {
    return this.publications;
  },

  getState() {
    return {
      publications: this.publications,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.publications = state.publications;
  },
});

export default PublicationStore;
