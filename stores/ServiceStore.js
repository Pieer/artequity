import { createStore } from 'fluxible/addons';

const ServiceStore = createStore({
  storeName: 'ServiceStore',

  handlers: {
    'RECEIVE_SERVICES_SUCCESS': 'receiveServices',
    'RECEIVE_SERVICE_SUCCESS': 'receiveService',
  },

  initialize() {
    this.services = void 0;
    this.service = void 0;
  },

  receiveServices(services) {
    this.services = services;
    this.emitChange();
  },

  receiveService(service) {
    this.service = service;
    this.emitChange();
  },

  getServices() {
    return this.services;
  },

  getService() {
    return this.service;
  },

  getState() {
    return {
      services: this.services,
      service: this.service,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.services = state.services;
    this.service = state.service;
  },
});

export default ServiceStore;
