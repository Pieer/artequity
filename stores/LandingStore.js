import cookie from 'react-cookie';

import { createStore } from 'fluxible/addons';

const LandingStore = createStore({
  storeName: 'LandingStore',

  handlers: {
    'SET_LANDING': 'setLanding',
    'RECEIVE_LANDING_INFO_SUCCESS': 'receiveInfo',
  },

  initialize() {
    this.landing = !cookie.load('landed');
  },

  setLanding(landing) {
    this.landing = landing;
    this.emitChange();
    if (!landing) { cookie.save('landed', true, { path: '/' }); }
  },

  receiveInfo(info) {
    this.info = info;
    this.emitChange();
  },

  isLanding() {
    return this.landing;
  },

  getInfo() {
    return this.info;
  },

  getState() {
    return {
      landing: this.landing,
      info: this.info,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.landing = state.landing;
    this.info = state.info;
  },
});

export default LandingStore;
