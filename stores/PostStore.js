import { createStore } from 'fluxible/addons';

const PostStore = createStore({
  storeName: 'PostStore',

  handlers: {
    'RECEIVE_POST_SUCCESS': 'receivePost',
    'RECEIVE_POSTS_SUCCESS': 'receivePosts',
    'RECEIVE_MORE_POSTS_START': 'receiveMorePostsStart',
    'RECEIVE_MORE_POSTS_FAILURE': 'receiveMorePostsFailure',
    'RECEIVE_MORE_POSTS_SUCCESS': 'receiveMorePosts',
  },

  initialize() {
    this.post = void 0;
    this.loadingMorePosts = false;
    this.allPostsLoaded = false;
  },

  receivePost(post) {
    this.post = post;
    this.emitChange();
  },

  receivePosts({ posts, allPostsLoaded }) {
    this.allPostsLoaded = allPostsLoaded;
    this.posts = posts;
    this.emitChange();
  },

  receiveMorePostsStart() {
    this.loadingMorePosts = true;
    this.emitChange();
  },

  receiveMorePostsFailure() {
    this.loadingMorePosts = false;
    this.emitChange();
  },

  receiveMorePosts({ posts, allPostsLoaded }) {
    this.allPostsLoaded = allPostsLoaded;
    this.posts = this.posts.concat(posts);
    this.loadingMorePosts = false;
    this.emitChange();
  },

  getPost() {
    return this.post;
  },

  getPosts() {
    return this.posts;
  },

  isLoadingMorePosts() {
    return this.loadingMorePosts;
  },

  isAllPostsLoaded() {
    return this.allPostsLoaded;
  },

  getState() {
    return {
      post: this.post,
      posts: this.posts,
      loadingMorePosts: this.loadingMorePosts,
      allPostsLoaded: this.allPostsLoaded,
    };
  },

  dehydrate() {
    return this.getState();
  },

  rehydrate(state) {
    this.post = state.post;
    this.posts = state.posts;
    this.loadingMorePosts = state.loadingMorePosts;
    this.allPostsLoaded = state.allPostsLoaded;
  },
});

export default PostStore;
