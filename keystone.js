require('dotenv').load();
require('babel/register')({ stage: 0 });

const keystone = require('keystone');

const async = require('async');
const autoprefixer = require('autoprefixer-core');

keystone.init({
  'name': 'nanda\\hobbs',
  'brand': 'nandahobbs contemporary',

  'description': 'Art Equity has become a byword for innovation in the art market. Over the past decade our representative gallery has grown to include exciting and esteemed contemporary artists from Australia and overseas. Now, it’s time for re-visioning…welcome to nanda\\hobbs contemporary.',

  'facebook page': 'https://www.facebook.com/nandahobbs',
  'twitter handle': '@nandahobbs',

  'root': process.env.ROOT || ((keystone.get('host') || 'http://localhost') + ':' + (keystone.get('port') || '3000')),

  'less': 'public',
  'less options': {
    preprocess: {
      importPaths: function importPaths(paths) {
        paths.push('node_modules');
      },
    },
    postprocess: {
      css: function css(result) {
        const processor = autoprefixer({ browsers: [ 'last 2 version' ], cascade: false });
        return processor.process(result);
      },
    },
  },

  'wysiwyg additional buttons': 'blockquote',

  'static': 'public',
  'favicon': 'public/favicon.ico',

  'auto update': true,
  'session': true,
  'auth': true,
  'user model': 'User',
  'cookie secret': ',N)MlX8kK8XlcDMN{qNp_f1KXB/(xvqoT,yQ"(8FQ!|]WklO2_`kbfOz8UZ0psZV',
});

keystone.import('models');

keystone.set('meta tags', (function metaTags() {
  const brand = keystone.get('brand');
  const description = keystone.get('description');
  const facebookPage = keystone.get('facebook page');
  const root = keystone.get('root');

  return [
    { name: 'description', content: description },
    { property: 'og:title', content: brand },
    { property: 'og:description', content: description },
    { property: 'og:site_name', content: brand },
    { property: 'og:url', content: root },
    { property: 'og:image', content: root + '/images/facebook.jpg' },
    { property: 'article:author', content: facebookPage },
    { property: 'article:publisher', content: facebookPage },
    { property: 'twitter:image:src', content: root + '/images/twitter.jpg' },
  ];
})());

keystone.set('locals', {
  _: require('lodash'),
  env: keystone.get('env'),
  utils: keystone.utils,
});

keystone.set('routes', require('./routes'));
keystone.set('nav', {});

keystone.start({
  onMount: function onMount() {
    async.auto({
      home: function home(cb) { keystone.list('HomePage').model.findOne().exec(cb); },
      about: function about(cb) { keystone.list('AboutPage').model.findOne().exec(cb); },
      privateTreaty: function about(cb) { keystone.list('Page').model.findOne({ path: '/private-treaty' }).exec(cb); },
      services: function about(cb) { keystone.list('ServicesPage').model.findOne({ path: '/services' }).exec(cb); },
      contact: function about(cb) { keystone.list('ContactPage').model.findOne().exec(cb); },
      privacy: function about(cb) { keystone.list('Page').model.findOne({ path: '/privacy' }).exec(cb); },
    }, function setupNav(err, result) {
      if (err) { return; }

      keystone.set('nav', {
        'pages': [
          'pages',
          { label: 'Home', key: 'home', path: '/keystone/home-pages/' + result.home.id },
          { label: 'About', key: 'about', path: '/keystone/about-pages/' + result.about.id },
          { label: 'Private Treaty', key: 'about', path: '/keystone/pages/' + result.privateTreaty.id },
          { label: 'Services', key: 'about', path: '/keystone/services-pages/' + result.services.id },
          { label: 'Contact', key: 'contact', path: '/keystone/contact-pages/' + result.contact.id },
          { label: 'Privacy', key: 'privacy', path: '/keystone/pages/' + result.privacy.id },
        ],
        'users': 'users',
        'artists': 'artists',
        'artworks': 'artworks',
        'exhibitions': 'exhibitions',
        'posts': [ 'posts', 'post-categories' ],
        'events': 'events',
        'publications': 'publications',
        'miscellaneous': [
          'services',
          'directors',
          'locations',
          'tags',
          'representations',
        ],
      });

      keystone.initNav();
    });
  },
});
