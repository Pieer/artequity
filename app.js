import Fluxible from 'fluxible';
import fetchrPlugin from 'fluxible-plugin-fetchr';

import Routes from './components/Routes.jsx';

import ApplicationStore from './stores/ApplicationStore';
import ArtistStore from './stores/ArtistStore';
import ArtworkStore from './stores/ArtworkStore';
import DirectorStore from './stores/DirectorStore';
import ExhibitionStore from './stores/ExhibitionStore';
import EventStore from './stores/EventStore';
import LandingStore from './stores/LandingStore';
import LocationStore from './stores/LocationStore';
import PostCategoryStore from './stores/PostCategoryStore';
import PageStore from './stores/PageStore';
import PostStore from './stores/PostStore';
import PublicationStore from './stores/PublicationStore';
import TagStore from './stores/TagStore';
import ServiceStore from './stores/ServiceStore';

const app = new Fluxible({
  component: Routes,
});

app.plug(fetchrPlugin({
  xhrPath: '/api',
}));

app.registerStore(ApplicationStore);
app.registerStore(ArtistStore);
app.registerStore(ArtworkStore);
app.registerStore(DirectorStore);
app.registerStore(ExhibitionStore);
app.registerStore(EventStore);
app.registerStore(LandingStore);
app.registerStore(LocationStore);
app.registerStore(PageStore);
app.registerStore(PostCategoryStore);
app.registerStore(PostStore);
app.registerStore(PublicationStore);
app.registerStore(TagStore);
app.registerStore(ServiceStore);

export default app;
