import React from 'react';
import { Resolver } from 'react-resolver';
import Router, { HistoryLocation } from 'react-router';

import FluxibleComponent from 'fluxible-addons-react/FluxibleComponent';

import cloudinaryConfig from 'cloudinary/lib/config';

import app from './app';

const dehydratedState = window.App;

window.React = React;

app.rehydrate(dehydratedState, (err, context) => {
  if (err) throw err;

  window.context = context;

  const config = context.getStore('ApplicationStore').getConfig();
  cloudinaryConfig('cloud_name', config.cloudinary.cloudName);

  function renderApp(handler) {
    const mountNode = document.getElementById('app');
    const component = React.createFactory(handler);

    const element = React.createElement(
      FluxibleComponent,
      { context: context.getActionContext() },
      component()
    );

    const resolver = new Resolver(window.resolver);
    delete window.resolver;

    Resolver.render(element, mountNode, resolver);
    return resolver.finish();
  }

  Router.run(app.getComponent(), HistoryLocation, (Handler) => {
    renderApp(Handler);
  });
});
