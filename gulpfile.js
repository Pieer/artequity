const gulp = require('gulp');
const livereload = require('gulp-livereload');

gulp.task('watch', function watch() {
  livereload.listen();

  gulp.watch('./public/styles/**/*.less')
    .on('change', function lessChanged() {
      livereload.changed('/styles/site.css');
    });
});
