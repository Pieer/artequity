nanda/hobbs
=================

## Getting Started

Begin by first installing node dependencies:

    $ npm install

### Environment Variables

To help with development, this project uses `dotenv` to setup environment
variables locally when the server is started. To get started create a new
`.env` file in the root of this project. Below is a sample of what is
required by the server for it to run:

    CLOUDINARY_URL=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    EMBEDLY_API_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    FORM_STACK_ID=xxxxxxxxxx (optional)
    FORM_STACK_VIEWKEY=xxxxxxxxxxx (optional)
    FORM_STACK_SUBSCRIBE_ID=xxxxxxxxxx (optional)
    FORM_STACK_SUBSCRIBE_VIEWKEY=xxxxxxxxxx (optional)
    GTM_CODE=GTM-xxxxxx (optional)
    MONGO_URL=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    NPM_CONFIG_PRODUCTION=false
    ROOT=http://nandahobbs.herokuapp.com
    S3_BUCKET=nandahobbs
    S3_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    S3_SECRET=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

Note: Some of those are test or dummy values, as your supervisor for actual
values you can use.

### Running Locally

Simply run the npm start script to start the web server in dev mode:

    $ npm run dev

### Tmuxinator

There is also a `tmuxinator.yml` config file for this project as well, if you
would like to use it add it to your `~/.tmuxinator` folder:

    $ ln ~/Work/JimmyMakesThings/nandahobbs/tmuxinator.yml ~/.tmuxinator/nandahobbs.yml

Then you can kickstart tmux by running:

    $ mux start nandahobbs

## Deployment

This project is deployed by Heroku, make sure you have the Heroku Toolbet
installed first:

    $ brew install heroku-toolbelt

Then login following these instructions (you can find the Heroku account
details in Jimmy's 1Password vault):
https://devcenter.heroku.com/articles/getting-started-with-nodejs#set-up

Once logged in, add the `heroku` Git remote:

    $ heroku git:remote -a nandahobbs

You can now deploy simply by pushing to the `heroku` remote:

    $ git push heroku master
