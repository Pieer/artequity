import seedrandom from 'seedrandom';

const colors = 'primary success info warning danger neutral dark soft haze clay shocking chetwood glade anakiwa tan manz silk buccaneer'.split(' ');
export default (seed) => {
  return colors[Math.abs(seedrandom(seed).int32()) % colors.length];
};
