export default (mixin) => {
  mixin.performLayout = function performLayout() {
    const diff = this.diffDomChildren();

    if (diff.removed.length > 0) { this.masonry.reloadItems(); }
    if (diff.added.length > 0) { this.masonry.appended(diff.added); }
    if (diff.moved.length > 0) { this.masonry.reloadItems(); }

    this.masonry.layout();
  };

  return mixin;
};
