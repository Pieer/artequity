import _ from 'lodash';
import faker from 'faker';

export default {
  paragraph() {
    return `<p>${faker.lorem.paragraph()}</p>`;
  },

  paragraphs(n = 5) {
    return _.times(n, this.paragraph).join('');
  },
};
