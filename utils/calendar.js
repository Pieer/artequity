import _ from 'lodash';
import moment from 'moment';
import 'moment-timezone';
import querystring from 'querystring';

require('moment-duration-format');

function prepare(event) {
  const startDate = moment(event.startDate).tz('Australia/Sydney');
  const endDate = moment(event.endDate).tz('Australia/Sydney');
  const duration = moment.duration(endDate.diff(startDate));

  const sameDay = startDate.isSame(endDate);
  const dateFormat = sameDay ? 'YYYYMMDD' : 'YYYYMMDD\\THHMMss\\Z';

  return {
    ...event,
    startDate,
    endDate,
    duration,
    dateFormat,
  };
}

export default {
  google(event) {
    const preparedEvent = prepare(event);
    const { dateFormat } = preparedEvent;

    const query = {
      action: 'TEMPLATE',
      text: preparedEvent.title,
      dates: `${preparedEvent.startDate.format(dateFormat)}/${preparedEvent.endDate.format(dateFormat)}`,
      details: preparedEvent.description,
      location: preparedEvent.address,
      sprop: [ '', 'name:' ],
    };

    return `https://www.google.com/calendar/render?${querystring.stringify(query)}`;
  },

  yahoo(event) {
    const preparedEvent = prepare(event);
    const { dateFormat } = preparedEvent;

    const query = {
      v: 60,
      view: 'd',
      type: 20,
      title: preparedEvent.title,
      st: preparedEvent.startDate.format(dateFormat),
      dur: preparedEvent.duration.format('hh:mm'),
      desc: preparedEvent.description,
      in_loc: preparedEvent.address,
    };

    return `http://calendar.yahoo.com/?${querystring.stringify(query)}`;
  },

  ics(event) {
    const preparedEvent = prepare(event);
    const { dateFormat } = preparedEvent;

    return encodeURI(
      `data:text/calendar;charset=utf8,` + _.compact([
        'BEGIN:VCALENDAR',
        'VERSION:2.0',
        'BEGIN:VEVENT',
        `DTSTART:${preparedEvent.startDate.format(dateFormat) || ''}`,
        `DTEND:${preparedEvent.endDate.format(dateFormat) || ''}`,
        `SUMMARY:${preparedEvent.title || ''}`,
        `DESCRIPTION:${preparedEvent.description || ''}`,
        `LOCATION:${preparedEvent.address || ''}`,
        'END:VEVENT',
        'END:VCALENDAR',
      ]).join('\n'));
  },
};
