import _ from 'lodash';

export default (strings) => {
  const last = strings.splice(strings.length - 1, 1);
  return _.compact([ strings.join(', '), last ]).join(' & ');
};
