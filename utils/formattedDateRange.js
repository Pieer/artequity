import _ from 'lodash';
import moment from 'moment';

import 'moment-timezone';

export default (startDate, endDate) => {
  const start = startDate && moment(startDate).tz('Australia/Sydney');
  const end = endDate && moment(endDate).tz('Australia/Sydney');

  const sameYear = end && start && start.isSame(end, 'year');
  const featureDates = _.compact([
    start && start.format(sameYear ? 'D MMMM' : 'D MMMM YYYY'),
    end && end.format('D MMMM YYYY'),
  ]);

  return featureDates.join(' \u2014 ');
};
