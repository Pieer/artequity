import winston from 'winston';

const transports = [
  new (winston.transports.Console)({
    colorize: true,
    timestamp: true,
    json: true,
  }),
];

export default new (winston.Logger)({ transports });
