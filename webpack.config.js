require('dotenv').load();

const _ = require('lodash');

const webpack = require('webpack');

const isDevelopment = process.env.NODE_ENV === 'development';
const port = process.env.HOT_LOAD_PORT || 8888;
const path = require('path');

const config = {
  cache: true,
  resolve: {
    extensions: [ '', '.js', '.jsx' ],
  },
  entry: _.compact([
    isDevelopment && 'webpack-dev-server/client?http://localhost:' + port,
    isDevelopment && 'webpack/hot/dev-server',
    './client.js',
  ]),
  output: {
    path: path.join(__dirname, '/public/js'),
    filename: 'client.js',
    publicPath: 'http://localhost:' + port + '/js/',
  },
  plugins: _.compact([
    isDevelopment && new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({ GA_TRACKING_CODE: JSON.stringify(process.env.GA_TRACKING_CODE) }),
  ]),
  module: {
    loaders: [
      { test: /\.(js|jsx)?$/, exclude: /node_modules/, loaders: ['babel-loader?stage=0'] },
      { test: /\.css$/, loader: 'style-loader!css-loader' },
      {include: /\.json$/, loaders: ["json-loader"]}
    ],
  },
};

if (process.env.NODE_ENV === 'development') {
  config.devtool = 'eval'; // This is not as dirty as it looks. It just generates source maps without being crazy slow.
}

module.exports = config;
