import keystone from 'keystone';

import React from 'react';
import cookie from 'react-cookie';
import Router from 'react-router';
import { Resolver, Container } from 'react-resolver';

import FluxibleComponent from 'fluxible-addons-react/FluxibleComponent';
import request from 'superagent';

import serialize from 'serialize-javascript';

import app from '../app';
import Html from '../components/Html.jsx';
import logger from '../logger';
import setConfig from '../actions/setConfig';

import ArtistService from '../services/ArtistService';
import ArtworkService from '../services/ArtworkService';
import DirectorService from '../services/DirectorService';
import ExhibitionService from '../services/ExhibitionService';
import EventService from '../services/EventService';
import LocationService from '../services/LocationService';
import PageService from '../services/PageService';
import PostCategoryService from '../services/PostCategoryService';
import PostService from '../services/PostService';
import PublicationService from '../services/PublicationService';
import RepresentationService from '../services/RepresentationService';
import ServiceService from '../services/ServiceService';
import TagService from '../services/TagService';

export default function(express) {
  const fetchrPlugin = app.getPlugin('FetchrPlugin');
  fetchrPlugin.registerService(ArtistService);
  fetchrPlugin.registerService(ArtworkService);
  fetchrPlugin.registerService(DirectorService);
  fetchrPlugin.registerService(ExhibitionService);
  fetchrPlugin.registerService(EventService);
  fetchrPlugin.registerService(LocationService);
  fetchrPlugin.registerService(PageService);
  fetchrPlugin.registerService(PostCategoryService);
  fetchrPlugin.registerService(PostService);
  fetchrPlugin.registerService(PublicationService);
  fetchrPlugin.registerService(RepresentationService);
  fetchrPlugin.registerService(ServiceService);
  fetchrPlugin.registerService(TagService);
  express.use(fetchrPlugin.getXhrPath(), fetchrPlugin.getMiddleware());

  express.post('/formstack', (req, res) => {
    request
      .post('https://www.formstack.com/forms/index.php')
      .type('form')
      .send(req.body)
      .end((err, result) => {
        if (err) { return res.sendStatus(400); }
        res.send(result);
      });
  });

  express.use((req, res) => {
    const context = app.createContext();

    Router.run(app.getComponent(), req.originalUrl, (Handler) => {
      cookie.setRawCookie(req.headers.cookie);

      context.executeAction(setConfig, {
        name: keystone.get('name'),
        brand: keystone.get('brand'),
        root: keystone.get('root'),
        cloudinary: {
          cloudName: keystone.get('cloudinary config').cloud_name,
        },
        defaultMetaTags: keystone.get('meta tags'),
        formStack: {
          contact: {
            id: process.env.FORM_STACK_ID,
            viewKey: process.env.FORM_STACK_VIEWKEY,
          },
          subscribe: {
            id: process.env.FORM_STACK_SUBSCRIBE_ID,
            viewKey: process.env.FORM_STACK_SUBSCRIBE_VIEWKEY,
          },
        },
      }, () => {
        const component = React.createFactory(Handler);

        const element = React.createElement(
          FluxibleComponent,
          { context: context.getActionContext() },
          component()
        );

        const resolver = new Resolver();
        const container = <Container resolver={resolver}>{element}</Container>;
        React.renderToString(container);

        resolver.finish().then(() => {
          resolver.freeze();

          const exposed = [
            `window.App = ${serialize(app.dehydrate(context))};`,
            `window.resolver = ${serialize(resolver.states)};`,
          ];
          const markup = React.renderToString(container);
          res.send(React.renderToStaticMarkup(<Html state={exposed.join('')} markup={markup} />));
        }).catch((err) => {
          logger.error(err.message, err);
          console.log(err.stack);
          res.sendStatus(500);
        });
      });
    });
  });
}
