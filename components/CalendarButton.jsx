import React, { Component } from 'react';
import { DropdownButton } from 'react-bootstrap';

import classNames from 'classnames';

import calendar from '../utils/calendar';

class CalendarButton extends Component {
  render() {
    const { children, slug } = this.props;

    const calendars = [
      { label: 'Apple iCalendar', icon: 'ion-social-apple', href: calendar.ics(this.props) },
      { label: 'Google', icon: 'ion-social-google', online: true, href: calendar.google(this.props) },
      { label: 'Yahoo', icon: 'ion-social-yahoo', online: true, href: calendar.yahoo(this.props) },
      { label: 'Outlook', icon: 'fa-windows', href: calendar.ics(this.props) },
    ];

    return (
      <DropdownButton {...this.props} title={children}>
        {calendars.map(({ label, icon, online, href }) => (
          <li key={label} role="presentation">
            <a href={href} target="_blank" download={online ? void 0 : `${slug}.ics`}>
              <i className={classNames(icon, 'icon-fixed text-body-default text-lg append-horizontal-xs-tiny')} />{' '}
              <span className="append-horizontal-xs-1">{label} {online && <span className="text-muted text-sm">(online)</span>}</span>
            </a>
          </li>))}
      </DropdownButton>
    );
  }
}

CalendarButton.propTypes = {
  children: React.PropTypes.any.isRequired,
  slug: React.PropTypes.string.isRequired,
  title: React.PropTypes.string,
  startDate: React.PropTypes.any.isRequired,
  endDate: React.PropTypes.any,
  description: React.PropTypes.string,
  address: React.PropTypes.string,
};

export default CalendarButton;
