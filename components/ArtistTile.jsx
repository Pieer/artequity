import React, { Component } from 'react';

import CloudinaryImage from 'react-cloudinary-img';

import HoverBorder from './HoverBorder';

class ArtistTile extends Component {
  render() {
    const { artist } = this.props;

    return (
      <div>
        <a className="link-plain" href={`/artist/${artist.slug}`}>
          {artist.featuredArtwork &&
            <HoverBorder>
              <CloudinaryImage image={artist.featuredArtwork.image} options={{ width: 380 * 2 }} className="img-full-width" />
            </HoverBorder>}
          <h2 className="text-black append-xs-none">{artist.name.full}</h2>
        </a>
      </div>
    );
  }
}

ArtistTile.propTypes = {
  artist: React.PropTypes.object.isRequired,
};

export default ArtistTile;
