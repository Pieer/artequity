import React, { Component } from 'react';

import _ from 'lodash';
import request from 'superagent';

class FormStackForm extends Component {
  render() {
    const { router } = this.context;
    const { children, action } = this.props;

    return (
      <form method="post" action={action} className="append-xs-none" onSubmit={this.submitForm}>
        {_.map(_.omit(this.props, 'children'), (value, key) => <input key={key} type="hidden" name={key} value={value} />)}
        <input type="hidden" name="field35149608" value={router.getCurrentPath()} />
        {children}
      </form>
    );
  }

  submitForm(e) {
    e.preventDefault();
    const { router } = this.context;
    const { action, onSubmit, onError, onSuccess } = this.props;

    const isValid = onSubmit();

    const body = {
      field35149608: router.getCurrentPath(),
      hidden_fields: '',
      _submit: 1,
      incomplete: '',
      ..._.omit(this.props, 'children', 'action', 'onError', 'onSuccess')
    };

    if(isValid){
      request
        .post(action)
        .send(body)
        .end((err, res) => {
          if (err) {
            onError(err);
          } else {
            onSuccess(res);
          }
        });
    }
  }

  constructor() {
    super();
    this.submitForm = this.submitForm.bind(this);
  }
}

FormStackForm.contextTypes = {
  router: React.PropTypes.func.isRequired,
};

FormStackForm.propTypes = {
  form: React.PropTypes.string.isRequired,
  viewkey: React.PropTypes.string.isRequired,
  children: React.PropTypes.any.isRequired,

  action: React.PropTypes.string.isRequired,
  onSubmit: React.PropTypes.func.isRequired,
  onSuccess: React.PropTypes.func.isRequired,
  onError: React.PropTypes.func.isRequired,
};

FormStackForm.defaultProps = {
  action: '/formstack',
};

export default FormStackForm;
