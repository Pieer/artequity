import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import masonryMixin from 'react-masonry-mixin';
import reactMixin from 'react-mixin';

import classNames from 'classnames';

import patchMasonry from '../utils/patchMasonry';

class MasonryTiles extends Component {
  renderTile(item) {
    const { renderTile, colClassName, colSizes } = this.props;

    return (
      <Col key={item.id} {...colSizes}
        className={classNames('prepad-xs-1 pad-horizontal-xs-xtiny pad-horizontal-sm-1', colClassName)}>
        <div className="accent-overlay">
          {renderTile(item)}
        </div>
      </Col>
    );
  }

  render() {
    const { className, title, items, children, filters } = this.props;

    return (
      <Row className={classNames(title ? 'pad-vertical-xs-1' : 'postpad-xs-1', 'bordered--xs bordered--top--xs', className)}>
        {filters}
        <div className="clearfix">
          {title &&
            <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
              \ {title}
            </h2>}
          <div ref="masonryContainer" className={classNames(title && 'prepend-xs-1')}>
            {items.map(this.renderTile)}
          </div>
          {children}
        </div>
      </Row>
    );
  }

  constructor() {
    super();
    this.renderTile = this.renderTile.bind(this);
  }
}

MasonryTiles.propTypes = {
  filters: React.PropTypes.any,
  className: React.PropTypes.string,
  colClassName: React.PropTypes.string,
  title: React.PropTypes.string,
  items: React.PropTypes.array.isRequired,
  renderTile: React.PropTypes.func.isRequired,
  children: React.PropTypes.any,
  colSizes: React.PropTypes.object.isRequired,
};

MasonryTiles.defaultProps = {
  colClassName: 'postpad-xs-tiny',
  colSizes: { xs: 6, md: 4 },
};

reactMixin.onClass(MasonryTiles, patchMasonry(masonryMixin('masonryContainer', { transitionDuration: 0 })));

export default MasonryTiles;
