import React, { Component } from 'react';

import _ from 'lodash';
import CloudinaryImage from 'react-cloudinary-img';

import HoverBorder from './HoverBorder';

class ArtworkTile extends Component {
  render() {
    const { artwork, exhibition, showArtist, isFromDealingFrom } = this.props;
    const info = _.compact([ artwork.year, artwork.medium, artwork.size ]).join(' \\ ');

    let url = `${exhibition ? `/exhibition/${exhibition.slug}` : ''}/artwork/${artwork.slug}`;
    if (isFromDealingFrom) {
      url = `/private-treaty/artwork/${artwork.slug}`;
    }

    return (
      <div className="text-content">
        <a className="link-plain" href={url}>
          <HoverBorder className="append-xs-1">
            <CloudinaryImage image={artwork.image} options={{ width: 380 * 2 }} className="img-full-width" />
          </HoverBorder>
          {showArtist && artwork.artist && artwork.artist.name && <h2 className="text-black prepend-xs-none append-xs-xtiny">{artwork.artist.name.full}</h2>}
          {!showArtist && <p className="text-uppercase text-spaced append-xs-xtiny">{artwork.title}</p>}
        </a>
        {showArtist && <p className="text-uppercase text-spaced append-xs-xtiny">{artwork.title}</p>}
        {info.length > 0 && <p className="append-xs-xtiny">{info}</p>}
        {artwork.price && <p className="append-xs-none">{artwork.price} {artwork.sold && <span className="text-uppercase">Sold</span>}</p>}
      </div>
    );
  }
}

ArtworkTile.propTypes = {
  artwork: React.PropTypes.object.isRequired,
  exhibition: React.PropTypes.object,
  showArtist: React.PropTypes.bool,
  isFromDealingFrom: React.PropTypes.bool,
};

ArtworkTile.defaultProps = {
  showArtist: true,
};

export default ArtworkTile;
