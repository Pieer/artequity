import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import CloudinaryImage from 'react-cloudinary-img';
import reactMixin from 'react-mixin';
import masonryMixin from 'react-masonry-mixin';

import connectToStores from 'fluxible-addons-react/connectToStores';

import createPage from './createPage.jsx';
import loadPostsAction from '../actions/loadPosts';
import loadRepresentedArtistsAction from '../actions/loadRepresentedArtists';

import ArtistStore from '../stores/ArtistStore';
import PostStore from '../stores/PostStore';

import ArtistTile from './ArtistTile.jsx';
import FeatureModule from './FeatureModule.jsx';
import PostTile from './PostTile.jsx';

import formattedDateRange from '../utils/formattedDateRange';
import patchMasonry from '../utils/patchMasonry';

class HomePage extends Component {
  renderFeatureModule() {
    const { page } = this.props;
    const { link, dates } = page.feature;

    const formattedDate = dates && formattedDateRange(dates.start, dates.end);

    return (
      <FeatureModule image={page.feature.image} link={page.feature.link}>
        <div>
          <h1 className="prepend-xs-none append-xs-1 append-md-2">
            {link && <a className="link-plain text-underline" href={link}>{page.feature.title}</a>}
            {!link && page.feature.title}
          </h1>
        </div>
        <h2 className="append-xs-1 append-md-2 text-spaced text-uppercase">
          {page.feature.subtitle}
        </h2>
        {formattedDate &&
          <p className="append-xs-none text-spaced text-uppercase">{formattedDate}</p>}
      </FeatureModule>
    );
  }

  renderIntroduction() {
    const { page, latestPost } = this.props;

    return (
      <Col md={4} className="bordered--xs postpad-xs-1 prepad-xs-1 pad-horizontal-xs-none pad-horizontal-md-1 prepad-md-2">
        <div className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 pad-xs-1 bg-white prepend-md-1 accent-overlay">
          {page.introduction.title &&
            <h1 className="prepend-xs-none h2 text-spaced text-uppercase">
              {page.introduction.title}
            </h1>}
          <div dangerouslySetInnerHTML={{__html: page.introduction.description}}></div>
          {page.introduction.link &&
            <a className="text-black text-underline text-lg" href={page.introduction.link}>About us</a>}
        </div>

        {latestPost &&
          <div className="accent-overlay">
            <PostTile post={latestPost} />
          </div>}
      </Col>
    );
  }

  renderArtistTile(artist) {
    return (
      <Col key={artist.id} xs={6} className="append-xs-3 pad-horizontal-xs-xtiny pad-horizontal-sm-1">
        <div className="accent-overlay">
          <ArtistTile artist={artist} />
        </div>
      </Col>
    );
  }

  renderRepresentedArtists() {
    const { representedArtists } = this.props;

    return (
      <Col md={8} className="bordered--xs bordered--left--md">
        <Row className="prepad-xs-2 postpad-xs-1 postpad-md-2">
          <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
            \ Represented artists
          </h2>
          <div ref="masonryContainer" className="prepend-xs-1">
            {representedArtists.map(this.renderArtistTile)}
            <Col xs={12} md={6} className="text-center-xs text-left-md accent-overlay">
              <a className="btn btn-body-default text-xs text-spaced text-uppercase" href="/artists">View more</a>
            </Col>
          </div>
        </Row>
      </Col>
    );
  }

  renderTopService(topService) {
    return (
      <div className="flex-stretch-md">
        <Col md={4} className="prepad-xs-2 postpad-xs-xtiny postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
          <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
            \ Services
          </h2>
          <CloudinaryImage image={topService.image} options={{ width: 380 * 2, height: 368 * 2, crop: 'fill' }} className="img-full-width prepend-xs-1" />
        </Col>
        <Col md={8} className="bordered--md bordered--centred--md bordered--left--md prepad-md-2 postpad-xs-xtiny postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
          <div className="divider-container pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-xs-1 postpad-xs-2 pad-sm-1 services-box-height-lg bg-white prepend-md-1">
            <Row className="pend-horizontal-xs-none">
              <Col md={4} className="pad-horizontal-xs-none">
                <h1 className="prepend-xs-none append-xs-1 append-sm-none">
                  {topService.title}
                </h1>
              </Col>
              <Col md={8} className="prepad-md-4 pad-horizontal-xs-none">
                <div dangerouslySetInnerHTML={{ __html: topService.description }}></div>
                {topService.details && topService.details.intro &&
                  <a className="text-black text-underline text-lg" href={`/service/${topService.slug}`}>
                    Find out more
                  </a>}
              </Col>
            </Row>
            <span className="divider divider--vertical--xs divider--bottom-left--xs divider--bottom-left--md" />
          </div>
        </Col>
      </div>
    );
  }

  renderServiceBox(service) {
    return (
      <div className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 pad-xs-1 services-box-height-lg bg-white">
        <h2 className="prepend-xs-none prepend-md-2 text-black text-capitalize">
          {service.title}
        </h2>
        <div dangerouslySetInnerHTML={{ __html: service.description }}></div>
      </div>
    );
  }

  renderLeftService(leftService) {
    return (
      <Col md={4} className="postpad-xs-xtiny postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
        {this.renderServiceBox(leftService)}
      </Col>
    );
  }

  renderRightService(rightService) {
    return (
      <Col md={8}>
        <Row>
          <Col md={6} className="postpad-xs-xtiny postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
            <CloudinaryImage image={rightService.image} options={{ width: 380 * 2, height: 368 * 2, crop: 'fill' }} className="img-full-width" />
          </Col>
          <Col md={6} className="postpad-xs-1 pad-horizontal-xs-none pad-horizontal-md-1">
            {this.renderServiceBox(rightService)}
          </Col>
        </Row>
      </Col>
    );
  }

  renderServices() {
    const { page } = this.props;
    const [ topService, leftService, rightService ] = page.services;

    return (
      <div>
        <Row>
          {topService && this.renderTopService(topService)}
        </Row>
        <Row className="flex-stretch-md">
          {leftService && this.renderLeftService(leftService)}
          {rightService && this.renderRightService(rightService)}
        </Row>
        <Row>
          <Col md={8} mdPush={4} className="clearfix postpad-xs-1">
            <Row>
              <Col md={6} className="text-center">
                <a className="btn btn-body-default text-xs text-spaced text-uppercase" href="/services">
                  All services
                </a>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    return (
      <div>
        <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
          {this.renderFeatureModule()}
        </Grid>

        <div className="accent-container">
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            <Row className="bordered--xs bordered--top--xs divider-container flex-stretch-md">
              {this.renderIntroduction()}
              {this.renderRepresentedArtists()}
              <span className="divider divider--horizontal--xs divider--bottom-right--xs divider--bottom-right--md" />
            </Row>
          </Grid>
          <div className="accent bg-striped"></div>
        </div>

        <Grid className="bordered--xs bordered--left--xs bordered--top--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
          {this.renderServices()}
        </Grid>
      </div>
    );
  }

  constructor() {
    super();
    this.renderFeatureModule = this.renderFeatureModule.bind(this);
  }
}

HomePage.propTypes = {
  page: React.PropTypes.object.isRequired,
  representedArtists: React.PropTypes.array.isRequired,
  latestPost: React.PropTypes.object,
};

reactMixin.onClass(HomePage, patchMasonry(masonryMixin('masonryContainer', { transitionDuration: 0 })));

HomePage = connectToStores(HomePage, [ ArtistStore, PostStore ], (context) => {
  return {
    representedArtists: context.getStore(ArtistStore).getRepresentedArtists(),
    latestPost: context.getStore(PostStore).getPosts()[0],
  };
});

export default createPage(HomePage, {
  representedArtists: (props, context) => {
    return context.executeAction(loadRepresentedArtistsAction, 5);
  },

  latestPost: (props, context) => {
    return context.executeAction(loadPostsAction, { count: 1 });
  },
});
