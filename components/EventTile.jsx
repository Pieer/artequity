import React, { Component } from 'react';

import CloudinaryImage from 'react-cloudinary-img';
import moment from 'moment';
import 'moment-timezone';

class EventTile extends Component {
  render() {
    const { event } = this.props;
    const url = `/event/${event.slug}`;

    return (
      <div className="bg-white pad-horizontal-xs-tiny pad-vertical-xs-1 postmad-md-2 pad-horizontal-md-1">
        <a href={url}>
          <CloudinaryImage image={event.thumbnailImage} options={{ width: 380 * 2 }} className="img-full-width" />
        </a>
        <h2 className="text-spaced text-uppercase append-xs-1">
          <a className="link-plain" href={url}>{event.title}</a>
        </h2>
        <h5 className="prepend-xs-none text-spaced text-uppercase append-xs-1">{moment(event.date).tz('Australia/Sydney').format('dddd D MMMM')}</h5>
        <a className="text-black text-underline text-lg" href={url}>
          Read more
        </a>
      </div>
    );
  }
}

EventTile.propTypes = {
  event: React.PropTypes.object.isRequired,
};

export default EventTile;
