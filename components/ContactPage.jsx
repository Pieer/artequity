import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import CloudinaryImage from 'react-cloudinary-img';

import setBodyThemeAction from '../actions/setBodyTheme';

import ContactForm from './ContactForm.jsx';
import createPage from './createPage.jsx';

class ContactPage extends Component {
  renderMap() {
    const { page } = this.props;

    return (
      <Col md={8} className="pad-horizontal-xs-none pad-xs-1 postpad-vertical-xs-none pad-md-1">
        <CloudinaryImage image={page.map} options={{ width: 820 * 2 }} className="img-full-width" />
      </Col>
    );
  }

  renderInfo() {
    const { page } = this.props;

    return (
      <Col md={4} className="divider-container pad-horizontal-xs-xtiny pad-horizontal-sm-1 pad-horizontal-lg-2 prepad-vertical-xs-1 postpad-vertical-xs-1 pad-vertical-md-3 pad-vertical-lg-5 text-center-xs text-left-md">
        <h3 className="text-black prepend-xs-none">
          {page.address.number}<br/>
          {page.address.street1}
        </h3>
        <h1>{page.address.suburb}</h1>
        <p className="pre">
          <span className="text-uppercase">Gallery Hours</span><br/>
          {page.hours}
        </p>
        <a className="btn btn-body-default text-xs text-spaced text-uppercase" href="#contact">
          Get in touch
        </a>
        <span className="divider divider--horizontal--xs divider--top-left--xs divider--top-left--sm" />
        <span className="divider divider--vertical--xs divider--bottom-right--xs divider--bottom-right--sm" />
      </Col>
    );
  }

  render() {
    return (
      <div>
        <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md ">
          <Row className="flex-center-md">
            {this.renderMap()}
            {this.renderInfo()}
          </Row>
        </Grid>
        <div id="contact">
          <ContactForm />
        </div>
      </div>
    );
  }
}

ContactPage.propTypes = {
  page: React.PropTypes.object.isRequired,
};

export default createPage(ContactPage, {
  bodyTheme: (props, context) => {
    return context.executeAction(setBodyThemeAction, 'soft');
  },
});
