import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import createPage from './createPage.jsx';

class PrivacyPage extends Component {
  render() {
    const { page } = this.props;

    return (
      <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md ">
        <Row className="flex-stretch-md">
          <Col md={4} className="divider-container prepad-vertical-xs-1 postpad-vertical-xs-1 prepad-vertical-sm-3 prepad-vertical-md-4 postpad-vertical-md-3 pad-horizontal-xs-xtiny pad-horizontal-sm-1">
            <h1 className="pend-xs-none">{page.title}</h1>
            <span className="divider divider--vertical--xs divider--top-right--xs divider--top-left--sm divider--top-left--md" />
            <span className="divider divider--horizontal--md divider--bottom-right--md" />
          </Col>
          <Col md={8} className="divider-container pad-vertical-xs-1 pad-horizontal-xs-none pad-horizontal-md-1">
            <div className="pad-horizontal-xs-xtiny pad-xs-1 pad-sm-3 bg-white text-content">
              <div dangerouslySetInnerHTML={{__html: page.content}}></div>
            </div>
            <span className="divider divider--horizontal--xs divider--bottom-left--xs hidden-md hidden-lg" />
          </Col>
        </Row>
      </Grid>
    );
  }
}

PrivacyPage.propTypes = {
  page: React.PropTypes.object.isRequired,
};

export default createPage(PrivacyPage);
