import React, { Component } from 'react';
import { Grid, Col } from 'react-bootstrap';

import _ from 'lodash';
import connectToStores from 'fluxible-addons-react/connectToStores';

import createPage from './createPage.jsx';

import loadArtworksAction from '../actions/loadArtworks';
import loadMoreArtworksAction from '../actions/loadMoreArtworks';
import loadTagsAction from '../actions/loadTags';
import setBodyThemeAction from '../actions/setBodyTheme';

import ArtworkStore from '../stores/ArtworkStore';
import TagStore from '../stores/TagStore';

import ArtworkTile from './ArtworkTile';
import Introduction from './Introduction';
import MasonryTiles from './MasonryTiles';

const artworksPerPage = 18;

class PrivateTreatyPage extends Component {
  renderArtworkTiles() {
    const { artworks, loadingMoreArtworks, allArtworksLoaded } = this.props;

    return (
      <MasonryTiles items={artworks}
        renderTile={(artwork) => <ArtworkTile artwork={artwork} showArtist={true} isFromDealingFrom={true} />}>
        {!allArtworksLoaded &&
          <Col md={12} className="text-center prepend-xs-1">
            <button
              className="btn btn-body-default text-xs text-spaced text-uppercase prepend-xs-none"
              type="button"
              disabled={loadingMoreArtworks}
              onClick={this.loadMoreArtworks}>
              {loadingMoreArtworks ? 'Loading...' : 'View more'}
            </button>
          </Col>}
      </MasonryTiles>
    );
  }

  render() {
    const { page, artworks } = this.props;

    return (
      <div>
        <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs boredered--centred--md--none bordered--third--md">
          <Introduction {...page.introduction} className="" />
        </Grid>
        {_.any(artworks) &&
          <div className="accent-container">
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs boredered--centred--md--none bordered--third--md">
              {this.renderArtworkTiles()}
            </Grid>
            <div className="accent bg-striped"></div>
          </div>}
      </div>
    );
  }

  loadMoreArtworks() {
    const { activeTag } = this.props;
    this.context.executeAction(loadMoreArtworksAction, {
      count: artworksPerPage,
      dealingRoom: true,
      tags: _.compact([ activeTag ]),
    });
  }

  constructor() {
    super();
    this.loadMoreArtworks = this.loadMoreArtworks.bind(this);
  }
}

PrivateTreatyPage.contextTypes = {
  executeAction: React.PropTypes.func.isRequired,
};

PrivateTreatyPage.propTypes = {
  page: React.PropTypes.object.isRequired,
  artworks: React.PropTypes.array.isRequired,
  activeTag: React.PropTypes.string,
  tags: React.PropTypes.array.isRequired,
  loadingMoreArtworks: React.PropTypes.bool.isRequired,
  allArtworksLoaded: React.PropTypes.bool.isRequired,
};

PrivateTreatyPage = connectToStores(PrivateTreatyPage, [ ArtworkStore, TagStore ], (context) => {
  const artworkStore = context.getStore(ArtworkStore);

  return {
    artworks: artworkStore.getArtworks(),
    tags: context.getStore(TagStore).getTags(),
    loadingMoreArtworks: artworkStore.isLoadingMoreArtworks(),
    allArtworksLoaded: artworkStore.isAllArtworksLoaded(),
  };
});

export default createPage(PrivateTreatyPage, {
  bodyTheme: (props, context) => {
    return context.executeAction(setBodyThemeAction, 'dark');
  },

  activeTag: (props) => {
    return props.params.tag;
  },

  artworks: (props, context) => {
    return context.executeAction(loadArtworksAction, { count: artworksPerPage, dealingRoom: true, tags: _.compact([ props.params.tag ]) });
  },

  tags: (props, context) => {
    return context.executeAction(loadTagsAction, { hideInDealingRoom: true });
  },
}, (path) => `/${path.split('/')[1]}`);
