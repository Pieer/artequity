import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

class Introduction extends Component {
  render() {
    const { title, description, className } = this.props;
    return (
      <Row className={className}>
        <div className="clearfix">
          <Col md={4} className="pad-horizontal-xs-xtiny prepad-vertical-xs-1 postpad-vertical-xs-none pad-horizontal-sm-1 pad-md-1">
            <h2 className="pend-vertical-sm-1 prepend-xs-none">
              \ {title}
            </h2>
          </Col>
          <Col md={8} className="pend-vertical-sm-1 append-vertical-xs-none pad-horizontal-xs-xtiny prepad-vertical-xs-none postpad-vertical-xs-1 pad-horizontal-sm-1 pad-md-1 text-content">
            <div dangerouslySetInnerHTML={{ __html: description }} />
          </Col>
        </div>
      </Row>
    );
  }
}

Introduction.propTypes = {
  title: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  className: React.PropTypes.string,
};

Introduction.defaultProps = {
  className: 'bordered--xs bordered--bottom--xs',
};

export default Introduction;
