import React, { Component } from 'react';

import connectToStores from 'fluxible-addons-react/connectToStores';

import PageStore from '../stores/PageStore';

class NumberRevealer extends Component {
  render() {
    const { contactInfo } = this.props;
    const { showPhoneNumber } = this.state;

    return (
      <span>
        {showPhoneNumber &&
          <a className="text-black text-underline h4" href={`tel:${contactInfo.phone}`}>
            {contactInfo.phone}
          </a>}
        {!showPhoneNumber &&
          <a className="text-black text-underline h4" href="#" onClick={this.togglePhoneNumber}>
            Show phone number
          </a>}
      </span>
    );
  }

  togglePhoneNumber(e) {
    this.setState({ showPhoneNumber: !this.state.showPhoneNumber });
    e.preventDefault();
  }

  constructor() {
    super();
    this.state = { showPhoneNumber: false };
    this.togglePhoneNumber = this.togglePhoneNumber.bind(this);
  }
}

NumberRevealer.propTypes = {
  contactInfo: React.PropTypes.object.isRequired,
};

NumberRevealer = connectToStores(NumberRevealer, [ PageStore ], (context) => {
  return {
    contactInfo: context.getStore(PageStore).getContactInfo(),
  };
});

export default NumberRevealer;
