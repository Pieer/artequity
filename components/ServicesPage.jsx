import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import _ from 'lodash';
import classNames from 'classnames';
import CloudinaryImage from 'react-cloudinary-img';
import connectToStores from 'fluxible-addons-react/connectToStores';

import createPage from './createPage.jsx';

import loadEventsAction from '../actions/loadEvents';
import loadMoreEventsAction from '../actions/loadMoreEvents';
import loadServicesAction from '../actions/loadServices';
import setBodyThemeAction from '../actions/setBodyTheme';

import EventStore from '../stores/EventStore';
import ServiceStore from '../stores/ServiceStore';

import ContactForm from './ContactForm';
import EventTile from './EventTile';
import Introduction from './Introduction';
import MasonryTiles from './MasonryTiles';

const eventsPerPage = 6;

class ServicesPage extends Component {
  renderService(service, index) {
    const { services } = this.props;
    const isLast = services.length - 1 === index;

    const imageWidth = { wide: 820, square: 380 }[service.imageMode];
    const imageColumns = { wide: 8, square: 4 }[service.imageMode];
    const imageElement = <CloudinaryImage image={service.image} options={{ width: imageWidth * 2, height: 370 * 2, crop: 'fill' }} className="img-full-width" />;

    const dividerOrientation = index % 4 === 0 ? 'vertical' : 'horizontal';
    const dividerOffset = index % 2 === 0 ? 8 : 0;
    const dividerPosition = index % 2 === 0 ? 'bottom-left' : 'bottom-right';

    return (
      <div>
        <Row className="flex-stretch-md">
          <Col
            md={imageColumns} mdPush={service.alignment === 'left' ? 4 : void 0}
            mdOffset={service.alignment === 'right' && service.imageMode === 'square' ? 4 : void 0}
            className="pad-horizontal-xs-none postpad-vertical-xs-1 pad-horizontal-md-1">
            {imageElement}
          </Col>
          <Col
            md={4} mdPull={service.alignment === 'left' ? imageColumns : void 0}
            className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-lg-1 postpad-vertical-md-2 postpad-vertical-xs-1">
            <h1 className="prepend-xs-none">{service.title}</h1>
            <div className="text-content" dangerouslySetInnerHTML={{__html: service.description}} />

            {service.details && service.details.intro &&
              <div className="prepend-xs-1 prepend-sm-2">
                <a className="text-black text-underline text-lg" href={`/service/${service.slug}`}>
                  Read more
                </a>
              </div>}
          </Col>
        </Row>
        {!isLast &&
          <Row className="hidden-xs hidden-sm">
            <Col md={4} mdOffset={dividerOffset} className="divider-container postpad-vertical-md-1">
               <span className={classNames('divider', `divider--${dividerOrientation}--md`, `divider--${dividerPosition}--md`)} />
            </Col>
          </Row>}
      </div>
    );
  }

  renderServices() {
    const { services } = this.props;
    return (
      <div className="prepad-xs-1">
        {services.map(this.renderService)}
      </div>
    );
  }

  renderEventTiles() {
    const { events, loadingMoreEvents, allEventsLoaded } = this.props;
    return (
      <div id="events">
        <MasonryTiles title="Upcoming Events" items={events}
          colSizes={{ xs: 12, md: 6, lg: 4 }}
          colClassName="postpad-vertical-xs-none"
          renderTile={(event) => <EventTile event={event}/>}>
          {!allEventsLoaded &&
            <Col md={12} className="text-center prepend-xs-1">
              <button
                className="btn btn-body-default text-xs text-spaced text-uppercase prepend-xs-none"
                type="button"
                disabled={loadingMoreEvents}
                onClick={this.loadMoreEvents}>
                {loadingMoreEvents ? 'Loading...' : 'View more'}
              </button>
            </Col>}
        </MasonryTiles>
      </div>
    );
  }

  renderQuote() {
    const { page } = this.props;

    return (
      <Row className="bordered--xs bordered--top--xs">
        <Col md={8} mdPush={4} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-xs-1 pad-md-1">
          <blockquote className="pend-md-1 pend-xs-none">{page.quote.text}</blockquote>
        </Col>
        <Col md={4} mdPull={8} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-xs-xtiny postpad-vertical-xs-1 pad-md-1 text-right">
          <h2 className="text-black pend-md-1 pend-xs-none">{page.quote.author}</h2>
        </Col>
      </Row>
    );
  }

  render() {
    const { page, services, events } = this.props;

    return (
      <div>
        <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
          <Introduction {...page.introduction} />
          {_.any(services) && this.renderServices()}
        </Grid>
        {_.any(events) &&
          <div className="accent-container">
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--third--lg">
              {this.renderEventTiles()}
            </Grid>
            <div className="accent bg-striped"></div>
          </div>}
        {page.quote && page.quote.author && page.quote.text &&
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            {this.renderQuote()}
          </Grid>}
        <ContactForm contextDescription={'to find out more about what we offer.'} />
      </div>
    );
  }

  loadMoreEvents() {
    this.context.executeAction(loadMoreEventsAction, { count: eventsPerPage });
  }

  constructor() {
    super();
    this.renderService = this.renderService.bind(this);
    this.loadMoreEvents = this.loadMoreEvents.bind(this);
  }
}

ServicesPage.contextTypes = {
  executeAction: React.PropTypes.func.isRequired,
};

ServicesPage.propTypes = {
  page: React.PropTypes.object.isRequired,
  services: React.PropTypes.array.isRequired,
  events: React.PropTypes.array.isRequired,
  loadingMoreEvents: React.PropTypes.bool.isRequired,
  allEventsLoaded: React.PropTypes.bool.isRequired,
};

ServicesPage = connectToStores(ServicesPage, [ ServiceStore, EventStore ], (context) => {
  const eventStore = context.getStore(EventStore);

  return {
    services: context.getStore(ServiceStore).getServices(),
    events: eventStore.getEvents(),
    loadingMoreEvents: eventStore.isLoadingMoreEvents(),
    allEventsLoaded: eventStore.isAllEventsLoaded(),
  };
});

export default createPage(ServicesPage, {
  bodyTheme: (props, context) => {
    return context.executeAction(setBodyThemeAction, 'danger');
  },

  services: (props, context) => {
    return context.executeAction(loadServicesAction);
  },

  events: (props, context) => {
    return context.executeAction(loadEventsAction, { count: eventsPerPage });
  },
});
