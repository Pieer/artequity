import React, { Component } from 'react';

class NativeComboBox extends Component {
  render() {
    const { children, options } = this.props;

    const selectStyle = {
      bottom: 0,
      height: '100%',
      left: 0,
      opacity: 0,
      position: 'absolute',
      right: 0,
      top: 0,
      width: '100%',
      zIndex: 1,
      WebkitAppearance: 'menulist-button',
    };

    return (
      <div style={{ position: 'relative' }}>
        {children}
        <select {...this.props} style={selectStyle}>
          {options}
        </select>
      </div>
    );
  }
}

NativeComboBox.propTypes = {
  children: React.PropTypes.any.isRequired,
  options: React.PropTypes.array.isRequired,
};

export default NativeComboBox;
