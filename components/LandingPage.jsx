import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { Resolver } from 'react-resolver';

import classNames from 'classnames';
import connectToStores from 'fluxible-addons-react/connectToStores';

import loadLandingInfoAction from '../actions/loadLandingInfo';
import setLandingAction from '../actions/setLanding';

import LandingStore from '../stores/LandingStore';

import Logo from './Logo.jsx';

class LandingPage extends Component {
  componentDidMount() {
    setInterval(() => {
      this.setState({ frame: (this.state.frame + 1) % 4 });
    }, 2000);
  }

  render() {
    const { info, isLanding } = this.props;
    const { title, description } = info;
    const { frame } = this.state;

    return (
      <div className={classNames('landing text-center clickable', isLanding ? 'landing--visible' : 'landing--hidden')} onClick={this.hide}>
        <Grid>
          <Row className="pad-vertical-xs-1" />
        </Grid>
        <Grid>
          <Row>
            <Col md={8} mdOffset={2}>
              <div className="divider-container divider-container--inline-block">
                <Logo className="btn-logo" link={null} onClick={this.hide} />
                <span className={classNames('divider divider--horizontal--xs divider--landing-1', `f${frame + 1}`)} />
                <span className={classNames('divider divider--vertical--xs divider--landing-2', `f${frame + 1}`)} />
                <span className={classNames('divider divider--tilt divider--vertical--xs divider--landing-3', `f${frame + 1}`)} />
              </div>
              {title && <h2 className="text-spaced text-uppercase prepend-xs-3 prepend-md-6 append-xs-1">{title}</h2>}
              <div dangerouslySetInnerHTML={{__html: description}} className="text-content"></div>
            </Col>
          </Row>
        </Grid>
        <Grid className="text-center">
          <Row className="pad-vertical-xs-1">
            <button className="btn btn-hollow btn-hollow-default btn-wide text-xs text-spaced text-uppercase prepend-xs-6" onClick={this.hide}>
              Continue
            </button>
          </Row>
        </Grid>
      </div>
    );
  }

  hide(e) {
    this.context.executeAction(setLandingAction, false);
    e.preventDefault();
  }

  constructor() {
    super();
    this.hide = this.hide.bind(this);
    this.state = { frame: 0 };
  }
}

LandingPage.contextTypes = {
  executeAction: React.PropTypes.func.isRequired,
};

LandingPage.propTypes = {
  info: React.PropTypes.shape({
    title: React.PropTypes.string.isRequired,
    description: React.PropTypes.string.isRequired,
  }),
  isLanding: React.PropTypes.bool,
};

LandingPage = connectToStores(LandingPage, [ LandingStore ], (context) => {
  return {
    info: context.getStore(LandingStore).getInfo(),
    isLanding: context.getStore(LandingStore).isLanding(),
  };
});

LandingPage = Resolver.createContainer(LandingPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    info: (props, context) => {
      return context.executeAction(loadLandingInfoAction);
    },
  },
});

export default LandingPage;
