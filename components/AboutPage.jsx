import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import _ from 'lodash';
import classNames from 'classnames';
import CloudinaryImage from 'react-cloudinary-img';
import connectToStores from 'fluxible-addons-react/connectToStores';

import createPage from './createPage.jsx';
import loadDirectorsAction from '../actions/loadDirectors';
import loadLocationsAction from '../actions/loadLocations';

import DirectorStore from '../stores/DirectorStore';
import LocationStore from '../stores/LocationStore';

import ContactForm from './ContactForm';
import Introduction from './Introduction';

class AboutPage extends Component {
  renderFeature() {
    const { page } = this.props;
    const imageElement = <CloudinaryImage image={page.feature.image} options={{ width: 820 * 2, height: 370 * 2, crop: 'fill' }} className="img-full-width" />;

    return (
      <div>
        <Row className="divider-container postpad-vertical-xs-1 postpad-md-none flex-stretch-md">
          <Col md={4} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-xs-1 postpad-vertical-xs-1 postpad-vertical-md-3 prepad-vertical-sm-3 prepad-vertical-lg-4">
            <h1 className="pend-vertical-xs-none">{page.feature.title}</h1>
            <span className="divider divider--vertical--xs divider--top-right--xs divider--top-left--sm" />
          </Col>
          <Col md={8} className="pad-horizontal-xs-none pad-md-1">
            {imageElement}
          </Col>
        </Row>
        <Row className="text-col-3-md">
          <Col md={12} className="postpad-vertical-xs-tiny postpad-vertical-sm-none pad-horizontal-xs-xtiny pad-horizontal-sm-1">
            <div dangerouslySetInnerHTML={{ __html: page.feature.description }} />
          </Col>
        </Row>
        <Row>
          <Col md={4} mdOffset={8} className="divider-container prepad-vertical-md-2">
            <span className="divider divider--vertical--md divider--bottom-left--md" />
          </Col>
        </Row>
      </div>
    );
  }

  renderDirector(director) {
    return (
      <Col key={director.id} sm={6} className="pad-horizontal-xs-none postpad-vertical-xs-1 pad-horizontal-sm-1">
        <CloudinaryImage image={director.photo} options={{ width: 380 * 2, crop: 'fill' }} className="img-full-width append-xs-1" />
        <div className="pad-horizontal-xs-xtiny pad-horizontal-sm-none">
          <h3 className="text-black prepend-xs-none append-xs-xtiny prepad-vertical-xs-xtiny">
            {director.name.full}
          </h3>
          <h4 className="text-spaced text-uppercase text-sm prepend-xs-none">
            {director.title}
          </h4>
          <div className="text-content" dangerouslySetInnerHTML={{ __html: director.bio }} />
        </div>
      </Col>
    );
  }

  renderDirectors() {
    const { directors } = this.props;

    return (
      <div>
        <Row className="postpad-vertical-md-1 postpad-md-none bordered--xs bordered--top--xs bordered--md--none">
          <Col md={4} className="divider-container pad-horizontal-xs-xtiny pad-horizontal-sm-1 pad-vertical-xs-1 pad-vertical-md-3 prepad-vertical-md-none">
            <h1 className="pend-vertical-xs-none append-md-3">Directors</h1>
            <span className="divider divider--horizontal--xs divider--top-right--xs divider--horizontal--sm divider--top-right--sm divider--vertical--md divider--bottom-left--md" />
          </Col>
          <Col md={8} className="prepad-vertical-xs-none postpad-vertical-md-1">
            <Row>
              {directors.map(this.renderDirector)}
            </Row>
          </Col>
        </Row>
        <Row className="divider-container">
          <Col md={4} mdOffset={4} className="divider-container prepad-vertical-md-1 prepend-vertical-md-1">
            <span className="divider divider--horizontal--md divider--bottom-right--md" />
          </Col>
          <span className="divider divider--horizontal--xs divider--bottom-left--xs visible-xs" />
        </Row>
      </div>
    );
  }

  renderLocation(location, index) {
    const { locations } = this.props;
    const isLast = index === locations.length - 1;

    let containerStyle;

    if (location.imageMode === 'background' && location.image && location.image.url) {
      containerStyle = {
        backgroundImage: `url(${location.image.url})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundAttachment: 'fixed',
        backgroundRepeat: 'no-repeat',
      };
    }

    return (
      <div key={location.id} className="bg-none-xs bg-none-sm" style={containerStyle}>
        <Grid className="divider-container bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
          <Row className="bordered--xs bordered--top--xs bordered--md--none prepad-vertical-xs-1 prepad-vertical-md-none">
            {location.image && location.image.public_id &&
              <Col md={4} className={classNames('pad-horizontal-xs-none pad-horizontal-md-1 postpad-vertical-xs-1', location.imageMode === 'background' && 'hidden-md hidden-lg')}>
                <CloudinaryImage
                  image={location.image}
                  options={{ width: 380 * 2, height: 370 * 2, crop: 'fill' }}
                  className="img-full-width" />
              </Col>}
            <Col
              md={location.imageMode === 'background' ? 4 : 8}
              mdOffset={location.imageMode === 'background' ? 8 : 0}
              className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-md-none pad-vertical-lg-1 postpad-vertical-xs-1">
              <h1 className="prepend-xs-none">{location.name}</h1>
              <div className="text-content" dangerouslySetInnerHTML={{__html: location.description}} />
            </Col>
          </Row>
          <Row className="divider-container">
            <Col md={4} mdOffset={4} className="divider-container prepad-vertical-md-1 prepend-vertical-md-2">
              <span className={classNames('divider divider--vertical--md', `divider--bottom-${index % 2 === 0 ? 'right' : 'left'}--md`)} />
            </Col>
          </Row>
          {isLast && <span className="divider divider--horizontal--xs divider--bottom-right--xs divider--bottom-right--md hidden-md hidden-lg" />}
        </Grid>
      </div>
    );
  }

  renderLocations() {
    const { locations } = this.props;
    return locations.map(this.renderLocation);
  }

  render() {
    const { page, directors, locations } = this.props;

    return (
      <div>
        <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
          <Introduction {...page.introduction} />
          {this.renderFeature()}
          {_.any(directors) && this.renderDirectors()}
        </Grid>
        {_.any(locations) && this.renderLocations()}
        <ContactForm contextDescription={'to find out more.'} />
      </div>
    );
  }

  constructor() {
    super();
    this.renderLocation = this.renderLocation.bind(this);
  }
}

AboutPage.propTypes = {
  page: React.PropTypes.object.isRequired,
  directors: React.PropTypes.array.isRequired,
  locations: React.PropTypes.array.isRequired,
};

AboutPage = connectToStores(AboutPage, [ DirectorStore, LocationStore ], (context) => {
  return {
    directors: context.getStore(DirectorStore).getDirectors(),
    locations: _.filter(context.getStore(LocationStore).getLocations(), (location) => !!location.description),
  };
});

export default createPage(AboutPage, {
  directors: (props, context) => {
    return context.executeAction(loadDirectorsAction);
  },

  locations: (props, context) => {
    return context.executeAction(loadLocationsAction, true);
  },
});
