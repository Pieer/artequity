import React, { Component } from 'react';

import _ from 'lodash';
import CloudinaryImage from 'react-cloudinary-img';
import moment from 'moment';
import 'moment-timezone';

class PostTile extends Component {
  render() {
    const { post, linkToAll } = this.props;
    const url = `/post/${post.slug}`;

    return (
      <div className="bg-white pad-horizontal-xs-tiny pad-vertical-xs-1 postmad-md-2 pad-horizontal-md-1">
        <div className="label-container">
          <a href={url}>
              <CloudinaryImage image={post.image} options={{ width: 380 * 2 }} className="img-full-width" />
            </a>
            {_.any(post.categories) &&
              <span className="label-item bg-white pad-vertical-xs-xtiny pad-horizontal-xs-tiny inline-block">
                {_.pluck(post.categories, 'name').join(', ')}
              </span>}
        </div>
        <h2 className="text-spaced text-uppercase append-xs-1">
          <a className="link-plain" href={url}>{post.title}</a>
        </h2>
        <h5 className="prepend-xs-none text-spaced text-uppercase append-xs-1">Posted: {moment(post.publishedDate).tz('Australia/Sydney').format('D MMMM YYYY')}</h5>
        <div dangerouslySetInnerHTML={{ __html: post.content.brief }} />
        <a className="text-black text-underline text-lg" href={url}>
          Read more
        </a>
        {linkToAll &&
          <div className="prepend-xs-2 text-center-xs text-left-md">
            <a className="btn btn-body-default text-xs text-spaced text-uppercase" href="/posts">
              More news
            </a>
          </div>}
      </div>
    );
  }
}

PostTile.propTypes = {
  post: React.PropTypes.object.isRequired,
  linkToAll: React.PropTypes.bool,
};

export default PostTile;
