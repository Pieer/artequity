import React, { Component } from 'react';
import { Grid, Col } from 'react-bootstrap';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import classNames from 'classnames';
import connectToStores from 'fluxible-addons-react/connectToStores';
import moment from 'moment';
import 'moment-timezone';

import loadPostCategoriesAction from '../actions/loadPostCategories';
import loadPostsAction from '../actions/loadPosts';
import loadMorePostsAction from '../actions/loadMorePosts';
import setBodyThemeAction from '../actions/setBodyTheme';

import PostCategoryStore from '../stores/PostCategoryStore';
import PostStore from '../stores/PostStore';

import PostTile from './PostTile';
import FeatureModule from './FeatureModule';
import MasonryTiles from './MasonryTiles';

const postsPerPage = 18;

class PostsPage extends Component {
  renderFeatureModule() {
    const { featuredPost: post } = this.props;
    const link = `/post/${post.slug}`;

    return (
      <FeatureModule image={post.image} link={link}>
        <div>
          <h2 className="prepend-xs-1 append-xs-1">
            <a className="link-plain text-spaced text-uppercase" href={link}>
              {post.title}
            </a>
          </h2>
        </div>
        {post.publishedDate &&
          <p className="text-spaced text-uppercase append-xs-1">Posted: {moment(post.publishedDate).tz('Australia/Sydney').format('D MMMM YYYY')}</p>}
        <div dangerouslySetInnerHTML={{ __html: post.content.brief }} />
        <div className="prepend-xs-3">
          <a className="text-black text-underline text-lg" href={link}>
            Read more
          </a>
        </div>
      </FeatureModule>
    );
  }

  renderCategory(category) {
    const { activePostCategory } = this.props;

    return (
      <li key={category.slug} className="append-horizontal-xs-1 append-horizontal-md-none prepend-horizontal-md-2">
        <a href={`/posts/${category.slug}`}
          className={classNames(activePostCategory !== category.slug ? 'link-plain text-black' : 'text-black text-underline')}>
          {category.name}
        </a>
      </li>
    );
  }

  renderCategories() {
    const { activePostCategory, postCategories } = this.props;

    return (
      <ul className="list-inline list-inline--compact append-xs-none">
        <li className="append-horizontal-xs-1 append-horizontal-md-none prepend-horizontal-md-2">
          <a href="/posts"
            className={classNames(activePostCategory ? 'link-plain text-black' : 'text-black text-underline')}>
            All
          </a>
        </li>
        {postCategories.map(this.renderCategory)}
      </ul>
    );
  }

  renderPostTiles() {
    const { posts, loadingMorePosts, allPostsLoaded } = this.props;

    const filters = (
      <Col md={12} className="pad-horizontal-xs-xtiny prepad-vertical-xs-1 pad-horizontal-sm-1 pad-horizontal-md-1 prepad-vertical-md-1 text-right-md">
        {this.renderCategories()}
      </Col>
    );

    return (
      <MasonryTiles items={posts}
        renderTile={(post) => <PostTile post={post} />} filters={filters}
        colSizes={{ xs: 12, md: 6, lg: 4 }}
        colClassName="postpad-vertical-xs-none">
        {!allPostsLoaded &&
          <Col md={12} className="text-center prepend-xs-1">
            <button
              className="btn btn-body-default text-xs text-spaced text-uppercase prepend-xs-none"
              type="button"
              disabled={loadingMorePosts}
              onClick={this.loadMorePosts}>
              {loadingMorePosts ? 'Loading...' : 'View more'}
            </button>
          </Col>}
      </MasonryTiles>
    );
  }

  render() {
    const { config } = this.context;
    const { posts } = this.props;

    return (
      <DocumentTitle title={`News - ${config.brand}`}>
        <div>
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs boredered--centred--md--none bordered--third--md">
            {this.renderFeatureModule()}
          </Grid>
          {_.any(posts) &&
            <div className="accent-container">
              <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--third--lg">
                {this.renderPostTiles()}
              </Grid>
              <div className="accent bg-striped"></div>
            </div>}
        </div>
      </DocumentTitle>
    );
  }

  loadMorePosts() {
    const { activePostCategory } = this.props;
    this.context.executeAction(loadMorePostsAction, { count: postsPerPage, categories: _.compact([ activePostCategory ]) });
  }

  constructor() {
    super();

    this.renderCategory = this.renderCategory.bind(this);
    this.loadMorePosts = this.loadMorePosts.bind(this);
  }
}

PostsPage.contextTypes = {
  config: React.PropTypes.object.isRequired,
  executeAction: React.PropTypes.func.isRequired,
};

PostsPage.propTypes = {
  featuredPost: React.PropTypes.object.isRequired,
  posts: React.PropTypes.array.isRequired,
  loadingMorePosts: React.PropTypes.bool.isRequired,
  allPostsLoaded: React.PropTypes.bool.isRequired,
  activePostCategory: React.PropTypes.string,
  postCategories: React.PropTypes.array.isRequired,
};

PostsPage = connectToStores(PostsPage, [ PostStore, PostCategoryStore ], (context) => {
  const postStore = context.getStore(PostStore);
  const posts = postStore.getPosts().concat();
  const featuredPost = posts.splice(0, 1)[0];

  return {
    featuredPost,
    posts,
    loadingMorePosts: postStore.isLoadingMorePosts(),
    allPostsLoaded: postStore.isAllPostsLoaded(),
    postCategories: context.getStore(PostCategoryStore).getPostCategories(),
  };
});

PostsPage = Resolver.createContainer(PostsPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    bodyTheme: (props, context) => {
      return context.executeAction(setBodyThemeAction, 'info');
    },

    activePostCategory: (props) => {
      return props.params.category;
    },

    posts: (props, context) => {
      return context.executeAction(loadPostsAction, { count: postsPerPage, categories: _.compact([ props.params.category ]) });
    },

    postCategories: (props, context) => {
      return context.executeAction(loadPostCategoriesAction);
    },
  },
});

export default PostsPage;
