import React, { Component } from 'react';
import { RouteHandler } from 'react-router';
import { Element } from 'react-scroll';

import Header from './Header';
import Footer from './Footer';
import LandingPage from './LandingPage';
import SubscribeForm from './SubscribeForm';

class DefaultLayout extends Component {
  render() {
    return (
      <div>
        <LandingPage />
        <div className="main">
          <Header />
          <RouteHandler />
          <Footer />
        </div>
        <SubscribeForm />
        <Element name="subscribe" />
      </div>
    );
  }
}

export default DefaultLayout;
