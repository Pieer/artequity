import React, { Component } from 'react';

import CloudinaryImage from 'react-cloudinary-img';

import HoverBorder from './HoverBorder';

import andJoin from '../utils/andJoin';
import formattedDateRange from '../utils/formattedDateRange';

class ExhibitionTile extends Component {
  render() {
    const { exhibition } = this.props;
    const { startDate, endDate, artists } = exhibition;
    const formattedDate = formattedDateRange(startDate, endDate);
    const exhibitionUrl = `/exhibition/${exhibition.slug}`;
    const artistNames = artists.length > 5 ? 'Group Exhibition' : andJoin(artists.map((artist) => artist.name.full));

    return (
      <div>
        <a className="link-plain" href={exhibitionUrl}>
          <HoverBorder>
            <CloudinaryImage image={exhibition.thumbnailImage} options={{ width: 380 * 2 }} className="img-full-width" />
          </HoverBorder>

          <h2 className="text-black append-xs-xtiny">
            {artists.length > 1 ? exhibition.title : artistNames}
          </h2>
        </a>
        <p className="text-uppercase text-spaced append-xs-xtiny">
          {artists.length > 1 ? artistNames : exhibition.title}
        </p>
        <p className="text-uppercase text-spaced append-xs-none">{formattedDate}</p>
      </div>
    );
  }
}

ExhibitionTile.propTypes = {
  exhibition: React.PropTypes.object.isRequired,
};

export default ExhibitionTile;
