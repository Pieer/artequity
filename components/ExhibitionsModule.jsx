import React, { Component } from 'react';
import classNames from 'classnames';

import CloudinaryImage from 'react-cloudinary-img';

import andJoin from '../utils/andJoin';
import formattedDateRange from '../utils/formattedDateRange';

class ExhibitionsModule extends Component {
  renderExhibition(exhibition, index) {
    const { exhibitions } = this.props;
    const { startDate, endDate, artists } = exhibition;
    const formattedDate = formattedDateRange(startDate, endDate);
    const exhibitionUrl = `/exhibition/${exhibition.slug}`;
    const artistNames = artists.length > 5 ? 'Group Exhibition' : andJoin(artists.map((artist) => artist.name.full));

    return (
      <div key={exhibition.id} className={classNames('media', index === exhibitions.length - 1 ? '' : 'append-xs-2')}>
        <div className="media-object media-middle media-left">
          <a href={exhibitionUrl}>
            <CloudinaryImage image={exhibition.thumbnailImage} options={{ width: 180, height: 180, crop: 'fill' }} className="img-square-small-xs" />
          </a>
        </div>
        <div className="media-body media-right media-middle">
          <h2 className="text-spaced text-uppercase prepend-xs-none append-xs-1">
            <a href={exhibitionUrl} className="link-plain">
              {artists.length > 1 ? exhibition.title : artistNames}
            </a>
          </h2>
          <h4 className="text-black append-xs-1">
            {artists.length > 1 ? artistNames : exhibition.title}
          </h4>
          {formattedDate &&
            <p className="append-xs-none text-spaced text-uppercase">{formattedDate}</p>}
        </div>
      </div>
    );
  }

  render() {
    const { exhibitions, linkToAll } = this.props;

    return (
      <div className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 pad-vertical-xs-1 pad-sm-1 bg-white">
        {exhibitions.map(this.renderExhibition)}
        {linkToAll &&
          <div className="text-center-xs text-right-md prepend-xs-2">
            <a className="btn btn-body-default text-xs text-spaced text-uppercase" href="/exhibitions">
              All exhibitions
            </a>
          </div>}
      </div>
    );
  }

  constructor() {
    super();
    this.renderExhibition = this.renderExhibition.bind(this);
  }
}

ExhibitionsModule.propTypes = {
  exhibitions: React.PropTypes.array.isRequired,
  linkToAll: React.PropTypes.bool,
};

export default ExhibitionsModule;
