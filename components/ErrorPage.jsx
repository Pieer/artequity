import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import DocumentTitle from 'react-document-title';

class ErrorPage extends Component {
  render() {
    const { config } = this.context;
    const { code, message } = this.props;

    return (
      <DocumentTitle title={`${code}: ${message} - ${config.brand}`}>
        <Grid className="prepend-vertical-xs-10">
          <Row>
            <Col md={6} mdOffset={3}>
              <h1 className="text-underline">{code}</h1>
              <h2 className="text-uppercase text-spaced">{message}</h2>
              <p className="text-lg">
                Please <a href="/contact">contact us</a> if you believe this error should not have occurred.
              </p>
            </Col>
          </Row>
        </Grid>
      </DocumentTitle>
    );
  }
}

ErrorPage.contextTypes = {
  config: React.PropTypes.object.isRequired,
};

ErrorPage.propTypes = {
  code: React.PropTypes.number.isRequired,
  message: React.PropTypes.string.isRequired,
};

export default ErrorPage;
