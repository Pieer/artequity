import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { Resolver } from 'react-resolver';
import { Link } from 'react-scroll';

import _ from 'lodash';
import classNames from 'classnames';
import connectToStores from 'fluxible-addons-react/connectToStores';

import loadContactInfoAction from '../actions/loadContactInfo';
import loadLocationsAction from '../actions/loadLocations';

import LocationStore from '../stores/LocationStore';
import PageStore from '../stores/PageStore';

import Logo from './Logo';

class Footer extends Component {
  renderLocationListItem(location) {
    return (
      <li key={location.id} className="append-horizontal-xs-3">
        <h4 className="text-black pend-xs-none">{location.shortName}</h4>
      </li>
    );
  }

  renderLocationColumn(location, index) {
    const { locations } = this.props;
    const position = index % 3;

    let newIndex = index;

    if (position === 0) { newIndex++; }
    if (position === 1) { newIndex--; }

    const swappedLocation = locations[newIndex >= locations.length ? locations.length - 1 : newIndex];

    return (
      <Col key={location.id} xs={4} className="pad-xs-none">
        <h4 className={
          classNames(
            'text-black pend-xs-none',
            position === 0 && 'text-left',
            position === 1 && 'text-center',
            position === 2 && 'text-right'
          )}>
          {swappedLocation.shortName}
        </h4>
      </Col>
    );
  }

  renderLocations() {
    const { locations } = this.props;

    return (
      <Row className="flex-center-md prepad-xs-1">
        <Col md={4} mdPush={8} className="text-center-xs text-right-md pad-horizontal-xs-1">
          <Logo />
        </Col>
        <Col md={8} mdPull={4} className="pad-horizontal-xs-xtiny pad-xs-1 pad-sm-1">
          <ul className="list-inline list-inline--compact hidden-xs hidden-sm append-xs-none">
            {locations.map(this.renderLocationListItem)}
          </ul>
          <div className="hidden-md hidden-lg">
            <Row className="pend-horizontal-xs-none">
              {locations.map(this.renderLocationColumn)}
            </Row>
          </div>
        </Col>
      </Row>
    );
  }

  renderContactInfo() {
    const { contactInfo } = this.props;
    const { address } = contactInfo;

    return (
      <Row className="divider-container text-sm postpad-xs-1">
        <Col md={4} className="text-center-xs text-left-md">
          <address className="pre pre--spaced--md">
            {_.compact([
                address.name, _.compact([ address.number, ' ']).join(''),
                address.street1, address.street2,
                _.compact([ address.suburb+',', address.state, address.postcode ]).join(' ') ]
              ).join('\n')}
          </address>
        </Col>
        <Col md={4} className="text-center-xs text-left-md">
          <div className="pre pre--spaced--md">
            Opening Hours<br/>
            {contactInfo.hours}
          </div>
        </Col>
        <Col md={4} className="text-center-xs text-left-md hidden-xs hidden-sm">
          <div className="pre pre--spaced--md">
            <br/>
            <a className="text-black" href={`tel:${contactInfo.phone}`}>{contactInfo.phone}</a><br/>
            <a className="text-black text-underline" href={`mailto:${contactInfo.email}`}>{contactInfo.email}</a>
          </div>
        </Col>
        <span className="divider divider--vertical--md divider--bottom-right--md" />
      </Row>
    );
  }

  renderSocialButton(link) {
    const icons = {
      'ion-social-facebook': /facebook/,
      'ion-social-twitter': /twitter/,
      'ion-social-instagram': /instagram/,
      'ion-social-vimeo': /vimeo/,
      'ion-social-youtube': /youtube/,
    };

    let button;

    _.each(icons, (regex, icon) => {
      if (regex.test(link)) {
        button = <a href={link} className={classNames(icon, 'btn btn-hollow btn-hollow-default btn-social link-plain')} target="_blank"></a>;
        return false;
      }
    });

    return button;
  }

  renderLinks() {
    const { contactInfo } = this.props;
    const { links } = contactInfo;

    return (
      <ul className="list-inline list-inline--compact append-xs-none">
        {links.map((link, index) => (
          <li key={index} className="pend-horizontal-xs-tiny prepend-horizontal-md-none">
            {this.renderSocialButton(link)}
          </li>))}
      </ul>
    );
  }

  renderOther() {
    const { config } = this.context;
    const { contactInfo } = this.props;

    return (
      <Row className="non-printable prepend-vertical-md--1 flex-flex-end-md postpad-md-tiny">
        <Col md={4} className="text-center-xs text-left-md pad-horizontal-xs-xtiny postpad-xs-1 pad-horizontal-sm-1">
          <h4 className="text-black prepend-xs-none append-xs-1">Connect</h4>
          {this.renderLinks()}
        </Col>
        <Col md={4} className="text-center pad-horizontal-xs-xtiny postpad-xs-tiny pad-horizontal-sm-1 hidden-md hidden-lg">
          <p className="append-xs-1">
            <a className="text-black text-underline" href={`tel:${contactInfo.phone}`}>{contactInfo.phone}</a><br/>
          </p>
          <p className="prepend-xs-tiny append-xs-1">
            <a className="text-black text-underline" href={`mailto:${contactInfo.email}`}>{contactInfo.email}</a>
          </p>
        </Col>
        <Col md={4} className="divider-container text-sm pad-horizontal-xs-xtiny postpad-xs-1 pad-horizontal-sm-1 pull-left">
          <Link to="subscribe" smooth={true}>
            <a className="text-black text-underline" href="/">Subscribe</a>
          </Link>
          <span className="divider divider--horizontal--md divider--bottom-right--md" />
        </Col>
        <Col md={4} className="text-right text-xs text-muted pad-horizontal-xs-xtiny postpad-xs-1 pad-horizontal-sm-1 pull-right">
          <ul className="list-inline list-inline--compact append-xs-none">
            <li><a href="/privacy"><u className="text-muted">Privacy</u></a></li>
            <li className="pend-horizontal-xs-tiny pend-horizontal-sm-1">\</li>
            <li>&copy; {config.name} {new Date().getFullYear()}. Site by <a href="http://www.pollen.com.au">Pollen</a>.</li>
          </ul>
        </Col>
      </Row>
    );
  }

  render() {
    return (
      <div>
        <div className="non-printable bordered--xs bordered--top--xs">
          <Grid>
            {this.renderLocations()}
            {this.renderContactInfo()}
            {this.renderOther()}
          </Grid>
        </div>
      </div>
    );
  }

  constructor() {
    super();
    this.renderLocationColumn = this.renderLocationColumn.bind(this);
  }
}

Footer.contextTypes = {
  config: React.PropTypes.object.isRequired,
};

Footer.propTypes = {
  locations: React.PropTypes.array.isRequired,
  contactInfo: React.PropTypes.object.isRequired,
};

Footer = connectToStores(Footer, [ LocationStore, PageStore ], (context) => {
  return {
    locations: context.getStore(LocationStore).getLocations(),
    contactInfo: context.getStore(PageStore).getContactInfo(),
  };
});

Footer = Resolver.createContainer(Footer, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    locations: (props, context) => {
      return context.executeAction(loadLocationsAction, false);
    },

    contactInfo: (props, context) => {
      return context.executeAction(loadContactInfoAction);
    },
  },
});

export default Footer;
