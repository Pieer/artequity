import React, { Component } from 'react';
import { Grid, Row, Col, Input } from 'react-bootstrap';
import reactMixin from 'react-mixin';

import _ from 'lodash';

import FormStackForm from './FormStackForm';

import NativeComboBox from './NativeComboBox';
import NumberRevealer from './NumberRevealer';

var trim = function() {
  var TRIM_RE = /^\s+|\s+$/g
  return function trim(string) {
    return string.replace(TRIM_RE, '')
  }
}();

class ContactForm extends Component {
  onSubmit() {
    const valid = this.isValid();
    if(valid) this.setState({ error: void 0, loading: true, sent: false });
    return valid;
  }

  onError(err) {
    this.setState({ error: err.message, loading: false });
  }

  onSuccess() {
    this.setState({
      loading: false,
      sent: true,
      firstName: void 0,
      lastName: void 0,
      email: void 0,
      phone: void 0,
      referredBy: void 0,
      enquiry: void 0,
      subscribe: void 0,
      error: '',
    });
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  isValid() {
    var fields = ['first_name', 'last_name','phone', 'referred_by', 'email','message'];
    var errors = {};
    fields.forEach(function(field) {
      var value = trim(this.refs[field].refs.input.getDOMNode().value);

      if (!value) {
        errors[field] = 'This field is required'
      }else{
        if(field == 'email' && !this.validateEmail(value)){
          errors[field] = 'Please complete all required fields'
        }
      }
    }.bind(this));

    var isValid = true;
    var error = false;
    for (var er in errors) {
      isValid = false;
      error = 'Please complete all required fields';
      break;
    }
    this.setState({errors: errors, error: error});
    return isValid;
  }

  render() {
    const { config } = this.context;
    const { context, contextDescription } = this.props;
    const { firstName, lastName, email, phone, referredBy, enquiry, subscribe, error, loading, sent } = this.state;

    const referOptions = [
      { label: 'Please select...', value: '' },
      'Newsletters',
      'Online advertising',
      'Friend/Colleague',
      'Other',
    ].map((referOption) => (
      <option key={referOption.value || referOption}
        value={_.isObject(referOption) ? referOption.value : referOption}>
        {referOption.label || referOption}
      </option>
    ));

    return (
      <Grid className="bordered--xs bordered--left--xs bordered--right--xs">
        <Row className="bordered--xs bordered--centred--xs bordered--centred--md--none bordered--top--xs bordered--third--md bg-striped">
          <div className="clearfix">
            <Col md={12} className="pad-horizontal-xs-none pad-vertical-xs-1 pad-md-1">
              <div className="divider-container bg-white">
                <Row className="pend-horizontal-xs-none pend-horizontal-md--2">
                  <FormStackForm
                    form={config.formStack.contact.id} viewkey={config.formStack.contact.viewKey}
                    onSubmit={this.onSubmit} onError={this.onError} onSuccess={this.onSuccess}
                    field35125999={firstName}
                    field35126000={lastName}
                    field35125981={email}
                    field35125982={phone}
                    field35125983={referredBy}
                    field35125984={enquiry}
                    field35125985_1={subscribe && 'Please subscribe me to Art Equity\'s bimonthly online market report, Art Insight.'}
                    {...context}>

                    <Col md={4} className="text-center-xs text-left-md prepad-vertical-xs-1 pad-vertical-md-1 prepad-horizontal-xs-xtiny prepad-horizontal-sm-1 prepad-horizontal-md-2 postpad-horizontal-sm-1">
                      <h1 className="prepend-xs-none append-xs-xtiny">Contact Us</h1>
                      {contextDescription && <p>{contextDescription}</p>}
                      <div className="prepend-xs-1">
                        <NumberRevealer />
                      </div>
                    </Col>
                    <Col md={4} className="prepad-vertical-xs-1 pad-vertical-md-1 pad-horizontal-xs-xtiny pad-horizontal-sm-1">
                      <Row className="pend-horizontal-xs-none">
                        <Col md={6} className="pad-horizontal-xs-none postpad-horizontal-md-xtiny">
                          <div className={'first_name' in this.state.errors ? 'has-error': ''}>
                            <Input type="text" placeholder="First Name *" bsSize="large" valueLink={this.linkState('firstName')} groupClassName="append-xs-tiny" ref='first_name'/>
                          </div>
                        </Col>
                        <Col md={6} className="pad-horizontal-xs-none prepad-horizontal-md-xtiny">
                          <div className={'last_name' in this.state.errors ? 'has-error': ''}>
                            <Input type="text" placeholder="Last Name *" bsSize="large" valueLink={this.linkState('lastName')} groupClassName="append-xs-tiny"  ref='last_name'/>
                          </div>
                        </Col>
                      </Row>
                      <div className={'email' in this.state.errors ? 'has-error': ''}>
                        <Input type="text" placeholder="Email *" bsSize="large" valueLink={this.linkState('email')} groupClassName="append-xs-tiny" ref='email'/>
                      </div>
                      <div className={'phone' in this.state.errors ? 'has-error': ''}>
                        <Input type="text" placeholder="Daytime Phone number *" bsSize="large" valueLink={this.linkState('phone')} groupClassName="append-xs-tiny"  ref='phone'/>
                      </div>

                      <NativeComboBox valueLink={this.linkState('referredBy')} options={referOptions}>
                        <div className={'referred_by' in this.state.errors ? 'has-error': ''}>
                          <Input type="text" tabIndex="-1" placeholder="Where did you hear about us? *" bsSize="large" value={referredBy} groupClassName="append-xs-tiny" ref='referred_by'/>
                        </div>
                      </NativeComboBox>
                    </Col>
                    <Col md={4} className="pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-horizontal-md-1 postpad-horizontal-md-2">
                      <div className={'message' in this.state.errors ? 'has-error': ''}>
                        <Input type="textarea" label="Your enquiry ... *"
                          labelClassName="text-heavy append-xs-none" rows="3" bsSize="large"
                          valueLink={this.linkState('enquiry')}  ref='message'/>
                      </div>
                      <Input type="checkbox" label={'Please subscribe me to updates from Nanda\Hobbs Contemporary'}
                         labelClassName="text-sm" groupClassName="append-xs-3"
                         checkedLink={this.linkState('subscribe')} />

                      {sent && <p className="text-right">Thank you. We will be in touch shortly.</p>}
                      {error && <p className="text-danger">{error}</p>}

                      <div className="text-center-xs text-right-md">
                        <button className="btn btn-body-default btn-wide text-xs text-spaced text-uppercase" type="submit"
                          disabled={loading}>
                          {loading ? 'Sending...' : 'Submit'}
                        </button>
                      </div>
                    </Col>
                  </FormStackForm>
                </Row>

                <span className="divider divider--vertical--xs divider--bottom-left--xs divider--bottom-left--sm" />
              </div>
            </Col>
          </div>
        </Row>
      </Grid>
    );
  }

  constructor() {
    super();

    this.state = {
      sent: false,
      loading: false,
      firstName: void 0,
      lastName: void 0,
      email: void 0,
      phone: void 0,
      referredBy: void 0,
      enquiry: void 0,
      subscribe: void 0,
      error: void 0,
      errors: {}
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
    this.onError = this.onError.bind(this);
  }
}

ContactForm.contextTypes = {
  config: React.PropTypes.object.isRequired,
};

ContactForm.propTypes = {
  context: React.PropTypes.object,
  contextDescription: React.PropTypes.string,
};

reactMixin.onClass(ContactForm, React.addons.LinkedStateMixin);

export default ContactForm;
