import React, { Component } from 'react';

import _ from 'lodash';
import CloudinaryImage from 'react-cloudinary-img';

import andJoin from '../utils/andJoin';

class ArtistBioModule extends Component {
  render() {
    const { artist, showImage, linkToCv } = this.props;
    const info = _.compact([ artist.birth, artist.abode, artist.additionalInfo ]);
    const representationString = andJoin(_.pluck(artist.representedBy, 'name'));

    return (
      <div className="pad-horizontal-xs-xtiny pad-xs-1 pad-sm-2 pad-horizontal-md-1 pad-horizontal-lg-2 bg-white text-center text-content append-xs-tiny append-md-2">
        {showImage && <CloudinaryImage
          image={artist.photo}
          options={{ width: 240 * 2, height: 240 * 2, crop: 'fill' }}
          className="img-circle img-thumbnail append-xs-2"
          width={240} height={240} />}
        {!showImage && <h2 className="prepend-xs-none">{artist.name.full}</h2>}

        <p className="text-spaced text-uppercase">
          {info.map((value, index) =>
            <span key={index}>{value}{index === info.length - 1 ? false : <br/>}</span>)}
        </p>

        {linkToCv && artist.cv && artist.cv.url &&
          <p>
            <a className="btn btn-body-default text-xs text-spaced text-uppercase" href={artist.cv.url} target="_blank">
               Artist's CV
            </a>
          </p>}
        {!linkToCv &&
          <p>
            <a className="btn btn-body-default text-xs text-spaced text-uppercase" href={`/artist/${artist.slug}`}>
              View profile
            </a>
          </p>}

        {_.any(artist.representedBy) &&
          <p className="text-spaced text-uppercase">Represented by {representationString}</p>}
      </div>
    );
  }
}

ArtistBioModule.propTypes = {
  artist: React.PropTypes.object.isRequired,
  showImage: React.PropTypes.bool.isRequired,
  linkToCv: React.PropTypes.bool.isRequired,
};


ArtistBioModule.defaultProps = {
  showImage: true,
  linkToCv: true,
};

export default ArtistBioModule;
