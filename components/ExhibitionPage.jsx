import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import DocMeta from 'react-doc-meta';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import connectToStores from 'fluxible-addons-react/connectToStores';
import moment from 'moment';
import 'moment-timezone';

import loadExhibitionAction from '../actions/loadExhibition';
import loadExhibitionArtworksAction from '../actions/loadExhibitionArtworks';
import loadExhibitionsAction from '../actions/loadExhibitions';
import loadPostsAction from '../actions/loadPosts';
import setBodyThemeAction from '../actions/setBodyTheme';

import ArtistStore from '../stores/ArtistStore';
import ArtworkStore from '../stores/ArtworkStore';
import ExhibitionStore from '../stores/ExhibitionStore';
import PostStore from '../stores/PostStore';

import ArtistBioModule from './ArtistBioModule';
import ArtworkTile from './ArtworkTile';
import CalendarButton from './CalendarButton';
import ContactForm from './ContactForm';
import FeatureModule from './FeatureModule';
import ExhibitionsModule from './ExhibitionsModule';
import MasonryTiles from './MasonryTiles';
import PostTile from './PostTile';
import SocialSharers from './SocialSharers';

import colors from '../utils/colors';
import formattedDateRange from '../utils/formattedDateRange';
import andJoin from '../utils/andJoin';

class ExhibitionPage extends Component {
  renderFeatureModule() {
    const { exhibition } = this.props;
    const { artists } = exhibition;
    const formattedDate = formattedDateRange(exhibition.startDate, exhibition.endDate);
    const artistNames = artists.length > 5 ? 'Group Exhibition' : andJoin(artists.map((artist) => artist.name ? artist.name.full : void 0));

    return (
      <FeatureModule image={exhibition.featuredImage}>
        <h1 className="prepend-xs-none append-xs-1 append-md-2">
          {artists.length > 1 ? exhibition.title : artistNames}
        </h1>
        <h2 className="append-xs-none text-spaced text-uppercase">
          {artists.length > 1 ? artistNames : exhibition.title}
        </h2>
        {formattedDate && <p className="text-spaced text-uppercase prepend-xs-1 prepend-md-2 append-xs-none">{formattedDate}</p>}
        <SocialSharers title={`${exhibition.title} by ${artistNames}`} className="prepend-xs-2" />
      </FeatureModule>
    );
  }

  renderInfo() {
    const { exhibition } = this.props;

    return (
      <Row className="bordered--xs bordered--top--xs">
        <div className="clearfix">
          <Col md={12} className="pad-horizontal-xs-none pad-vertical-xs-1 pad-md-1 text-center-xs text-left-md">
            <div className="bg-white">
              <Row className="pend-horizontal-xs-none pend-horizontal-md--2">
                <Col md={4} className="prepad-vertical-xs-1 pad-vertical-md-1 prepad-horizontal-xs-xtiny prepad-horizontal-sm-1 prepad-horizontal-md-2 postpad-horizontal-sm-1">
                  <h2 className="pend-vertical-xs-none pend-vertical-md-xtiny">
                    Exhibition opening<br/>
                    {moment(exhibition.launchDate).tz('Australia/Sydney').format('dddd D MMMM')}
                    {exhibition.launchTime && <span>,<br/>{exhibition.launchTime}</span>}
                  </h2>
                </Col>
                <Col md={4} className="prepad-vertical-xs-1 pad-vertical-md-1 pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-horizontal-md-1">
                  <div className="pend-vertical-xs-none pend-vertical-md-xtiny text-content">
                    <p>
                      {exhibition.location.full}
                    </p>
                    {exhibition.galleryHours &&
                      <p className="pre">
                        Gallery Hours:<br/>
                        {exhibition.galleryHours}
                      </p>}
                  </div>
                </Col>
                <Col xs={8} xsOffset={2} sm={6} smOffset={3} md={4} mdOffset={0} lgOffset={1} lg={3} className="pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-horizontal-md-1 postpad-horizontal-md-2 text-center-xs text-left-md">
                  <CalendarButton
                    className="btn-block"
                    buttonClassName="btn btn-hollow btn-block btn-hollow-default text-xs text-spaced text-uppercase" type="button"
                    slug={exhibition.slug}
                    title={exhibition.title}
                    startDate={moment(exhibition.launchDate).tz('Australia/Sydney').format()}
                    endDate={moment(exhibition.launchDate).tz('Australia/Sydney').format()}
                    address={exhibition.location.full.replace(/,/g, '')}>
                    Add to calendar
                  </CalendarButton>
                  {exhibition.rsvpLink &&
                    <div className="prepend-xs-1">
                      <a className="btn btn-block btn-body-default text-xs text-spaced text-uppercase" href={exhibition.rsvpLink} target="_blank">
                        RSVP
                      </a>
                    </div>}
                </Col>
              </Row>
            </div>
          </Col>
        </div>
      </Row>
    );
  }

  renderSidebar() {
    const { exhibition } = this.props;
    const { artists } = exhibition;

    return (
      <Col md={4} className="divider-container bordered--xs bordered--bottom--xs bordered--md--none prepad-vertical-xs-1 postpad-vertical-xs-1 postpad-vertical-md-2 pad-horizontal-xs-none pad-horizontal-md-1">
        {artists.length === 1 && (typeof artists[0] === 'object') && <ArtistBioModule key={artists[0].id} artist={artists[0]} showImage={true} linkToCv={false} />}
        {artists.length > 1 &&
          <div className="pad-horizontal-xs-xtiny pad-xs-1 pad-sm-2 bg-white text-center text-content append-xs-tiny append-vertical-md-2 append-horizontal-md-1 append-lg-2">
            {artists.map((artist) => (typeof artist === 'object') && <a key={artist.id} href={`/artist/${artist.slug}`}><p>{artist.name.full}</p></a>)}
          </div>}
        <span className="divider divider--vertical--xs divider--bottom-right--xs divider--bottom-right--md" />
      </Col>
    );
  }

  renderEssay() {
    const { exhibition } = this.props;

    return (
      <Col md={8} className="pad-vertical-xs-1 pad-horizontal-xs-none pad-horizontal-md-1">
        <div className="pad-horizontal-xs-xtiny pad-xs-1 pad-horizontal-sm-3 pad-vertical-sm-2 bg-white text-content">
          <div dangerouslySetInnerHTML={{__html: exhibition.essay}}></div>
        </div>
      </Col>
    );
  }

  renderProfile() {
    return (
      <Row className="bordered--xs bordered--top--xs flex-stretch-md">
        {this.renderSidebar()}
        {this.renderEssay()}
      </Row>
    );
  }

  renderArtworkTiles() {
    const { exhibition, artworks } = this.props;
    return (
      <MasonryTiles title="Exhibition featured works" items={artworks}
        renderTile={(artwork) => <ArtworkTile artwork={artwork} exhibition={exhibition}/>} />
    );
  }

  renderNews() {
    const { latestPost } = this.props;

    return (
      <Col md={4} className=" divider-container prepad-xs-2 postpad-xs-1 postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
        {latestPost &&
          <div>
            <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
              \ News
            </h2>
            <div className="prepend-xs-1">
              <PostTile post={latestPost} linkToAll={true} />
            </div>
          </div>}
      </Col>
    );
  }

  renderExhibitions() {
    const { exhibitions } = this.props;

    return (
      <Col md={8} className="divider-container prepad-xs-2 postpad-xs-1 postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
        <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
          \ Other exhibitions
        </h2>
        <div className="prepend-xs-1">
          <ExhibitionsModule exhibitions={exhibitions} linkToAll={true} />
        </div>
        <span className="divider divider--horizontal--xs divider--top-right--xs divider--top-right--md" />
      </Col>
    );
  }

  render() {
    const { exhibition, artworks, exhibitions, latestPost } = this.props;
    const launchHasPasted = !moment().isBefore(exhibition.launchDate);

    return (
      <DocumentTitle title={exhibition.meta.title}>
        <div>
          <DocMeta tags={exhibition.meta.tags} />
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            <Row className="bordered--xs bordered--bottom--xs">
              <Col md={12} className="pad-horizontal-xs-xtiny pad-vertical-xs-1 pad-sm-1">
                <h2 className="pend-vertical-xs-none">
                  \ <a className="link-plain" href="/exhibitions">Exhibitions</a>
                </h2>
              </Col>
            </Row>
            {this.renderFeatureModule()}
            {!launchHasPasted && this.renderInfo()}
            {this.renderProfile()}
          </Grid>
          {_.any(artworks) &&
            <div className="accent-container">
              <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
                {this.renderArtworkTiles()}
              </Grid>
              <div className="accent bg-striped"></div>
            </div>}
          {(_.any(exhibitions) || latestPost) &&
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
              <Row className="bordered--xs bordered--top--xs">
                <div className="clearfix">
                  {this.renderNews()}
                  {this.renderExhibitions()}
                </div>
              </Row>
            </Grid>}
          <ContactForm contextDescription={`to find out more about ${exhibition.title}.`} />
        </div>
      </DocumentTitle>
    );
  }
}

ExhibitionPage.propTypes = {
  exhibition: React.PropTypes.object.isRequired,
  artworks: React.PropTypes.array.isRequired,
  exhibitions: React.PropTypes.array.isRequired,
  latestPost: React.PropTypes.object,
};

ExhibitionPage = connectToStores(ExhibitionPage, [ ArtistStore, ArtworkStore, ExhibitionStore, PostStore ], (context) => {
  const exhibitionStore = context.getStore(ExhibitionStore);
  return {
    exhibition: exhibitionStore.getExhibition(),
    artworks: context.getStore(ArtworkStore).getExhibitionArtworks(),
    exhibitions: exhibitionStore.getExhibitions(),
    latestPost: context.getStore(PostStore).getPosts()[0],
  };
});

ExhibitionPage = Resolver.createContainer(ExhibitionPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    exhibition: (props, context) => {
      return context.executeAction(loadExhibitionAction, props.params.slug).then(() => {
        const exhibition = context.getStore(ExhibitionStore).getExhibition();
        if (exhibition) {
          return context.executeAction(setBodyThemeAction, colors[exhibition.color]);
        }
      });
    },

    artworks: (props, context) => {
      return context.executeAction(loadExhibitionArtworksAction, props.params.slug);
    },

    exhibitions: (props, context) => {
      return context.executeAction(loadExhibitionsAction, { count: 3, exclude: props.params.slug });
    },

    latestPost: (props, context) => {
      return context.executeAction(loadPostsAction, { count: 1, exhibition: props.params.slug });
    },
  },
});

export default ExhibitionPage;
