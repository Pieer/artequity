import React, { Component } from 'react';

import ErrorPage from './ErrorPage';

class NotFoundPage extends Component {
  render() {
    return <ErrorPage code={404} message="Page not found" />;
  }
}

export default NotFoundPage;
