import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import connectToStores from 'fluxible-addons-react/connectToStores';

import loadPublicationsAction from '../actions/loadPublications';
import setBodyThemeAction from '../actions/setBodyTheme';

import PublicationStore from '../stores/PublicationStore';

import PublicationTile from './PublicationTile';
import MasonryTiles from './MasonryTiles';

class PublicationsPage extends Component {
  renderPublicationTiles() {
    const { publications } = this.props;

    return (
      <MasonryTiles items={publications}
        colSizes={{ xs: 12, md: 6, lg: 4 }}
        renderTile={(publication) => <PublicationTile publication={publication} />}
        colClassName="postpad-vertical-xs-none" />
    );
  }

  render() {
    const { config } = this.context;
    const { publications } = this.props;

    return (
      <DocumentTitle title={`Publications - ${config.brand}`}>
        <div>
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--third--lg">
            <Row>
              <Col md={12} className="pad-horizontal-xs-xtiny pad-vertical-xs-1 pad-sm-1">
                <h2 className="pend-vertical-xs-none">\ Publications</h2>
              </Col>
            </Row>
          </Grid>
          {_.any(publications) &&
            <div className="accent-container">
              <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs boredered--centred--md--none bordered--third--lg">
                {this.renderPublicationTiles()}
              </Grid>
              <div className="accent bg-striped"></div>
            </div>}
        </div>
      </DocumentTitle>
    );
  }
}

PublicationsPage.contextTypes = {
  config: React.PropTypes.object.isRequired,
  executeAction: React.PropTypes.func.isRequired,
};

PublicationsPage.propTypes = {
  publications: React.PropTypes.array.isRequired,
};

PublicationsPage = connectToStores(PublicationsPage, [ PublicationStore ], (context) => {
  return {
    publications: context.getStore(PublicationStore).getPublications(),
  };
});

PublicationsPage = Resolver.createContainer(PublicationsPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    bodyTheme: (props, context) => {
      return context.executeAction(setBodyThemeAction, 'warning');
    },

    publications: (props, context) => {
      return context.executeAction(loadPublicationsAction);
    },
  },
});

export default PublicationsPage;
