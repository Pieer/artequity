import React, { Component } from 'react';

import classNames from 'classnames';

class Logo extends Component {
  render() {
    const { className, link, onClick } = this.props;

    return (
      <a className={classNames('btn btn-hollow-thick btn-hollow-white btn-menu text-black link-plain', className)} href={link}
        onClick={onClick}>
        n<span className="hidden-xs">anda</span>
        <span className="text-body-default">\</span>
        h<span className="hidden-xs">obbs</span>
      </a>
    );
  }
}

Logo.propTypes = {
  className: React.PropTypes.string,
  link: React.PropTypes.string,
  onClick: React.PropTypes.func,
};

Logo.defaultProps = {
  link: '/',
};

export default Logo;
