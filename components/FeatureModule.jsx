import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

import CloudinaryImage from 'react-cloudinary-img';

class FeatureModule extends Component {
  render() {
    const { image, link, children } = this.props;
    const imageElement = <CloudinaryImage image={image} options={{ width: 820 * 2, height: 510 * 2, crop: 'fill' }} className="img-full-width" />;

    return (
      <Row className="divider-container postpad-vertical-xs-1 postpad-md-none flex-stretch-md">
        <Col md={4} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-xs-1 postpad-vertical-xs-1 postpad-vertical-md-3 prepad-vertical-sm-3 prepad-vertical-lg-4">
          <span className="divider divider--vertical--xs divider--top-right--xs divider--top-left--sm" />
          {children}
        </Col>
        <Col md={8} className="pad-horizontal-xs-none pad-md-1">
          {link && <a href={link}>{imageElement}</a>}
          {!link && imageElement}
        </Col>
        <span className="divider divider--horizontal--xs divider--bottom-left--xs divider--bottom-left--sm" />
      </Row>
    );
  }
}

FeatureModule.propTypes = {
  image: React.PropTypes.object,
  link: React.PropTypes.string,
  children: React.PropTypes.any,
};

export default FeatureModule;
