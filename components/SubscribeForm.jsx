import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import reactMixin from 'react-mixin';

import FormStackForm from './FormStackForm';

var trim = function() {
  var TRIM_RE = /^\s+|\s+$/g
  return function trim(string) {
    return string.replace(TRIM_RE, '')
  }
}();

class SubscribeForm extends Component {
  onSubmit() {
    const valid = this.isValid();
    if(valid) this.setState({ error: void 0, loading: true, sent: false });
    return valid
  }

  onError(err) {
    this.setState({ error: err.message, loading: false });
  }

  onSuccess() {
    this.setState({
      loading: false,
      sent: true,
      email: void 0,
      error: void 0,
    });
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  isValid() {

    var value = trim(this.refs['email'].getDOMNode().value);
    var error = 0;
    if (!value) {
      error = 'This field is required'
    }
    else{
      if(!this.validateEmail(value)){
        error = 'Please enter a valid email address'
      }
    }

    this.setState({error: error});
    return !error
  }

  render() {
    const { config } = this.context;
    const { email, error, loading, sent } = this.state;

    return (
      <div className="non-printable subscribe bg-striped-dark text-inverse pad-vertical-xs-1 prepad-vertical-md-3 postpad-vertical-md-4">
        <Grid>
          <Row>
            <Col md={10}>
              <h3 className="text-black prepend-xs-none append-xs-1 text-xxl">Subscribe</h3>

              <FormStackForm
                form={config.formStack.subscribe.id} viewkey={config.formStack.subscribe.viewKey}
                onSubmit={this.onSubmit} onError={this.onError} onSuccess={this.onSuccess}
                field34994850={email}>
                <div className="input-group">
                  <div className={this.state.error ? 'has-error': ''}>
                    <input type="text" className="form-control input-hollow-white-outline input-xxl"
                           valueLink={this.linkState('email')}
                           placeholder="Enter email address"  ref='email'/>
                  </div>
                  <span className="input-group-btn">
                    <button className="btn btn-hollow btn-hollow-white-outline btn-xxl" type="submit"
                            disabled={loading}>
                      Go!
                    </button>
                  </span>
                </div>
              </FormStackForm>

              {sent && <p className="prepend-xs-2 append-xs-none">Subscription successful!</p>}
              {error && <p className="prepend-xs-2 append-xs-none">{error}</p>}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }

  constructor() {
    super();

    this.state = {
      loading: false,
      sent: false,
      email: void 0,
      error: void 0,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
    this.onError = this.onError.bind(this);
  }
}

SubscribeForm.contextTypes = {
  config: React.PropTypes.object.isRequired,
};

reactMixin.onClass(SubscribeForm, React.addons.LinkedStateMixin);

export default SubscribeForm;
