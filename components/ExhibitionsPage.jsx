import React, { Component } from 'react';
import { Grid, Col } from 'react-bootstrap';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import connectToStores from 'fluxible-addons-react/connectToStores';

import loadCurrentExhibitionAction from '../actions/loadCurrentExhibition';
import loadExhibitionsAction from '../actions/loadExhibitions';
import loadMoreExhibitionsAction from '../actions/loadMoreExhibitions';
import setBodyThemeAction from '../actions/setBodyTheme';

import ExhibitionStore from '../stores/ExhibitionStore';

import ContactForm from './ContactForm';
import ExhibitionTile from './ExhibitionTile';
import FeatureModule from './FeatureModule';
import MasonryTiles from './MasonryTiles';

import andJoin from '../utils/andJoin';
import formattedDateRange from '../utils/formattedDateRange';

const exhibitionsPerPage = 18;

class ExhibitionsPage extends Component {
  renderFeatureModule() {
    const { featuredExhibition: exhibition } = this.props;
    const { artists } = exhibition;
    const formattedDate = formattedDateRange(exhibition.startDate, exhibition.endDate);
    const link = `/exhibition/${exhibition.slug}`;
    const artistNames = artists.length > 5 ? 'Group Exhibition' : andJoin(artists.map((artist) => artist.name.full));

    return (
      <FeatureModule image={exhibition.featuredImage} link={link}>
        <div>
          <h1 className="prepend-xs-none append-xs-1 append-md-2">
            <a className="link-plain text-underline" href={link}>
              {artists.length > 1 ? exhibition.title : artistNames}
            </a>
          </h1>
        </div>
        <h2 className="append-xs-1 append-md-2 text-spaced text-uppercase">
          {artists.length > 1 ? artistNames : exhibition.title}
        </h2>
        {formattedDate &&
          <p className="text-spaced text-uppercase">{formattedDate}</p>}
      </FeatureModule>
    );
  }

  renderExhibitionTiles() {
    const { exhibitions, loadingMoreExhibitions, allExhibitionsLoaded } = this.props;
    return (
      <MasonryTiles items={exhibitions} renderTile={(exhibition) => <ExhibitionTile exhibition={exhibition} />}>
        {!allExhibitionsLoaded &&
          <Col md={12} className="text-center prepend-xs-1">
            <button
              className="btn btn-body-default text-xs text-spaced text-uppercase prepend-xs-none"
              type="button"
              disabled={loadingMoreExhibitions}
              onClick={this.loadMoreExhibitions}>
              {loadingMoreExhibitions ? 'Loading...' : 'View more'}
            </button>
          </Col>}
      </MasonryTiles>
    );
  }

  render() {
    const { config } = this.context;
    const { exhibitions } = this.props;

    return (
      <DocumentTitle title={`Exhibitions - ${config.brand}`}>
        <div>
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs boredered--centred--md--none bordered--third--md">
            {this.renderFeatureModule()}
          </Grid>
          {_.any(exhibitions) &&
            <div className="accent-container">
              <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs boredered--centred--md--none bordered--third--md">
                {this.renderExhibitionTiles()}
              </Grid>
              <div className="accent bg-striped"></div>
            </div>}
          <ContactForm contextDescription={'about exhibitions.'} />
        </div>
      </DocumentTitle>
    );
  }

  loadMoreExhibitions() {
    const { currentExhibition: exhibition } = this.props;
    this.context.executeAction(loadMoreExhibitionsAction, {
      count: exhibitionsPerPage,
      exclude: exhibition ? exhibition.slug : void 0,
    });
  }

  constructor() {
    super();
    this.loadMoreExhibitions = this.loadMoreExhibitions.bind(this);
  }
}

ExhibitionsPage.contextTypes = {
  config: React.PropTypes.object.isRequired,
  executeAction: React.PropTypes.func.isRequired,
};

ExhibitionsPage.propTypes = {
  featuredExhibition: React.PropTypes.object.isRequired,
  currentExhibition: React.PropTypes.object.isRequired,
  exhibitions: React.PropTypes.array.isRequired,
  loadingMoreExhibitions: React.PropTypes.bool.isRequired,
  allExhibitionsLoaded: React.PropTypes.bool.isRequired,
};

ExhibitionsPage = connectToStores(ExhibitionsPage, [ ExhibitionStore ], (context) => {
  const exhibitionStore = context.getStore(ExhibitionStore);
  const exhibitions = exhibitionStore.getExhibitions().concat();

  const currentExhibition = exhibitionStore.getExhibition();
  let featuredExhibition = currentExhibition;
  if (!featuredExhibition) {
    featuredExhibition = exhibitions.splice(0, 1)[0];
  }

  return {
    featuredExhibition,
    currentExhibition,
    exhibitions,
    loadingMoreExhibitions: exhibitionStore.isLoadingMoreExhibitions(),
    allExhibitionsLoaded: exhibitionStore.isAllExhibitionsLoaded(),
  };
});

ExhibitionsPage = Resolver.createContainer(ExhibitionsPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    bodyTheme: (props, context) => {
      return context.executeAction(setBodyThemeAction, 'success');
    },

    exhibitions: (props, context) => {
      return context.executeAction(loadCurrentExhibitionAction).then(() => {
        const exhibition = context.getStore(ExhibitionStore).getExhibition();
        return context.executeAction(loadExhibitionsAction, { count: exhibitionsPerPage, exclude: exhibition ? exhibition.slug : void 0 });
      });
    },
  },
});

export default ExhibitionsPage;
