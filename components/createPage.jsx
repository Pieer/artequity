import React from 'react';
import { Resolver } from 'react-resolver';
import DocMeta from 'react-doc-meta';
import DocumentTitle from 'react-document-title';

import connectToStores from 'fluxible-addons-react/connectToStores';

import loadPageAction from '../actions/loadPage';
import PageStore from '../stores/PageStore';

export default function createPage(Component, resolutions = {}, pathGetter = (path) => path) {
  class Page extends React.Component {
    render() {
      const { page } = this.props;

      return (
        <DocumentTitle title={page.meta.title}>
          <div>
            <DocMeta tags={page.meta.tags} />
            <Component {...this.props} />
          </div>
        </DocumentTitle>
      );
    }
  }

  Page.propTypes = {
    page: React.PropTypes.object.isRequired,
  };

  Page = connectToStores(Page, [ PageStore ], (context) => {
    return {
      page: context.getStore(PageStore).getPage(),
    };
  });

  Page = Resolver.createContainer(Page, {
    contextTypes: {
      getStore: React.PropTypes.func.isRequired,
      executeAction: React.PropTypes.func.isRequired,
      router: React.PropTypes.func.isRequired,
    },

    resolve: {
      page: (props, context) => {
        const pathname = pathGetter(context.router.getCurrentPathname());
        return context.executeAction(loadPageAction, pathname);
      },

      ...resolutions,
    },
  });

  return Page;
}
