import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import DocMeta from 'react-doc-meta';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import connectToStores from 'fluxible-addons-react/connectToStores';
import moment from 'moment';
import 'moment-timezone';

import loadPostAction from '../actions/loadPost';
import loadPostsAction from '../actions/loadPosts';
import setBodyThemeAction from '../actions/setBodyTheme';

import PostStore from '../stores/PostStore';

import PostTile from './PostTile';
import FeatureModule from './FeatureModule';
import MasonryTiles from './MasonryTiles';
import SocialSharers from './SocialSharers';

class PostPage extends Component {
  renderFeatureModule() {
    const { post } = this.props;

    return (
      <FeatureModule image={post.image}>
        <h2 className="prepend-xs-3 text-spaced text-uppercase append-xs-3">{post.title}</h2>
        {post.publishedDate &&
          <p className="text-spaced text-uppercase append-xs-1">Posted: {moment(post.publishedDate).tz('Australia/Sydney').format('D MMMM YYYY')}</p>}
        <SocialSharers title={post.title} className="prepend-xs-2" />
      </FeatureModule>
    );
  }

  renderDetails() {
    const { post } = this.props;

    return (
      <Row className="bordered--xs bordered--top--xs flex-stretch-md">
        <Col md={8} mdOffset={4} className="pad-vertical-xs-1 pad-horizontal-xs-none pad-horizontal-md-1">
          <div className="pad-horizontal-xs-xtiny pad-xs-1 pad-sm-3 bg-white text-content accent-overlay">
            <div dangerouslySetInnerHTML={{__html: post.content.extended}}></div>
            {post.fileLabel && post.fileUpload && post.fileUpload.url &&
              <p className="prepend-xs-2"><a className="text-black text-underline" href={post.fileUpload.url} target="_blank">{post.fileLabel}</a></p>}
            {post.embedContent && post.embedContent.html &&
              <div className="prepend-xs-2 embed-container" dangerouslySetInnerHTML={{__html: post.embedContent.html}}></div>}
          </div>
        </Col>
      </Row>
    );
  }

  renderPostTiles() {
    const { posts } = this.props;

    return (
      <MasonryTiles items={posts}
        colSizes={{ xs: 12, md: 6, lg: 4 }}
        renderTile={(post) => <PostTile post={post} />}
        colClassName="postpad-vertical-xs-none" />
    );
  }

  render() {
    const { post, posts } = this.props;

    return (
      <DocumentTitle title={post.meta.title}>
        <div>
          <DocMeta tags={post.meta.tags} />
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            <Row className="bordered--xs bordered--bottom--xs">
              <Col md={12} className="pad-horizontal-xs-xtiny pad-vertical-xs-1 pad-sm-1">
                <h2 className="pend-vertical-xs-none">
                  \ <a className="link-plain" href="/posts">News</a>
                </h2>
              </Col>
            </Row>
            {this.renderFeatureModule()}
          </Grid>
          <div className="accent-container">
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
              {this.renderDetails()}
              <div className="accent bg-striped"></div>
            </Grid>
          </div>
          {_.any(posts) &&
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--third--lg">
              {this.renderPostTiles()}
            </Grid>}
        </div>
      </DocumentTitle>
    );
  }
}

PostPage.propTypes = {
  post: React.PropTypes.object.isRequired,
  posts: React.PropTypes.array.isRequired,
};

PostPage = connectToStores(PostPage, [ PostStore ], (context) => {
  const postStore = context.getStore(PostStore);
  return {
    post: postStore.getPost(),
    posts: postStore.getPosts(),
  };
});

PostPage = Resolver.createContainer(PostPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    bodyTheme: (props, context) => {
      return context.executeAction(setBodyThemeAction, 'info');
    },

    post: (props, context) => {
      return context.executeAction(loadPostAction, props.params.slug);
    },

    posts: (props, context) => {
      return context.executeAction(loadPostsAction, { count: 3, exclude: props.params.slug });
    },
  },
});

export default PostPage;
