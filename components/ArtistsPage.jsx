import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import classNames from 'classnames';
import connectToStores from 'fluxible-addons-react/connectToStores';

import loadArtistsAction from '../actions/loadArtists';
import loadExhibitionsAction from '../actions/loadExhibitions';
import loadTagsAction from '../actions/loadTags';

import ArtistStore from '../stores/ArtistStore';
import ExhibitionStore from '../stores/ExhibitionStore';
import TagStore from '../stores/TagStore';

import ArtistTile from './ArtistTile.jsx';
import ContactForm from './ContactForm.jsx';
import ExhibitionsModule from './ExhibitionsModule';
import MasonryTiles from './MasonryTiles';

class ArtistsPage extends Component {
  renderTag(tag, index) {
    const firstTag = this.props.tags[0];
    const { activeTag = firstTag ? firstTag.slug : void 0 } = this.props;

    return (
      <li key={tag.slug} className="append-horizontal-xs-1 append-horizontal-md-none prepend-horizontal-md-2">
        <a href={`/artists${index === 0 ? '' : `/${tag.slug}`}`} className={classNames(activeTag !== tag.slug ? 'link-plain text-black' : 'text-black text-underline')}>
          {tag.name} artists
        </a>
      </li>
    );
  }

  renderTags() {
    const { tags } = this.props;

    return (
      <ul className="list-inline list-inline--compact append-xs-none">
        {tags.map(this.renderTag)}
      </ul>
    );
  }

  renderArtistsListColumn(artists) {
    return (
      <ul className="list-unstyled append-xs-none">
        {artists.map((artist) => (
          <li key={artist.id} className="pend-vertical-xs-tiny pend-vertical-md-none">
            <a className="link-plain" href={`/artist/${artist.slug}`}>{artist.name.full}</a>
          </li>))}
      </ul>
    );
  }

  renderArtistsList() {
    const { artists } = this.props;

    const cols = 3;
    const maxPerColumn = Math.ceil(artists.length / cols);
    const groupedArtists = _.groupBy(artists, (artist, index) => {
      return Math.floor(index / maxPerColumn);
    });

    return (
      <Row className="bordered--md bordered--top--md flex-stretch-md text-center-xs text-left-md">
        <Col md={4} className="pad-horizontal-xs-xtiny prepad-vertical-xs-1 pad-horizontal-sm-1 pad-md-1">
          {this.renderArtistsListColumn(groupedArtists[0] || [])}
        </Col>
        <Col md={4} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 pad-md-1">
          {this.renderArtistsListColumn(groupedArtists[1] || [])}
        </Col>
        <Col md={4} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 postpad-vertical-xs-1 pad-md-1">
          {this.renderArtistsListColumn(groupedArtists[2] || [])}
        </Col>
      </Row>
    );
  }

  renderArtistTiles() {
    const { artists } = this.props;
    return <MasonryTiles items={artists} renderTile={(artist) => <ArtistTile artist={artist} />} />;
  }

  renderExhibitions() {
    const { exhibitions } = this.props;

    return (
      <Row className="bordered--xs bordered--top--xs">
        <div className="clearfix">
          <Col md={8} mdOffset={4} className="divider-container prepad-xs-2 postpad-xs-1 postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
            <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
              \ Upcoming exhibitions
            </h2>
            <div className="prepend-xs-1">
              <ExhibitionsModule exhibitions={exhibitions} linkToAll={true} />
            </div>
            <span className="divider divider--horizontal--xs divider--top-right--xs divider--top-right--md" />
          </Col>
        </div>
      </Row>
    );
  }

  render() {
    const { config } = this.context;
    const { exhibitions } = this.props;

    return (
      <DocumentTitle title={`Artists - ${config.brand}`}>
        <div>
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            <Row className="flex-stretch-md">
              <Col md={4} className="pad-horizontal-xs-xtiny pad-vertical-xs-1 pad-sm-1 bordered--xs bordered--bottom--xs bordered--md--none">
                <h2 className="pend-vertical-xs-none">\ Artists</h2>
              </Col>
              <Col md={8} className="pad-horizontal-xs-xtiny prepad-vertical-xs-1 pad-horizontal-sm-1 pad-md-1 text-right-md">
                {this.renderTags()}
              </Col>
            </Row>
            {this.renderArtistsList()}
          </Grid>
          <div className="accent-container">
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
              {this.renderArtistTiles()}
            </Grid>
            <div className="accent bg-striped"></div>
          </div>
          {_.any(exhibitions) &&
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
              {this.renderExhibitions()}
            </Grid>}
          <ContactForm contextDescription={'to find out more about our artists.'} />
        </div>
      </DocumentTitle>
    );
  }

  constructor() {
    super();

    this.renderTag = this.renderTag.bind(this);
  }
}

ArtistsPage.propTypes = {
  activeTag: React.PropTypes.string,
  artists: React.PropTypes.array.isRequired,
  tags: React.PropTypes.array.isRequired,
  exhibitions: React.PropTypes.array.isRequired,
};

ArtistsPage.contextTypes = {
  config: React.PropTypes.object.isRequired,
};

ArtistsPage = connectToStores(ArtistsPage, [ ArtistStore, ExhibitionStore, TagStore ], (context) => {
  return {
    artists: context.getStore(ArtistStore).getArtists(),
    tags: context.getStore(TagStore).getTags(),
    exhibitions: context.getStore(ExhibitionStore).getExhibitions(),
  };
});

ArtistsPage = Resolver.createContainer(ArtistsPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
    router: React.PropTypes.func.isRequired,
  },

  resolve: {
    activeTag: (props) => {
      return props.params.tag;
    },

    artists: (props, context) => {
      return context.executeAction(loadTagsAction).then(() => {
        const tags = context.getStore(TagStore).getTags();
        const firstTag = tags[0];
        const { tag } = context.router.getCurrentParams();
        return context.executeAction(loadArtistsAction, _.compact([ tag || (firstTag && firstTag.slug) ]));
      });
    },

    exhibitions: (props, context) => {
      return context.executeAction(loadExhibitionsAction, { count: 3 });
    },
  },
});

export default ArtistsPage;
