import React, { Component } from 'react';

import classNames from 'classnames';

class HoverBorder extends Component {
  render() {
    const { className, children } = this.props;

    return (
      <div className={classNames('h-border', className)}>
        {children}

        <div className="h-border-top" />
        <div className="h-border-left" />
        <div className="h-border-right" />
        <div className="h-border-bottom" />
      </div>
    );
  }
}

HoverBorder.propTypes = {
  className: React.PropTypes.any,
  children: React.PropTypes.any.isRequired,
};

export default HoverBorder;
