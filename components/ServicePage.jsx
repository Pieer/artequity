import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import DocMeta from 'react-doc-meta';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import CloudinaryImage from 'react-cloudinary-img';
import connectToStores from 'fluxible-addons-react/connectToStores';

import loadServiceAction from '../actions/loadService';
import setBodyThemeAction from '../actions/setBodyTheme';

import ServiceStore from '../stores/ServiceStore';

import ArtworkTile from './ArtworkTile';
import ContactForm from './ContactForm';
import MasonryTiles from './MasonryTiles';

class ServicePage extends Component {
  renderFeatureModule() {
    const { service } = this.props;
    const { details } = service;

    return (
      <div>
        <Row className="divider-container bordered--xs postpad-vertical-xs-1 postpad-md-none flex-stretch-md">
          <Col md={4} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-xs-1 postpad-vertical-xs-1 postpad-vertical-md-3 prepad-vertical-sm-3 prepad-vertical-lg-5">
            <h1 className="pre pend-xs-none">{service.title.split(' ').join('\n')}</h1>
            <span className="divider divider--vertical--xs divider--top-right--xs divider--top-left--sm" />
            <span className="divider divider--horizontal--xs divider--bottom-left--xs divider--bottom-right--sm" />
          </Col>
          <Col md={8} className="pad-horizontal-xs-none pad-md-1">
            <CloudinaryImage image={service.image} options={{ width: 820 * 2, height: 510 * 2, crop: 'fill' }} className="img-full-width" />
          </Col>
        </Row>
        <Row className="divider-container text-col-3-md">
          <Col md={12} className="postpad-vertical-xs-tiny postpad-vertical-sm-1 pad-horizontal-xs-xtiny pad-horizontal-sm-1">
            <div className="text-content text-lead" dangerouslySetInnerHTML={{ __html: details.content }} />
          </Col>
        </Row>
      </div>
    );
  }

  renderImage(image) {
    return (
      <Col key={image.public_id} md={4} className="accent-overlay pad-horizontal-xs-none pad-horizontal-sm-1 pad-vertical-xs-xtiny pad-vertical-sm-tiny">
        <CloudinaryImage image={image} options={{ width: 380 * 2 }} className="img-full-width" />
      </Col>
    );
  }

  renderImages() {
    const { service } = this.props;
    const { images } = service.details;

    return (
      <div>
        <Row>
          <Col md={4} mdOffset={8} className="divider-container prepad-vertical-md-2">
            <span className="divider divider--vertical--md divider--bottom-right--md" />
          </Col>
        </Row>
        <Row className="divider-container bordered--xs pad-vertical-xs-xtiny pad-vertical-sm-tiny pad-vertical-md-none bordered--xs bordered--top--xs bordered--md--none">
          {images.map(this.renderImage)}
        </Row>
        <Row>
          <Col md={4} className="divider-container prepad-vertical-md-2">
            <span className="divider divider--vertical--md divider--top-right--md" />
          </Col>
        </Row>
      </div>
    );
  }

  renderInstructions() {
    const { service } = this.props;
    const { instructions } = service;

    return (
      <Row className="divider-container bordered--xs bordered--xs bordered--top--xs bordered--md--none append-md--2 postpad-vertical-md-1">
        <Col md={4} mdOffset={4} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 pad-vertical-xs-1 postpad-xs-none postpad-md-1">
          <h1 className="prepend-xs-none">{instructions.title}</h1>
        </Col>
        <Col md={4} className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-xs-none postpad-vertical-xs-1 pad-vertical-md-1">
          <div className="text-content" dangerouslySetInnerHTML={{ __html: instructions.content }} />
        </Col>
      </Row>
    );
  }

  renderArtworkTiles() {
    const { service } = this.props;
    const { artworks } = service.instructions;
    return (
      <MasonryTiles
        className="bordered--md--none"
        items={artworks}
        renderTile={(artwork) => <ArtworkTile artwork={artwork} showArtist={false} />} />
    );
  }

  render() {
    const { service } = this.props;
    const { images } = service.details;
    const { artworks } = service.instructions;

    return (
      <DocumentTitle title={service.meta.title}>
        <div>
          <DocMeta tags={service.meta.tags} />

          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            {this.renderFeatureModule()}
          </Grid>

          {_.any(images) &&
            <div className="accent-container">
              <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
                {this.renderImages()}
              </Grid>
              <div className="accent bg-striped"></div>
            </div>}

          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            {this.renderInstructions()}
          </Grid>

          {_.any(artworks) &&
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
              {this.renderArtworkTiles()}
            </Grid>}

          <ContactForm contextDescription={'to find out more.'} />
        </div>
      </DocumentTitle>
    );
  }
}

ServicePage.propTypes = {
  service: React.PropTypes.object.isRequired,
};

ServicePage = connectToStores(ServicePage, [ ServiceStore ], (context) => {
  return {
    service: context.getStore(ServiceStore).getService(),
  };
});

ServicePage = Resolver.createContainer(ServicePage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    bodyTheme: (props, context) => {
      return context.executeAction(setBodyThemeAction, 'eggplant');
    },

    service: (props, context) => {
      return context.executeAction(loadServiceAction, props.params.slug);
    },
  },
});

export default ServicePage;
