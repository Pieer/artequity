import React, { Component } from 'react';

import CloudinaryImage from 'react-cloudinary-img';
import numeral from 'numeral';

class PublicationTile extends Component {
  render() {
    const { publication } = this.props;

    return (
      <div className="bg-white pad-horizontal-xs-tiny pad-vertical-xs-1 pad-horizontal-md-1">
        <CloudinaryImage image={publication.image} options={{ width: 380 * 2 }} className="img-full-width" />
        <h2 className="text-spaced text-uppercase append-xs-1">
          {publication.title}
        </h2>
        <div dangerouslySetInnerHTML={{ __html: publication.description }} />
        <div className="flex-center-xs">
          <div className="paypal-button" dangerouslySetInnerHTML={{ __html: publication.paypalCode }} />
          <div className="text-right flex-item-grow-xs">
            <div className="inline-block pad-vertical-xs-1 text-center text-black text-sm bg-circle-stripes">
              {numeral(publication.price).format('$0,0.00')}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PublicationTile.propTypes = {
  publication: React.PropTypes.object.isRequired,
};

export default PublicationTile;
