import React, { Component } from 'react';

import classNames from 'classnames';

class SocialSharers extends Component {
  render() {
    const { router, config } = this.context;
    const { title, className, printable } = this.props;
    const url = `${config.root}${router.getCurrentPath()}`;
    const shareBody = `${title} - ${url}`;

    const sharingOptions = [
      { icon: 'ion-social-facebook', url: `https://www.facebook.com/sharer/sharer.php?u=${url}` },
      { icon: 'ion-social-twitter', url: `https://twitter.com/home?status=${shareBody}` },
      { icon: 'ion-email', url: `mailto:?body=${shareBody}` },
    ];

    return (
      <ul className={classNames('list-inline list-inline--compact append-xs-none', className)}>
        {sharingOptions.map((option) => (
          <li key={option.icon} className="pend-horizontal-xs-tiny prepend-horizontal-md-none">
            <a href={option.url} className={classNames(option.icon, 'btn btn-hollow btn-hollow-default btn-social link-plain')} target="_blank"></a>
          </li>))}
        {printable &&
          <li className="pend-horizontal-xs-tiny prepend-horizontal-md-none">
            <a href="#" className="ion-ios-printer btn btn-hollow btn-hollow-default btn-social link-plain" onClick={this.print}></a>
          </li>}
      </ul>
    );
  }

  print(e) {
    window.print();
    e.preventDefault();
  }
}

SocialSharers.contextTypes = {
  router: React.PropTypes.func.isRequired,
  config: React.PropTypes.object.isRequired,
};

SocialSharers.propTypes = {
  title: React.PropTypes.string.isRequired,
  className: React.PropTypes.string,
  printable: React.PropTypes.bool,
};

export default SocialSharers;
