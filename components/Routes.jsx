import React from 'react';
import { DefaultRoute, Route, NotFoundRoute } from 'react-router';

import Application from './Application.jsx';
import AboutPage from './AboutPage.jsx';
import ArtistPage from './ArtistPage.jsx';
import ArtistsPage from './ArtistsPage.jsx';
import ArtworkPage from './ArtworkPage.jsx';
import ContactPage from './ContactPage.jsx';
import DefaultLayout from './DefaultLayout.jsx';
import ExhibitionPage from './ExhibitionPage.jsx';
import ExhibitionsPage from './ExhibitionsPage.jsx';
import EventPage from './EventPage.jsx';
import HomePage from './HomePage.jsx';
import NotFoundPage from './NotFoundPage.jsx';
import PostPage from './PostPage.jsx';
import PostsPage from './PostsPage.jsx';
import PrivacyPage from './PrivacyPage.jsx';
import PrivateTreatyPage from './PrivateTreatyPage.jsx';
import PublicationsPage from './PublicationsPage.jsx';
import ServicePage from './ServicePage.jsx';
import ServicesPage from './ServicesPage.jsx';

export default (
  <Route name="app" path="/" handler={Application}>
    <Route path="/" handler={DefaultLayout}>
      <Route name="about" handler={AboutPage} />
      <Route name="contact" handler={ContactPage} />
      <Route name="privacy" handler={PrivacyPage} />

      <Route name="private-treaty" handler={PrivateTreatyPage} />
      <Route path="private-treaty/:tag" handler={PrivateTreatyPage} />

      <Route name="services" handler={ServicesPage} />
      <Route name="service/:slug" handler={ServicePage} />

      <Route name="artists" handler={ArtistsPage} />
      <Route path="artists/:tag" handler={ArtistsPage} />
      <Route path="artist/:slug" handler={ArtistPage} />

      <Route path="exhibitions" handler={ExhibitionsPage} />
      <Route path="exhibition/:slug" handler={ExhibitionPage} />

      <Route path="artwork/:slug" handler={ArtworkPage} />
      <Route path="exhibition/:exhibitionSlug/artwork/:slug" handler={ArtworkPage} />
      <Route path="private-treaty/artwork/:slug" handler={ArtworkPage} />

      <Route path="event/:slug" handler={EventPage} />

      <Route name="posts" handler={PostsPage} />
      <Route path="posts/:category" handler={PostsPage} />
      <Route path="post/:slug" handler={PostPage} />

      <Route name="publications" handler={PublicationsPage} />

      <DefaultRoute handler={HomePage} />
    </Route>
    <NotFoundRoute handler={NotFoundPage}/>
  </Route>
);
