import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import DocMeta from 'react-doc-meta';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import connectToStores from 'fluxible-addons-react/connectToStores';

import loadArtistAction from '../actions/loadArtist';
import loadArtistArtworksAction from '../actions/loadArtistArtworks';
import loadArtistExhibitionsAction from '../actions/loadArtistExhibitions';
import loadPostsAction from '../actions/loadPosts';
import setBodyThemeAction from '../actions/setBodyTheme';

import ArtistStore from '../stores/ArtistStore';
import ArtworkStore from '../stores/ArtworkStore';
import ExhibitionStore from '../stores/ExhibitionStore';
import PostStore from '../stores/PostStore';

import ArtistBioModule from './ArtistBioModule';
import ArtworkTile from './ArtworkTile';
import ContactForm from './ContactForm';
import FeatureModule from './FeatureModule';
import ExhibitionsModule from './ExhibitionsModule';
import MasonryTiles from './MasonryTiles';
import PostTile from './PostTile';

import colors from '../utils/colors';

class ArtistPage extends Component {
  renderFeatureModule() {
    const { artist } = this.props;
    let featuredArtworkImage = artist.featuredArtwork && artist.featuredArtwork.image;
    featuredArtworkImage = featuredArtworkImage ? featuredArtworkImage : void 0;

    return (
      <FeatureModule image={artist.featuredImage && artist.featuredImage.public_id ? artist.featuredImage : featuredArtworkImage}>
        <h1 className="prepend-xs-none append-xs-1 append-md-4">
          {artist.name.full}
        </h1>
        {artist.shortDescription && <p className="append-xs-none text-spaced text-uppercase">\ {artist.shortDescription}</p>}
      </FeatureModule>
    );
  }

  renderSidebar() {
    const { artist } = this.props;

    return (
      <Col md={4} className="divider-container bordered--xs bordered--top--xs bordered--md--none prepad-vertical-xs-1 postpad-vertical-xs-1 postpad-vertical-md-3 pad-horizontal-xs-none pad-horizontal-md-1">
        <ArtistBioModule artist={artist} />
        <span className="divider divider--vertical--xs divider--bottom-right--xs divider--bottom-right--md" />
      </Col>
    );
  }

  renderBio() {
    const { artist } = this.props;

    return (
      <Col md={8} className="pad-vertical-xs-1 pad-horizontal-xs-none pad-horizontal-md-1">
        <div className="pad-horizontal-xs-xtiny pad-xs-1 pad-horizontal-sm-3 pad-vertical-sm-2 bg-white text-content">
          <div dangerouslySetInnerHTML={{__html: artist.bio}}></div>
        </div>
      </Col>
    );
  }

  renderProfile() {
    return (
      <Row className="bordered--xs bordered--top--xs flex-stretch-md">
        {this.renderSidebar()}
        {this.renderBio()}
      </Row>
    );
  }

  renderArtworkTiles() {
    const { artworks } = this.props;
    return (
      <MasonryTiles title="Artworks" items={artworks}
        renderTile={(artwork) => <ArtworkTile artwork={artwork} showArtist={false} />} />
    );
  }

  renderNews() {
    const { latestPost } = this.props;

    return (
      <Col md={4} className=" divider-container prepad-xs-2 postpad-xs-1 postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
        {latestPost &&
          <div>
            <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
              \ News
            </h2>
            <div className="prepend-xs-1">
              <PostTile post={latestPost} linkToAll={true} />
            </div>
          </div>}
      </Col>
    );
  }

  renderExhibitions() {
    const { artist, exhibitions } = this.props;

    return (
      <Col md={8} className="divider-container prepad-xs-2 postpad-xs-1 postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
        <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
          \ Exhibitions featuring {artist.name.full}
        </h2>
        <div className="prepend-xs-1">
          <ExhibitionsModule exhibitions={exhibitions} />
        </div>
        <span className="divider divider--horizontal--xs divider--top-right--xs divider--top-right--md" />
      </Col>
    );
  }

  render() {
    const { artist, artworks, exhibitions, latestPost } = this.props;

    return (
      <DocumentTitle title={artist.meta.title}>
        <div>
          <DocMeta tags={artist.meta.tags} />
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            <Row className="bordered--xs bordered--bottom--xs bordered--centred--xs bordered--centred--md--none">
              <Col md={12} className="bordered--third--md pad-horizontal-xs-xtiny pad-vertical-xs-1 pad-sm-1">
                <h2 className="pend-vertical-xs-none">
                  \ <a className="link-plain" href="/artists">Artists</a>
                </h2>
              </Col>
            </Row>
            {this.renderFeatureModule()}
            {this.renderProfile()}
          </Grid>
          {_.any(artworks) &&
            <div className="accent-container">
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
                {this.renderArtworkTiles()}
              </Grid>
              <div className="accent bg-striped"></div>
            </div>}
          {(_.any(exhibitions) || latestPost) &&
            <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
              <Row className="bordered--xs bordered--top--xs">
                <div className="clearfix">
                  {this.renderNews()}
                  {this.renderExhibitions()}
                </div>
              </Row>
            </Grid>}
          <ContactForm
            context={{ 'related_artist': artist.name.full }}
            contextDescription={`to find out more about ${artist.name.full}.`} />
        </div>
      </DocumentTitle>
    );
  }
}

ArtistPage.contextTypes = {
  executeAction: React.PropTypes.func.isRequired,
};

ArtistPage.propTypes = {
  artist: React.PropTypes.object.isRequired,
  artworks: React.PropTypes.array.isRequired,
  exhibitions: React.PropTypes.array.isRequired,
  latestPost: React.PropTypes.object,
};

ArtistPage = connectToStores(ArtistPage, [ ArtistStore, ArtworkStore, ExhibitionStore, PostStore ], (context) => {
  return {
    artist: context.getStore(ArtistStore).getArtist(),
    artworks: context.getStore(ArtworkStore).getArtistArtworks(),
    exhibitions: context.getStore(ExhibitionStore).getArtistExhibitions(),
    latestPost: context.getStore(PostStore).getPosts()[0],
  };
});

ArtistPage = Resolver.createContainer(ArtistPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    artist: (props, context) => {
      return context.executeAction(loadArtistAction, props.params.slug).then(() => {
        const artist = context.getStore(ArtistStore).getArtist();
        if (artist) {
          return context.executeAction(setBodyThemeAction, colors[artist.color]);
        }
      });
    },

    artworks: (props, context) => {
      return context.executeAction(loadArtistArtworksAction, props.params.slug);
    },

    exhibitions: (props, context) => {
      return context.executeAction(loadArtistExhibitionsAction, props.params.slug);
    },

    latestPost: (props, context) => {
      return context.executeAction(loadPostsAction, { count: 1, artist: props.params.slug });
    },
  },
});

export default ArtistPage;
