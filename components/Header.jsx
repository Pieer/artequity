import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import _ from 'lodash';
import classNames from 'classnames';

import Logo from './Logo';

const showNews = true;
const showEvents = true;
const showPublications = true;
const showPrivate = true;

class Header extends Component {
  renderLink(link) {
    const path = this.context.router.getCurrentPath();
    const isActive = link.regex.test(path);

    return (
      <li key={link.path} className={classNames(isActive && '', 'append-vertical-xs-1 append-md-none')}>
        <a className={classNames(isActive ? 'text-body-default text-underline text-black fixed-line-height-height' : 'link-plain')} href={link.path}>
          {link.label}
        </a>
      </li>
    );
  }

  renderLinks(links) {
    return (
      <ul className="list-unstyled append-xs-none" role="nav">
        {links.map(this.renderLink)}
      </ul>
    );
  }

  renderContactLink() {
    const path = this.context.router.getCurrentPath();
    const regex = /\/contact/;
    const isActive = regex.test(path);

    return (
      <a className={classNames('pull-right text-black text-underline', !isActive && 'link-plain' )} href="/contact">
        Contact
      </a>
    );
  }

  render() {
    const { showMenuOnMobile } = this.state;

    const links = _.compact([
      { label: 'Artists', path: '/artists', regex: /^\/artist/ },
      { label: 'Exhibitions', path: '/exhibitions', regex: /^\/exhibition/ },
      showPrivate && { label: 'Private Treaty', path: '/private-treaty', regex: /^\/private-treaty/ },
      { label: 'Art Rental', path: '/service/art-rental', regex: /^\/service\/art-rental/ },
      { label: `Services${showEvents ? ' \\ Events' : ''}`, path: '/services', regex: /^\/services/ },
      showNews && { label: 'News', path: '/posts', regex: /^\/post/ },
      { label: 'About', path: '/about', regex: /\/about/ },
      showPublications && { label: 'Publications', path: '/publications', regex: /\/publications/ },
    ]);

    const cols = 2;
    const maxPerColumn = Math.ceil(links.length / cols);
    const groupedLinks = _.groupBy(links, (link, index) => {
      return Math.floor(index / maxPerColumn);
    });

    return (
      <div className="bordered--xs bordered--bottom--xs">
        <Grid>
          <Row>
            <Col md={4} className="clearfix pad-horizontal-xs-none pad-vertical-xs-tiny pad-sm-1">
              <div className="btn-group pull-left">
                <Logo />
                <button className="non-printable btn btn-menu btn-hollow-thick btn-hollow-white hidden-md hidden-lg"
                  onClick={this.toggleMobileMenu}>
                  <span className="ion-navicon-round"></span>
                </button>
              </div>
              <div className="non-printable hidden-md hidden-lg pad-vertical-xs-tiny pend-vertical-xs-tiny pend-horizontal-xs-tiny">
                {this.renderContactLink()}
              </div>
            </Col>
            <div className={classNames('non-printable', !showMenuOnMobile && 'hidden-xs hidden-sm')}>
              <Col md={4} className="pad-horizontal-xs-none pad-horizontal-sm-1 pad-md-1 ">
                {this.renderLinks(groupedLinks[0] || [])}
              </Col>
              <Col md={4} className="clearfix pad-horizontal-xs-none postpad-vertical-xs-tiny postpad-vertical-sm-1 pad-horizontal-sm-1 pad-md-1">
                <div className="pull-left">
                  {this.renderLinks(groupedLinks[1] || [])}
                </div>
                <span className="hidden-xs hidden-sm">{this.renderContactLink()}</span>
              </Col>
            </div>
          </Row>
        </Grid>
      </div>
    );
  }

  toggleMobileMenu() {
    this.setState({ showMenuOnMobile: !this.state.showMenuOnMobile });
  }

  constructor() {
    super();

    this.renderLink = this.renderLink.bind(this);
    this.toggleMobileMenu = this.toggleMobileMenu.bind(this);

    this.state = { showMenuOnMobile: false };
  }
}

Header.contextTypes = {
  router: React.PropTypes.func.isRequired,
};

export default Header;
