import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import DocMeta from 'react-doc-meta';
import DocumentTitle from 'react-document-title';
import { Resolver } from 'react-resolver';

import _ from 'lodash';
import connectToStores from 'fluxible-addons-react/connectToStores';
import moment from 'moment';
import 'moment-timezone';


import loadEventAction from '../actions/loadEvent';
import loadEventsAction from '../actions/loadEvents';
import setBodyThemeAction from '../actions/setBodyTheme';

import EventStore from '../stores/EventStore';

import CalendarButton from './CalendarButton';
import ContactForm from './ContactForm';
import FeatureModule from './FeatureModule';
import EventsModule from './EventsModule';
import SocialSharers from './SocialSharers';

import colors from '../utils/colors';

class EventPage extends Component {
  renderFeatureModule() {
    const { event } = this.props;

    return (
      <FeatureModule image={event.featuredImage}>
        <h2 className="text-spaced text-uppercase prepend-xs-none append-xs-1 append-md-2">
          {event.title}
        </h2>
        <h3 className="append-xs-1">

          {moment(event.date).tz('Australia/Sydney').format('dddd D MMMM')}

        </h3>
        {event.time && <p className="text-spaced text-uppercase append-xs-none">{event.time}</p>}
        <SocialSharers title={event.title} className="prepend-xs-2" />
      </FeatureModule>
    );
  }

  renderSidebar() {
    const { event } = this.props;

    return (
      <Col md={4} className="divider-container bordered--xs bordered--bottom--xs bordered--md--none prepad-vertical-xs-1 postpad-vertical-xs-1 postpad-vertical-md-2 pad-horizontal-xs-none pad-horizontal-md-1">
        <div className="pad-horizontal-xs-xtiny pad-xs-1 pad-sm-2 pad-horizontal-md-1 pad-horizontal-lg-2 bg-white text-center text-content append-xs-tiny append-md-2">
          <h3>{event.address.full}</h3>
          <CalendarButton
            className="btn-block"
            buttonClassName="btn btn-hollow btn-block btn-hollow-default text-xs text-spaced text-uppercase" type="button"
            slug={event.slug}
            title={event.title}
            startDate={moment(event.date).tz('Australia/Sydney').format()}
            endDate={moment(event.endTime).tz('Australia/Sydney').format() || moment(event.date).tz('Australia/Sydney').format()}
            address={event.address.full.replace(/,/g, '')}>
            Add to calendar
          </CalendarButton>
          {event.rsvpLink &&
            <div className="prepend-xs-1">
              <a className="btn btn-block btn-body-default text-xs text-spaced text-uppercase" href={event.rsvpLink} target="_blank">
                RSVP
              </a>
            </div>}
        </div>
        <span className="divider divider--vertical--xs divider--bottom-right--xs divider--bottom-right--md" />
      </Col>
    );
  }

  renderDetails() {
    const { event } = this.props;

    return (
      <Col md={8} className="pad-vertical-xs-1 pad-horizontal-xs-none pad-horizontal-md-1">
        <div className="pad-horizontal-xs-xtiny pad-xs-1 pad-sm-3 bg-white text-content">
          <div dangerouslySetInnerHTML={{__html: event.content.extended}}></div>
        </div>
      </Col>
    );
  }

  renderProfile() {
    return (
      <Row className="bordered--xs bordered--top--xs flex-stretch-md">
        {this.renderSidebar()}
        {this.renderDetails()}
      </Row>
    );
  }

  renderEvents() {
    const { events } = this.props;

    return (
      <Row className="bordered--xs bordered--top--xs">
        <div className="clearfix">
          <Col md={8} mdOffset={4} className="divider-container prepad-xs-2 postpad-xs-1 postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1 accent-overlay">
            <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
              \ Other events
            </h2>
            <div className="prepend-xs-1">
              <EventsModule events={events} />
            </div>
            <span className="divider divider--horizontal--xs divider--top-right--xs divider--top-right--md" />
          </Col>
        </div>
      </Row>
    );
  }

  render() {
    const { event, events } = this.props;

    return (
      <DocumentTitle title={event.meta.title}>
        <div>
          <DocMeta tags={event.meta.tags} />
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            <Row className="bordered--xs bordered--bottom--xs">
              <Col md={12} className="pad-horizontal-xs-xtiny pad-vertical-xs-1 pad-sm-1">
                <h2 className="pend-vertical-xs-none">
                  \ <a className="link-plain" href="/services#events">Events</a>
                </h2>
              </Col>
            </Row>
            {this.renderFeatureModule()}
            {this.renderProfile()}
          </Grid>
          {_.any(events) &&
            <div className="accent-container">
              <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
                {this.renderEvents()}
              </Grid>
              <div className="accent bg-striped"></div>
            </div>}
          <ContactForm contextDescription={`to find out more about ${event.title}.`} />
        </div>
      </DocumentTitle>
    );
  }
}

EventPage.propTypes = {
  event: React.PropTypes.object.isRequired,
  events: React.PropTypes.array.isRequired,
};

EventPage = connectToStores(EventPage, [ EventStore ], (context) => {
  const eventStore = context.getStore(EventStore);
  return {
    event: eventStore.getEvent(),
    events: eventStore.getEvents(),
  };
});

EventPage = Resolver.createContainer(EventPage, {
  contextTypes: {
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    event: (props, context) => {
      return context.executeAction(loadEventAction, props.params.slug).then(() => {
        const event = context.getStore(EventStore).getEvent();
        if (event) {
          return context.executeAction(setBodyThemeAction, colors[event.color]);
        }
      });
    },

    events: (props, context) => {
      return context.executeAction(loadEventsAction, { count: 3, exclude: props.params.slug });
    },
  },
});

export default EventPage;
