import React, { Component } from 'react';
import classNames from 'classnames';

import CloudinaryImage from 'react-cloudinary-img';
import moment from 'moment';
import 'moment-timezone';

class EventsModule extends Component {
  renderEvent(event, index) {
    const { events } = this.props;
    const eventUrl = `/event/${event.slug}`;

    return (
      <div key={event.id} className={classNames('media', index === events.length - 1 ? '' : 'append-xs-2')}>
        <div className="media-object media-middle media-left">
          <a href={eventUrl}>
            <CloudinaryImage image={event.thumbnailImage} options={{ width: 180, height: 180, crop: 'fill' }} className="img-square-small-xs" />
          </a>
        </div>
        <div className="media-body media-right media-middle">
          <h2 className="text-spaced text-uppercase prepend-xs-none append-xs-1">
            <a href={eventUrl} className="link-plain">{event.title}</a>
          </h2>
          <p className="append-xs-none text-spaced text-uppercase">
            {moment(event.date).tz('Australia/Sydney').format('dddd D MMMM')}
          </p>
        </div>
      </div>
    );
  }

  render() {
    const { events } = this.props;

    return (
      <div className="pad-horizontal-xs-xtiny pad-horizontal-sm-1 pad-vertical-xs-1 pad-sm-1 bg-white">
        {events.map(this.renderEvent)}
      </div>
    );
  }

  constructor() {
    super();
    this.renderEvent = this.renderEvent.bind(this);
  }
}

EventsModule.propTypes = {
  events: React.PropTypes.array.isRequired,
};

export default EventsModule;
