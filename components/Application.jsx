import React, { Component } from 'react';
import DocMeta from 'react-doc-meta';
import { RouteHandler } from 'react-router';

import connectToStores from 'fluxible-addons-react/connectToStores';
import provideContext from 'fluxible-addons-react/provideContext';

import ApplicationStore from '../stores/ApplicationStore';

import ErrorPage from './ErrorPage';

class Application extends Component {
  getChildContext() {
    const { config } = this.props;
    return {
      cloudName: config.cloudinary.cloudName,
      config,
    };
  }

  componentDidMount() {
    this.loadWebFonts();
  }

  render() {
    const { config, bodyTheme, error } = this.props;

    return (
      <div className={`body-${bodyTheme}`}>
        <DocMeta tags={config.defaultMetaTags} />
        {error && <ErrorPage {...error} />}
        {!error && <RouteHandler />}
      </div>
    );
  }

  loadWebFonts() {
    global.WebFontConfig = {
      google: {
        families: [
          'Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic:latin',
        ],
      },
    };

    const wf = document.createElement('script');
    wf.src = (document.location.protocol === 'https:' ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    const s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  }
}

Application.propTypes = {
  config: React.PropTypes.object.isRequired,
  bodyTheme: React.PropTypes.string,
  error: React.PropTypes.shape({
    code: React.PropTypes.number.isRequired,
    message: React.PropTypes.string.isRequired,
  }),
};

Application.contextTypes = {
  executeAction: React.PropTypes.func.isRequired,
};

Application.childContextTypes = {
  cloudName: React.PropTypes.string,
  config: React.PropTypes.object,
};

Application = provideContext(connectToStores(Application, [ ApplicationStore ], (context) => {
  return context.getStore(ApplicationStore).getState();
}));

export default Application;
