import React, { Component } from 'react';
import { Grid, Row, Col, Input } from 'react-bootstrap';
import DocMeta from 'react-doc-meta';
import DocumentTitle from 'react-document-title';
import reactMixin from 'react-mixin';
import { Resolver } from 'react-resolver';
import Portal from 'react-portal';

import _ from 'lodash';
import classNames from 'classnames';
import CloudinaryImage from 'react-cloudinary-img';
import connectToStores from 'fluxible-addons-react/connectToStores';

import loadArtworkAction from '../actions/loadArtwork';
import loadArtworksAction from '../actions/loadArtworks';
import loadArtworkExhibitionsAction from '../actions/loadArtworkExhibitions';
import loadArtistArtworksAction from '../actions/loadArtistArtworks';
import loadExhibitionAction from '../actions/loadExhibition';
import loadExhibitionArtworksAction from '../actions/loadExhibitionArtworks';
import setBodyThemeAction from '../actions/setBodyTheme';

import ArtworkStore from '../stores/ArtworkStore';
import ApplicationStore from '../stores/ApplicationStore';
import ExhibitionStore from '../stores/ExhibitionStore';

import ArtworkTile from './ArtworkTile';
import ExhibitionsModule from './ExhibitionsModule';
import FormStackForm from './FormStackForm';
import MasonryTiles from './MasonryTiles';
import NumberRevealer from './NumberRevealer';
import SocialSharers from './SocialSharers';

import colors from '../utils/colors';
var trim = function() {
  var TRIM_RE = /^\s+|\s+$/g
  return function trim(string) {
    return string.replace(TRIM_RE, '')
  }
}();
class ArtworkPage extends Component {
  onSubmit() {
    const valid = this.isValid();
    if(valid) this.setState({ error: void 0, loading: true, sent: false });
    return valid;
  }

  onError(err) {
    this.setState({ error: err.message, loading: false });
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  isValid() {
    var fields = ['first_name', 'last_name', 'email','message'];
    var errors = {};
    fields.forEach(function(field) {
      var value = trim(this.refs[field].refs.input.getDOMNode().value);

      if (!value) {
        errors[field] = 'This field is required'
      }else{
        if(field == 'email' && !this.validateEmail(value)){
          errors[field] = 'Please complete all required fields'
        }
      }
    }.bind(this));

    var isValid = true;
    var error = false;
    for (var er in errors) {
      isValid = false;
      error = 'Please complete all required fields';
      break;
    }
    this.setState({errors: errors, error: error});
    return isValid;
  }

  onSuccess() {
    this.setState({
      loading: false,
      sent: true,
      firstName: void 0,
      lastName: void 0,
      email: void 0,
      enquiry: '',
      error: void 0,
    });
  }

  renderArtistBreadcrumb() {
    const { artist } = this.props;

    return (
      <h2 className="pend-vertical-xs-none">
        \ <a className="link-plain append-horizontal-xs-1" href="/artists">Artists</a>
        <small className="text-body">
          \ <a className="link-plain" href={`/artist/${artist.slug}`}>{artist.name.full}</a>
        </small>
      </h2>
    );
  }

  renderExhibitionBreadcrumb() {
    const { exhibition } = this.props;

    return (
      <h2 className="pend-vertical-xs-none">
        \ <a className="link-plain append-horizontal-xs-1" href="/exhibitions">Exhibitions</a>
        <small className="text-body">
          \ <a className="link-plain" href={`/exhibition/${exhibition.slug}`}>{exhibition.title}</a>
        </small>
      </h2>
    );
  }

  renderPrivateTreatyBreadcrumb() {
    return (
      <h2 className="pend-vertical-xs-none">
        \ <a className="link-plain append-horizontal-xs-1" href="/private-treaty">Private Treaty</a>
      </h2>
    );
  }

  renderBreadcrumb() {
    const { exhibition, isFromDealingFrom } = this.props;

    return (
      <Col md={4} className="pad-horizontal-xs-xtiny pad-vertical-xs-1 pad-sm-1">
        {exhibition && this.renderExhibitionBreadcrumb()}
        {isFromDealingFrom && this.renderPrivateTreatyBreadcrumb()}
        {!exhibition && !isFromDealingFrom && this.renderArtistBreadcrumb()}
      </Col>
    );
  }

  renderNavigationLinks(fromGallery) {
    const { artwork, artworks } = this.props;
    const currentIndex = _.findIndex(artworks, { slug: artwork.slug });
    const previousArtwork = artworks[currentIndex - 1 < 0 ? artworks.length - 1 : currentIndex - 1];
    const nextArtwork = artworks[(currentIndex + 1) % artworks.length];

    const sizeClass = fromGallery ? 'h2' : 'h1';
    const iconClassName = fromGallery ? '' : 'text-sm';

    return (
      <div>
        <a className={sizeClass} href={`${this.artworkUrl(previousArtwork.slug)}${fromGallery ? '?_' : ''}`}>
          <span className={classNames(iconClassName, 'fa-chevron-left')} />
        </a>
        {fromGallery &&
          <a className={classNames(sizeClass, 'prepend-horizontal-xs-3 text-baseline-adjust')} href="#" onClick={this.closePortal}>
            <span className={classNames(iconClassName, 'fa-times')} />
          </a>}
        <a className={classNames(sizeClass, 'prepend-horizontal-xs-3')} href={`${this.artworkUrl(nextArtwork.slug)}${fromGallery ? '?_' : ''}`}>
          <span className={classNames(iconClassName, 'fa-chevron-right')} />
        </a>
      </div>
    );
  }

  renderNavigation() {
    return (
      <Col md={4} className="non-printable pad-horizontal-xs-xtiny pad-vertical-xs-1 prepad-vertical-xs-none pad-sm-1 text-center-xs text-right-md">
        {this.renderNavigationLinks()}
      </Col>
    );
  }

  renderArtwork(fromGallery) {
    const { artwork } = this.props;
    return (
      <Col md={fromGallery ? 9 : 8} className={classNames('pad-horizontal-xs-none pad-xs-1 pad-vertical-xs-none pad-md-1 flex-item-flex-start-md text-center', fromGallery && 'text-right-md')}>
        <a href="#" onClick={fromGallery ? this.closePortal : this.openPortal}>
          <CloudinaryImage image={artwork.image} options={{ width: 820 * 2 }} className="img-full-width" />
        </a>
      </Col>
    );
  }

  renderInfo(fromGallery) {
    const { config } = this.context;
    const { artwork } = this.props;
    const { firstName, lastName, email, enquiry, sent, loading, error } = this.state;

    return (
      <Col md={fromGallery ? 3 : 4} className={classNames('pad-horizontal-xs-xtiny pad-horizontal-sm-1 prepad-vertical-xs-1 postpad-vertical-xs-1', !fromGallery && 'prepad-vertical-md-2')}>
        {fromGallery &&
          <div className="append-xs-1 append-md-2">
            {this.renderNavigationLinks(fromGallery)}
          </div>}

        <div className="divider-container postpad-xs-1 postpad-md-2">
          <div>
            <h2 className="text-black prepend-xs-none append-xs-1 append-md-2">
              <span className="text-underline">{artwork.artist.name.full}</span>
            </h2>
          </div>
          <h2 className="append-xs-1 append-md-2 text-spaced text-uppercase">
            {artwork.title}
          </h2>
          <p className="append-md-1">
            {artwork.year && <span>{artwork.year}<br/></span>}
            {artwork.medium && <span>{artwork.medium}<br/></span>}
            {artwork.size && <span>{artwork.size}<br/></span>}
          </p>
          {artwork.fileLabel && artwork.fileUpload && artwork.fileUpload.url &&
            <p className="prepend-xs-2"><a className="text-black text-underline" href={artwork.fileUpload.url} target="_blank">{artwork.fileLabel}</a></p>}
          {!fromGallery && <span className="divider divider--vertical--xs divider--bottom-left--xs" />}
        </div>

        {!fromGallery &&
          <Row className="non-printable prepend-xs-1 pend-horizontal-xs-none">
            <Col md={10} className="pad-horizontal-xs-none">
              <FormStackForm
                form={config.formStack.contact.id} viewkey={config.formStack.contact.viewKey}
                onSubmit={this.onSubmit} onError={this.onError} onSuccess={this.onSuccess}
                field35125999={firstName}
                field35126000={lastName}
                field35125981={email}
                field35125984={enquiry}
                related_artist={artwork.artist.name.full}>
                <h3 className="text-black append-xs-1">Contact Us</h3>

                <Row className="pend-horizontal-xs-none">
                  <Col md={6} className="pad-horizontal-xs-none postpad-horizontal-md-xtiny">
                    <div className={'first_name' in this.state.errors ? 'has-error': ''}>
                      <Input type="text" placeholder="First Name *" bsSize="large" valueLink={this.linkState('firstName')} groupClassName="append-xs-tiny" ref='first_name'/>
                    </div>
                  </Col>
                  <Col md={6} className="pad-horizontal-xs-none prepad-horizontal-md-xtiny">
                    <div className={'last_name' in this.state.errors ? 'has-error': ''}>
                      <Input type="text" placeholder="Last Name *" bsSize="large" valueLink={this.linkState('lastName')} groupClassName="append-xs-tiny" ref='last_name'/>
                    </div>
                  </Col>
                </Row>

                <div className={'email' in this.state.errors ? 'has-error': ''}>
                  <Input type="text" placeholder="Email *" bsSize="large" valueLink={this.linkState('email')} groupClassName="append-xs-tiny" ref='email'/>
                </div>
                <div className={'message' in this.state.errors ? 'has-error': ''}>
                  <Input type="textarea" label="Your enquiry ... *"
                    groupClassName="append-xs-2"
                    labelClassName="text-heavy append-xs-none" rows="3" bsSize="large"
                    valueLink={this.linkState('enquiry')} ref='message'/>
                </div>
                <div className="text-center-xs text-left-md append-xs-2">
                  <button className="btn btn-body-default btn-wide text-xs text-spaced text-uppercase" type="submit"
                    disabled={loading}>
                    {loading ? 'Sending...' : 'Submit'}
                  </button>
                </div>

                {sent && <p className="prepend-xs-2">Thank you. We will be in touch shortly.</p>}
                {error && <p className="prepend-xs-2 text-danger">{error}</p>}

                <div className="text-center-xs text-left-md">
                  <NumberRevealer />
                  <SocialSharers title={`${artwork.title} by ${artwork.artist.name.full}`} printable={true} className="prepend-xs-2" />
                </div>
              </FormStackForm>
            </Col>
          </Row>}
      </Col>
    );
  }

  renderArtworkTiles() {
    const { artist, artworks, exhibition, isFromDealingFrom } = this.props;
    const title = `Other works ${exhibition ? `in ${exhibition.title}` : `by ${artist.name.full}`}`;
    return (
      <MasonryTiles title={isFromDealingFrom ? 'Other artworks' : title} items={artworks}
        renderTile={(artwork) => <ArtworkTile artwork={artwork} exhibition={exhibition} showArtist={!!exhibition} isFromDealingFrom={isFromDealingFrom} />} />
    );
  }

  renderExhibitions() {
    const { exhibitions } = this.props;

    return (
      <Row className="bordered--xs bordered--top--xs">
        <div className="clearfix">
          <Col md={8} mdOffset={4} className="divider-container prepad-xs-2 postpad-xs-1 postpad-sm-1 pad-horizontal-xs-none pad-horizontal-md-1">
            <h2 className="prepend-xs-none append-xs-none section-heading accent-overlay text-sm text-spaced text-uppercase pad-vertical-xs-1 pad-horizontal-xs-xtiny pad-sm-1">
              \ Featured in
            </h2>
            <div className="prepend-xs-1">
              <ExhibitionsModule exhibitions={exhibitions} linkToAll={true} />
            </div>
            <span className="divider divider--horizontal--xs divider--top-right--xs divider--top-right--md" />
          </Col>
        </div>
      </Row>
    );
  }

  render() {
    const { artwork, artworks, bodyTheme, exhibitions } = this.props;
    const { isPortalOpened } = this.state;

    return (
      <DocumentTitle title={artwork.meta.title}>
        <div>
          <DocMeta tags={artwork.meta.tags} />
          <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
            <Row className="bordered--xs bordered--bottom--xs flex-center-md">
              {this.renderBreadcrumb()}
              {this.renderNavigation()}
            </Row>
            <Row>
              {this.renderArtwork()}
              {this.renderInfo()}
            </Row>
          </Grid>
          {_.any(artworks) &&
            <div className="non-printable accent-container">
              <Grid className="bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
                {this.renderArtworkTiles()}
              </Grid>
              <div className="accent bg-striped"></div>
            </div>}
          {_.any(exhibitions) &&
            <Grid className="non-printable bordered--xs bordered--left--xs bordered--right--xs bordered--centred--xs bordered--centred--md--none bordered--third--md">
              {this.renderExhibitions()}
            </Grid>}

          {typeof window !== 'undefined' &&
            <Portal isOpened={isPortalOpened} closeOnEsc={true} onClose={this.closePortal}>
              <div className={classNames('non-printable gallery text-center-xs text-left-md', `body-${bodyTheme}`)}>
                <div className="gallery__body">
                  <Grid fluid>
                    <Row className="text-inverse">
                      {this.renderArtwork(true)}
                      {this.renderInfo(true)}
                    </Row>
                  </Grid>
                </div>
              </div>
            </Portal>}
        </div>
      </DocumentTitle>
    );
  }

  openPortal(e) {
    this.setState({ isPortalOpened: true });
    if (e) { e.preventDefault(); }
  }

  closePortal(e) {
    this.setState({ isPortalOpened: false });
    if (e) { e.preventDefault(); }
  }

  artworkUrl(slug) {
    const { exhibition, isFromDealingFrom } = this.props;

    if (isFromDealingFrom) {
      return `/private-treaty/artwork/${slug}`;
    }

    return `${exhibition ? `/exhibition/${exhibition.slug}` : ''}/artwork/${slug}`;
  }

  constructor(props) {
    super(props);

    this.state = {
      sent: false,
      loading: false,
      firstName: void 0,
      lastName: void 0,
      email: void 0,
      enquiry: void 0,
      error: void 0,
      errors: {},
      isPortalOpened: props.showGallery,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
    this.onError = this.onError.bind(this);
    this.openPortal = this.openPortal.bind(this);
    this.closePortal = this.closePortal.bind(this);
  }
}

ArtworkPage.contextTypes = {
  config: React.PropTypes.object.isRequired,
};

ArtworkPage.propTypes = {
  artwork: React.PropTypes.object.isRequired,
  artist: React.PropTypes.object.isRequired,
  artworks: React.PropTypes.array.isRequired,
  bodyTheme: React.PropTypes.string,
  exhibition: React.PropTypes.object,
  exhibitions: React.PropTypes.array.isRequired,
  isFromDealingFrom: React.PropTypes.bool,
};

reactMixin.onClass(ArtworkPage, React.addons.LinkedStateMixin);

ArtworkPage = connectToStores(ArtworkPage, [ ArtworkStore, ExhibitionStore ], (context) => {
  const artworkStore = context.getStore(ArtworkStore);
  const exhibitionStore = context.getStore(ExhibitionStore);
  const artwork = artworkStore.getArtwork();
  const exhibition = exhibitionStore.getExhibition();

  return {
    artwork,
    artist: artwork.artist,
    artworks: artworkStore.getArtworks() || artworkStore[exhibition ? 'getExhibitionArtworks' : 'getArtistArtworks'](),
    bodyTheme: context.getStore(ApplicationStore).getBodyTheme(),
    exhibition,
    exhibitions: exhibitionStore.getArtworkExhibitions(),
  };
});

ArtworkPage = Resolver.createContainer(ArtworkPage, {
  contextTypes: {
    router: React.PropTypes.func.isRequired,
    getStore: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func.isRequired,
  },

  resolve: {
    showGallery: (props, context) => {
      const { router } = context;
      return !_.isUndefined(router.getCurrentQuery()._);
    },

    isFromDealingFrom: (props, context) => {
      const { router } = context;
      return router.getCurrentPathname().indexOf('/private-treaty') === 0;
    },

    artwork: (props, context) => {
      const { router } = context;
      const { exhibitionSlug } = props.params;
      const artworkStore = context.getStore(ArtworkStore);

      return context.executeAction(loadArtworkAction, props.params.slug).then(() => {
        const artwork = artworkStore.getArtwork();

        if (router.getCurrentPathname().indexOf('/private-treaty') === 0) {
          return context.executeAction(loadArtworksAction, { dealingRoom: true });
        }

        if (exhibitionSlug) {
          return context.executeAction(loadExhibitionArtworksAction, exhibitionSlug);
        }

        if (artwork) {
          return context.executeAction(loadArtistArtworksAction, artwork.artist.slug);
        }
      }).then(() => {
        const artwork = artworkStore.getArtwork();
        if (artwork) {
          return context.executeAction(setBodyThemeAction, colors[artwork.artist.color]);
        }
      });
    },

    exhibition: (props, context) => {
      const { exhibitionSlug } = props.params;
      if (exhibitionSlug) {
        return context.executeAction(loadExhibitionAction, exhibitionSlug);
      }
    },

    exhibitions: (props, context) => {
      return context.executeAction(loadArtworkExhibitionsAction, props.params.slug);
    },
  },
});

export default ArtworkPage;
