import React, { Component } from 'react';
import DocMeta from 'react-doc-meta';
import DocumentTitle from 'react-document-title';

class Html extends Component {
  render() {
    const metaTags = DocMeta.rewind();
    let clientJsFile = '/js/client.js';

    if (process.env.NODE_ENV === 'development') {
      const hotLoadPort = process.env.HOT_LOAD_PORT || 8888;
      clientJsFile = `http://localhost:${hotLoadPort}/js/client.js`;
    }

    return (
      <html>
      <head>
        <meta charSet="utf-8" />
        <title>{DocumentTitle.rewind()}</title>

        {metaTags.map((tag, index) => <meta data-doc-meta="true" key={index} {...tag} />)}
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0" />

        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152.png" />

        <link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="/favicon-128.png" sizes="128x128" />

        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="/mstile-310x310.png" />

        <link rel="stylesheet" href="/styles/site.css" />
      </head>
      <body dangerouslySetInnerHTML={{__html: `
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=${process.env.GTM_CODE}"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','${process.env.GTM_CODE}');
        </script>
        <div id="app">${this.props.markup}</div>
        <script type="text/javascript">
          (function() {var s = document.createElement('script');s.type = 'text/javascript';s.async = true;
          s.src = document.location.protocol + '//loader.wisepops.com/default/index/get-loader?user_id=25080';
          var s2 = document.getElementsByTagName('script')[0];s2.parentNode.insertBefore(s, s2);})();
        </script>
        `}}>
      </body>
      <script dangerouslySetInnerHTML={{__html: this.props.state}}></script>
      <script src={clientJsFile} defer></script>
      <script src="/js/placeholders.js"></script>
      </html>
    );
  }
}

Html.propTypes = {
  markup: React.PropTypes.string.isRequired,
  state: React.PropTypes.string.isRequired,
};

export default Html;
